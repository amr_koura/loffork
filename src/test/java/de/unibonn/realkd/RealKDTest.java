package de.unibonn.realkd;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Test;

import de.unibonn.realkd.RealKD;

public class RealKDTest {

	@Test
	public void mainTest() {
		File outputfile = new File("resultpatterns.txt");
		outputfile.delete();
		String[] args = { "load", TestConstants.GERMANY_DATA_TXT,
				TestConstants.GERMANY_ATTRIBUTES_TXT,
				TestConstants.GERMANY_GROUPS_TXT, "run", "EMM_BEAMSEARCH",
				"Model class=Mean model",
				"Model distance function=Manhattan mean distance",
//				"Model class and distance function=Eucledian distance between means",
				"Target attributes=CDU 2005,SPD 2005", "test=blupp" };
		RealKD.main(args);
		assertTrue(outputfile.exists());
	}
	

	@After
	public void cleanUp() {
		File outputfile = new File("resultpatterns.txt");
		outputfile.delete();
	}

}
