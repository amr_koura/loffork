package de.unibonn.realkd;

public class TestConstants {

	public static final String GERMANY_GROUPS_TXT = "testdata//germany//groups.txt";
	
	public static final String GERMANY_ATTRIBUTES_TXT = "testdata//germany//attributes.txt";
	
	public static final String GERMANY_DATA_TXT = "testdata//germany//data.txt";

}


