package de.unibonn.realkd;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.unibonn.realkd.algorithms.association.AssociationMiningBeamSearch;
import de.unibonn.realkd.algorithms.association.AssociationTargetFunctionParameter;
import de.unibonn.realkd.algorithms.association.AssociationSampler;
import de.unibonn.realkd.algorithms.emm.EMMTargetFunctionParameter;
import de.unibonn.realkd.algorithms.emm.ExceptionalModelBeamSearch;
import de.unibonn.realkd.algorithms.emm.ExceptionalModelSampler;
import de.unibonn.realkd.algorithms.emm.dssd.DiverseSubgroupSetDiscovery;
import de.unibonn.realkd.algorithms.outlier.OneClassModelMiner;
import de.unibonn.realkd.algorithms.sampling.DiscriminativityDistributionFactory;
import de.unibonn.realkd.algorithms.sampling.RareDistributionTimesPowerOfFrequencyFactory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.DataWorkspaceFactory;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVFileBuilder;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ContingencyTableModel;
import de.unibonn.realkd.patterns.emm.ContingencyTableModelFactory;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.MeanModelFactory;
import de.unibonn.realkd.patterns.emm.ProbabilisticModel;
import de.unibonn.realkd.patterns.outlier.Outlier;

public class IntegrationTest {

	private static DataWorkspace workspace;

	@BeforeClass
	public static void loadData() throws DataFormatException {
		workspace = DataWorkspaceFactory.INSTANCE.createDataWorkspace();
		DataTableFromCSVFileBuilder builder = new DataTableFromCSVFileBuilder();
		builder.setAttributeGroupCSVFilename(TestConstants.GERMANY_GROUPS_TXT)
				.setDataCSVFilename(TestConstants.GERMANY_DATA_TXT)
				.setAttributeMetadataCSVFilename(
						TestConstants.GERMANY_ATTRIBUTES_TXT);
		DataTable dataTable = builder.build();
		workspace.add(dataTable);
		workspace.add(new PropositionalLogic(dataTable));
	}

	@Test
	@Ignore("reason: currently dssd not functional")
	public void dssdTest() {
		DiverseSubgroupSetDiscovery dssd = new DiverseSubgroupSetDiscovery(
				workspace);
		dssd.findParameterByName("Target attributes").setByString("Unemployed");
		Collection<Pattern> results = dssd.call();
		assertNotNull("result should be not null", results);
		assertFalse("result should be not empty", results.isEmpty());
	}

	@Test
	public void testSubspaceOutlierAlgorithm() {
		OneClassModelMiner oneClassModelMiner = new OneClassModelMiner(
				workspace);
		oneClassModelMiner.getTargetAttributesParameter().setByString(
				"CDU 2005", "CDU 2009");
		Collection<Pattern> resultPatterns = oneClassModelMiner.call();
		assertNotNull(resultPatterns);
		assertFalse(resultPatterns.isEmpty());
		Pattern pattern = resultPatterns.iterator().next();
		assertTrue(pattern instanceof Outlier);
	}

	@Test
	public void testAssociationBeamSearchNegativeLift() {
		AssociationMiningBeamSearch associationMiningBeamSearch = new AssociationMiningBeamSearch(
				workspace);

		associationMiningBeamSearch.findParameterByName("Number of results")
				.setByString("3");
		// associationMiningBeamSearch.getOptimizationOrderParameter().set(
		// Association.NEGATIVE_LIFT_COMPARATOR);
		associationMiningBeamSearch.findParameterByName(
				AssociationTargetFunctionParameter.NAME).setByString(
				"negative lift");
		// .setComparator(Association.NEGATIVE_LIFT_COMPARATOR);
		Collection<Pattern> resultPatterns = associationMiningBeamSearch.call();
		assertNotNull(resultPatterns);
		assertFalse(resultPatterns.isEmpty());
		assertTrue(resultPatterns.size() == 3);
		Pattern pattern = resultPatterns.iterator().next();
		assertTrue(pattern instanceof Association);
		assertTrue(pattern.hasMeasure(InterestingnessMeasure.NEGATIVE_LIFT));
	}

	@Test
	public void testAssociationBeamSearchPositiveLift() {
		AssociationMiningBeamSearch associationMiningBeamSearch = new AssociationMiningBeamSearch(
				workspace);
		associationMiningBeamSearch.findParameterByName("Number of results")
				.setByString("8");
		// associationMiningBeamSearch.getOptimizationOrderParameter().set(
		// Association.POSITIVE_LIFT_COMPARATOR);
		associationMiningBeamSearch.findParameterByName(
				AssociationTargetFunctionParameter.NAME).setByString("lift");
		// .setComparator(Association.POSITIVE_LIFT_COMPARATOR);
		Collection<Pattern> resultPatterns2 = associationMiningBeamSearch
				.call();
		assertTrue(resultPatterns2.size() == 8);
		Pattern pattern2 = resultPatterns2.iterator().next();
		assertTrue("Pattern must have lift measure bound",
				pattern2.hasMeasure(InterestingnessMeasure.LIFT));
	}

	@Test
	public void testEMMBeamsearchInvalidSetting() {
		ExceptionalModelBeamSearch exceptionalModelBeamSearch = new ExceptionalModelBeamSearch(
				workspace);

		exceptionalModelBeamSearch.findParameterByName("Beam width")
				.setByString("4");
		exceptionalModelBeamSearch.findParameterByName("Number of results")
				.setByString("3");
		exceptionalModelBeamSearch.getTargetAttributesParameter().setByString(
				"Type", "No school degree");
		exceptionalModelBeamSearch.getModelClassParameter().set(
				MeanModelFactory.INSTANCE);
		exceptionalModelBeamSearch.getModelDistanceFunctionParameter().set(
				ProbabilisticModel.TOTALVARIATION);

		try {
			exceptionalModelBeamSearch.call();
		} catch (IllegalStateException e) {
			System.out.println(e.getMessage());
			assertTrue("expected exception", e != null);
		}
	}

	@Test(expected = IllegalStateException.class)
	public void testInvalidToModifyParameterDuringRuntime()
			throws InterruptedException, ExecutionException {
		ExceptionalModelBeamSearch exceptionalModelBeamSearch = new ExceptionalModelBeamSearch(
				workspace);

		exceptionalModelBeamSearch.findParameterByName("Beam width")
				.setByString("2");
		exceptionalModelBeamSearch.findParameterByName("Number of results")
				.setByString("3");
		exceptionalModelBeamSearch.getTargetAttributesParameter().setByString(
				"Type", "No school degree");
		exceptionalModelBeamSearch.getModelClassParameter().setByString(
				"Contingency table");
		exceptionalModelBeamSearch.getModelDistanceFunctionParameter()
				.setByString("Total variation distance");

//		exceptionalModelBeamSearch.getOptimizationFunctionParameter().set(
//				ExceptionalModelPattern.DEVIATION_COMPARATOR);

		exceptionalModelBeamSearch.findParameterByName(
				EMMTargetFunctionParameter.NAME).setByString(
				"deviation");

		Future<Collection<Pattern>> future = Executors
				.newSingleThreadExecutor().submit(exceptionalModelBeamSearch);
		while (!exceptionalModelBeamSearch.isRunning()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
		exceptionalModelBeamSearch.findParameterByName("Beam width")
				.setByString("5");
		Collection<Pattern> results = future.get();

	}

	@Test
	public void testEMMBeamSearch() {
		ExceptionalModelBeamSearch exceptionalModelBeamSearch = new ExceptionalModelBeamSearch(
				workspace);

		exceptionalModelBeamSearch.findParameterByName("Beam width")
				.setByString("2");
		exceptionalModelBeamSearch.findParameterByName("Number of results")
				.setByString("3");
		exceptionalModelBeamSearch.getTargetAttributesParameter().setByString(
				"Type", "No school degree");
		exceptionalModelBeamSearch.getModelClassParameter().setByString(
				"Contingency table");
		exceptionalModelBeamSearch.getModelDistanceFunctionParameter()
				.setByString("Total variation distance");
		// exceptionalModelBeamSearch
		// .getEMMPatternClassAndDistanceFunctionParameter()
		// .set(ExceptionalModelPatternFactory.TVD_BETWEEN_CONTINGENCY_TABLE_PF);
//		exceptionalModelBeamSearch.getOptimizationFunctionParameter().set(
//				ExceptionalModelPattern.DEVIATION_COMPARATOR);

		exceptionalModelBeamSearch.findParameterByName(
				EMMTargetFunctionParameter.NAME).setByString(
				"deviation");

		// assertTrue(exceptionalModelBeamSearch
		// .getEMMPatternClassAndDistanceFunctionParameter()
		// .getCurrentValue()
		// .equals(ExceptionalModelPatternFactory.TVD_BETWEEN_CONTINGENCY_TABLE_PF));
//		assertTrue(exceptionalModelBeamSearch
//				.getOptimizationFunctionParameter().getCurrentValue()
//				.equals(ExceptionalModelPattern.DEVIATION_COMPARATOR));
		Collection<Pattern> resultPatterns = exceptionalModelBeamSearch.call();
		assertTrue(resultPatterns.size() == 3);
		Pattern topPattern = resultPatterns.iterator().next();
		assertTrue(topPattern instanceof ExceptionalModelPattern);
		assertTrue(topPattern
				.hasMeasure(InterestingnessMeasure.TOTAL_VARIATION_DISTANCE));
		assertFalse(topPattern
				.hasMeasure(InterestingnessMeasure.MANHATTAN_MEAN_DEVIATION));

		ExceptionalModelPattern topPatternAsEMM = (ExceptionalModelPattern) topPattern;
		assertTrue(topPatternAsEMM.getGlobalModel() instanceof ContingencyTableModel);
	}

	@Test
	public void testAssociationSampler() {
		AssociationSampler associationSampler = new AssociationSampler(
				workspace);
		associationSampler.setNumberOfResults(23);
		associationSampler
				.setDistributionFactory(new RareDistributionTimesPowerOfFrequencyFactory(
						2));

		Collection<Pattern> patterns = associationSampler.call();
		assertTrue(patterns.size() == 23);
	}

	@Test
	public void testEMMSampler() {
		ExceptionalModelSampler exceptionalModelSampler = new ExceptionalModelSampler(
				workspace);

		exceptionalModelSampler.setNumberOfResults(10);
		exceptionalModelSampler.findParameterByName("Number of results")
				.setByString("15");
		exceptionalModelSampler.getTargetAttributesParameter().setByString(
				"Type", "No school degree");
		exceptionalModelSampler
				.setDistributionFactory(new DiscriminativityDistributionFactory(
						exceptionalModelSampler.getTargetAttributesParameter(),
						1, 1, 1));

		Parameter<?> posNegCreatorParameter = exceptionalModelSampler
				.findParameterByName("Splitting method");
		assertNotNull(posNegCreatorParameter);
		RangeEnumerableParameter<?> posNegCreatorAsRangeEnum = (RangeEnumerableParameter<?>) posNegCreatorParameter;
		assertTrue(posNegCreatorAsRangeEnum.getRange().size() == 2);
		posNegCreatorAsRangeEnum.setByString("split on all attributes");
		assertTrue(posNegCreatorAsRangeEnum.getCurrentValue().toString()
				.equals("split on all attributes"));

		exceptionalModelSampler.getModelClassParameter().set(
				ContingencyTableModelFactory.INSTANCE);
		// exceptionalModelSampler
		// .getModelDistanceFunctionParameter().set(ProbabilisticModel.TOTALVARIATION);

		Collection<Pattern> resultPatterns = exceptionalModelSampler.call();
		assertTrue(resultPatterns.size() == 15);
		Pattern topPattern = resultPatterns.iterator().next();
		assertTrue(topPattern instanceof ExceptionalModelPattern);
		assertTrue(topPattern
				.hasMeasure(InterestingnessMeasure.TOTAL_VARIATION_DISTANCE));
		assertFalse(topPattern
				.hasMeasure(InterestingnessMeasure.MANHATTAN_MEAN_DEVIATION));
	}
}
