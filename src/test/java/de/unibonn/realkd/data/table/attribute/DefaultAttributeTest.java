package de.unibonn.realkd.data.table.attribute;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.unibonn.realkd.data.table.attribute.DefaultAttribute;

public class DefaultAttributeTest {

	private static final List<String> TEST_ATTRIBUTE_VALUES = Arrays.asList("A", "B", null, "A");
	private static final String TEST_ATTRIBUTE_DESCRIPTION = "This attribute has been added for unit testing DefaultAttribute.java";
	private static final String TEST_ATTRIBUTE_NAME = "Test Attribute";
	private DefaultAttribute<String> testAttribute;

	@Before
	public void setUp() {
		this.testAttribute = new DefaultAttribute<String>(
				TEST_ATTRIBUTE_NAME,
				TEST_ATTRIBUTE_DESCRIPTION,
				TEST_ATTRIBUTE_VALUES);
	}
	
	@Test
	public void testIsValueMissing() {
		assertTrue(testAttribute.isValueMissing(2));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetValueIllegalAccess() {
		testAttribute.getValue(2);
	}
	
	@Test
	public void testGetValue() {
		assertEquals("B",testAttribute.getValue(1));
	}
	
}
