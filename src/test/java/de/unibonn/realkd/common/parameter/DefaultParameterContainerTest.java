package de.unibonn.realkd.common.parameter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;

/**
 * Test case with complex parameter container with four parameters: a,b,c, and
 * d.
 * 
 * Parameters a and b are simple integer parameters that accept positive values.
 * 
 * Parameter c is a complex range enumerable parameter that contains two
 * options, each of which are again parameter container that contain a
 * sub-parameter called sub-option. These sub-parameters are range-enumerable
 * with the available range being the integers 1 to value-of-a (for the first
 * container) and 1 to value-of-b (for the second container). Thus, c depends on
 * a and b.
 * 
 * Finally, parameter d is a sublist parameter with range being the possible
 * strings from 1 to value-of-subparameter-of-value-of-c.
 * 
 * @author mboley
 * 
 */
public class DefaultParameterContainerTest {

	private static final String D_PARAMETER_NAME = "d";

	private static final String SUB_PARAMETER_NAME = "sub-option";

	private static final String A_PARAM_NAME = "a";

	private static final String B_PARAM_NAME = "b";

	private static final String C_PARAM_NAME = "c";

	private Parameter<Integer> a;

	private Parameter<Integer> b;

	private RangeEnumerableParameter<ParameterContainer> c;

	private DefaultParameterContainer firstPossibleCValue;

	private DefaultParameterContainer secondPossibelCValue;

	private SubListParameter<List<String>> d;

	private DefaultParameterContainer container;

	private class MySublistParam extends
			AbstractNonEmptySubListParameter<List<String>> {

		public MySublistParam() {
			super(D_PARAMETER_NAME, "", List.class, null, "", c);
		}

		@Override
		protected List<String> getConcreteRange() {
			int rangeBound = Integer.parseInt(c.getCurrentValue()
					.findParameterByName(SUB_PARAMETER_NAME).getCurrentValue()
					.toString());
			List<String> result = new ArrayList<>();
			for (int i = 0; i < rangeBound; i++) {
				result.add(new Integer(i).toString());
			}
			return result;
		}

	}

	@Before
	public void setUp() {

		a = new DefaultParameter<Integer>(A_PARAM_NAME, "", Integer.class, 1,
				StringParser.INTEGER_PARSER,
				new ValueValidator.LargerThanThresholdValidator<Integer>(0), "");

		b = new DefaultParameter<Integer>(B_PARAM_NAME, "", Integer.class, 1,
				StringParser.INTEGER_PARSER,
				new ValueValidator.LargerThanThresholdValidator<Integer>(0), "");

		firstPossibleCValue = new DefaultParameterContainer("first c option");
		firstPossibleCValue.addParameter(createRangeEnumParamDependingOn(a));
		secondPossibelCValue = new DefaultParameterContainer("second c option");
		secondPossibelCValue.addParameter(createRangeEnumParamDependingOn(b));

		c = new DefaultRangeEnumerableParameter<ParameterContainer>(
				C_PARAM_NAME, "", ParameterContainer.class,
				new RangeComputer<ParameterContainer>() {
					@Override
					public List<ParameterContainer> computeRange() {
						return Arrays.asList(
								(ParameterContainer) firstPossibleCValue,
								(ParameterContainer) secondPossibelCValue);
					}
				}, firstPossibleCValue.findParameterByName(SUB_PARAMETER_NAME),
				secondPossibelCValue.findParameterByName(SUB_PARAMETER_NAME));

		d = new MySublistParam();

		container = new DefaultParameterContainer("test container");

		container.addParameter(a);
		container.addParameter(b);
		container.addParameter(c);
		container.addParameter(d);
	}

	private DefaultRangeEnumerableParameter<Integer> createRangeEnumParamDependingOn(
			final Parameter<Integer> depParam) {
		return new DefaultRangeEnumerableParameter<>(SUB_PARAMETER_NAME, "",
				Integer.class, new RangeComputer<Integer>() {

					@Override
					public List<Integer> computeRange() {
						List<Integer> result = new ArrayList<>();
						for (int i = 0; i < depParam.getCurrentValue(); i++) {
							result.add(i);
						}
						return result;
					}
				}, depParam);
	}

	@Test
	public void testBlockAssignment() {
		Map<String, String[]> keyValues = new HashMap<>();
		keyValues.put(A_PARAM_NAME, new String[] { "4" });
		keyValues.put(B_PARAM_NAME, new String[] { "5" });
		keyValues.put(SUB_PARAMETER_NAME, new String[] { "3" });
		keyValues.put(D_PARAMETER_NAME, new String[] { "1", "2" });
		container.passValuesToParameters(keyValues);

		assertEquals(Arrays.asList("1", "2"),
				container.findParameterByName(D_PARAMETER_NAME)
						.getCurrentValue());

		assertTrue(container.isStateValid());
	}

	@Test
	public void testIndividualAssignment() {
		container.findParameterByName(A_PARAM_NAME).setByString("4");
		RangeEnumerableParameter<?> subParam = (RangeEnumerableParameter<?>) c
				.getCurrentValue().findParameterByName(SUB_PARAMETER_NAME);
		assertNotNull(subParam);
		assertEquals(4, subParam.getRange().size());
		assertTrue(subParam == container
				.findParameterByName(SUB_PARAMETER_NAME));

		container.findParameterByName(SUB_PARAMETER_NAME).setByString("3");
		container.findParameterByName(D_PARAMETER_NAME).setByString("1", "2");
		assertEquals(Arrays.asList("1", "2"),
				container.findParameterByName(D_PARAMETER_NAME)
						.getCurrentValue());

		assertTrue(container.isStateValid());
	}

	@Test
	public void testFindTopLevelParameterByString() {
		assertNotNull(container.findParameterByName(A_PARAM_NAME));
	}

	@Test
	public void testGetTopLevelParameters() {
		assertEquals(4, container.getTopLevelParameters().size());
	}

	@Test
	public void testFindNestedParameterByString() {
		assertNotNull(container.findParameterByName(SUB_PARAMETER_NAME));
	}

	@Test
	public void testNonStateValidIfOneParemeterInvalid() {
		container.findParameterByName(A_PARAM_NAME).set(null);
		assertFalse(container.isStateValid());
	}

}
