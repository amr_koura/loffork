package de.unibonn.realkd.knowledgemodeling.learning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.knowledgemodeling.constraints.FrequencyConstraint;
import de.unibonn.realkd.knowledgemodeling.constraints.MaxEntConstraint;

/**
 * @author bkang
 */
public class ValueSpace {

    private final int cardinality;

    private final Map<Attribute, List<String>> values;

    private final List<MaxEntConstraint> associatedConstraints;

    private final List<Attribute> associatedAttributes;

    public ValueSpace(MaxEntConstraint constraint, List<Attribute> associatedAttributes) {
        this.associatedConstraints = new ArrayList<>();
        this.associatedConstraints.add(constraint);
        this.values = initValueSpace(constraint);
        this.cardinality = computeCardinality(values, associatedAttributes);
        this.associatedAttributes = associatedAttributes;
    }

    public ValueSpace(List<MaxEntConstraint> associatedConstraints, Map<Attribute, List<String>> values, List<Attribute> associatedAttributes) {
        this.associatedConstraints = associatedConstraints;
        this.values = values;
        this.cardinality = computeCardinality(values, associatedAttributes);;
        this.associatedAttributes = associatedAttributes;
    }

    private int computeCardinality(Map<Attribute, List<String>> values, List<Attribute> attributes) {
        int result = 1;
        Set<Attribute> keySet = values.keySet();
        for (Attribute attribute : attributes) {
            if (keySet.contains(attribute)) {
                result *= values.get(attribute).size();
                if (result == 0) {
                    return 0;
                }
            } else {
                result *= computeSubSpaceCardOfAttribute(attribute);
            }
        }
        return result;
    }

    private int computeSubSpaceCardOfAttribute(Attribute attribute) {
        if (attribute instanceof CategoricalAttribute) {
            return ((CategoricalAttribute) attribute).getCategories().size();
        } else if (attribute instanceof MetricAttribute) {
            return (attribute.getNumberOfNonMissingValues() + attribute.getMissingPositions().size());
        } else {
            throw new IllegalArgumentException();
        }
    }

    private Map<Attribute, List<String>> initValueSpace(MaxEntConstraint constraint) {
        Map<Attribute, List<String>> result = new HashMap<>();
        Map<Attribute, List<Proposition>> attributePropositionMap = computeAttributePropositionMap(((FrequencyConstraint)constraint).getAssociatedPropostions());
        for (Attribute attribute : attributePropositionMap.keySet()) {
            result.put(attribute, computeOneDimSubValueSpace(attributePropositionMap.get(attribute), attribute));
        }
        return result;
    }

    private Map<Attribute, List<Proposition>>computeAttributePropositionMap(List<Proposition> propositions) {
        Map<Attribute, List<Proposition>> result = new HashMap<>();
        for (Proposition proposition : propositions) {
            Attribute attribute = proposition.getAttribute();
            if (!result.containsKey(proposition.getAttribute())) {
                result.put(attribute, new ArrayList<Proposition>());
            }
            result.get(attribute).add(proposition);
        }
        return result;
    }

    private List<String> computeOneDimSubValueSpace(List<Proposition> propositions, Attribute attribute) {
        if (attribute instanceof CategoricalAttribute) {
            return filterCategoricalValuesAgainstPropositions(((CategoricalAttribute) attribute).getCategories(), propositions);
        } else if (attribute instanceof MetricAttribute){
            return convertSupportSetToList(filterMetricSupportAgainstPropositions(propositions.get(0).getSupportSet(), propositions));
        } else {
            throw new IllegalArgumentException();
        }
    }


    private List<String> filterCategoricalValuesAgainstPropositions(List<String> values, List<Proposition> propositions) {
        boolean isPassedAllPropositions;
        List<String> result = new ArrayList<>();
        for (String value : values) {
            isPassedAllPropositions = true;
            for (Proposition proposition : propositions) {
                if (!proposition.getConstraint().holds(value)) {
                    isPassedAllPropositions = false;
                    break;
                }
            }
            if (isPassedAllPropositions) {
                result.add(value);
            }
        }
        return result;
    }

    private List<String> convertSupportSetToList(Set<Integer> support) {
        List<String> result = new ArrayList<>();
        for (Integer integer : support) {
            result.add(integer.toString());
        }
        return result;
    }

    private Set<Integer> filterMetricSupportAgainstPropositions(Set<Integer> support, List<Proposition> propositions) {
        Set<Integer> result = new HashSet<>(support);
        for (Proposition proposition : propositions) {
            result.retainAll(proposition.getSupportSet());
        }
        return result;
    }

    public ValueSpace getSubSpace(MaxEntConstraint additionalConstraint) {
        Map<Attribute, List<Proposition>> additionalAttributePropositionMap = computeAttributePropositionMap(((FrequencyConstraint)additionalConstraint).getAssociatedPropostions());
        Map<Attribute, List<String>> newValues = new HashMap<>();
        for (Attribute attribute : values.keySet()) {
            if (additionalAttributePropositionMap.keySet().contains(attribute)) {
                List<Proposition> additionalPropositions = additionalAttributePropositionMap.get(attribute);
                if (attribute instanceof CategoricalAttribute) {
                    List<String> valuesOfOneAttribute = filterCategoricalValuesAgainstPropositions(values.get(attribute), additionalPropositions);
                    newValues.put(attribute, valuesOfOneAttribute);
                } else if (attribute instanceof MetricAttribute) {
                    Set<Integer> support = new HashSet<>();
                    for (String s : values.get(attribute)) {
                        support.add(Integer.valueOf(s));
                    }
                    newValues.put(attribute, convertSupportSetToList(filterMetricSupportAgainstPropositions(support, additionalPropositions)));
                } else {
                    throw new IllegalArgumentException();
                }
            } else {
                newValues.put(attribute, new ArrayList<>(values.get(attribute)));
            }
        }
        for (Attribute attribute : additionalAttributePropositionMap.keySet()) {
            if (!values.keySet().contains(attribute)) {
                newValues.put(attribute, computeOneDimSubValueSpace(additionalAttributePropositionMap.get(attribute), attribute));

            }
        }


        List<MaxEntConstraint> newConstraints = new ArrayList<>(associatedConstraints);
        newConstraints.add(additionalConstraint);
        return new ValueSpace(newConstraints, newValues, associatedAttributes);
    }

    public int getCardinality() {
        return cardinality;
    }

    public List<MaxEntConstraint> getAssociatedConstraints() {
        return associatedConstraints;
    }

    public Map<Attribute, List<String>> getValues() {
        return values;
    }


}
