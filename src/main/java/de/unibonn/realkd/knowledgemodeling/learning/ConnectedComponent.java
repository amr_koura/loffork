package de.unibonn.realkd.knowledgemodeling.learning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.knowledgemodeling.constraints.FrequencyConstraint;
import de.unibonn.realkd.knowledgemodeling.constraints.MaxEntConstraint;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.association.DefaultAssociationBuilder;

/**
 * @author bkang
 */
public class ConnectedComponent {

	private final List<MaxEntConstraint> constraints;

	private final Map<String, Integer> cardinalityOfPartitions;

	private final Map<String, ValueSpace> valuesPacesOfConstraintCombinations;

	private final List<Attribute> attributesWithInConnectedComponent;

	private Map<String, Double> constCombinationToPartitionValueMap;

	private double partitionFunctionValue;

	public ConnectedComponent(List<MaxEntConstraint> constraints) {
		this.constraints = constraints;

		this.attributesWithInConnectedComponent = computeAttributesWithinConnectedComponent();
		this.valuesPacesOfConstraintCombinations = ValueSpaceUtility
				.computeSubValueSpacesOfConstraintCombinations(constraints,
						attributesWithInConnectedComponent);
		this.cardinalityOfPartitions = ValueSpaceUtility
				.computeCardinalityOfPartitions(valuesPacesOfConstraintCombinations);
		recomputePartitionFunctionValue();
	}

	public double computeSumOfMultipliersActivatedByWildCardProps(
			Set<Proposition> propositions) {
		PropositionalLogic propositionalLogic = ((FrequencyConstraint) constraints
				.get(0)).getPattern().getPropositionalLogic();
		MaxEntConstraint newConstraint = new FrequencyConstraint(
				new DefaultAssociationBuilder().build((new Description(
						propositionalLogic, propositions))));
		List<MaxEntConstraint> augmentedConstraints = new ArrayList<>(
				constraints);
		augmentedConstraints.add(newConstraint);

		Map<String, ValueSpace> augmentedValuesPacesOfConstraintCombinations = ValueSpaceUtility
				.computeSubValueSpacesOfConstraintCombinations(
						augmentedConstraints,
						attributesWithInConnectedComponent);
		Map<String, Integer> cardinalityOfAugmentedPartitions = ValueSpaceUtility
				.computeCardinalityOfPartitions(augmentedValuesPacesOfConstraintCombinations);

		List<String> keys = ValueSpaceUtility
				.getKeysOfCombinationsContainConstriant(newConstraint,
						cardinalityOfAugmentedPartitions.keySet(),
						augmentedConstraints);

		double result = 0.;
		for (String key : keys) {
			int cardinality = cardinalityOfAugmentedPartitions.get(key);
			if (cardinality != 0) {
				List<MaxEntConstraint> constraintInCombination = ValueSpaceUtility
						.getConstraintByDecodingKey(key, augmentedConstraints);
				double sum = 0;
				for (MaxEntConstraint constraint : constraintInCombination) {
					sum += constraint.getMultiplier();
				}
				sum = Math.exp(sum);
				result += sum * cardinality;
			}
		}

		return result;
	}

	public void recomputePartitionFunctionValue() {
		partitionFunctionValue = 0.;
		constCombinationToPartitionValueMap = new HashMap<>();
		int cardOfNotPassedValues = ValueSpaceUtility
				.computeValueSpaceCardinalityFromAttributes(new HashSet<>(
						attributesWithInConnectedComponent));
		for (String key : cardinalityOfPartitions.keySet()) {
			int cardinality = cardinalityOfPartitions.get(key);
			if (cardinality != 0) {
				List<MaxEntConstraint> constraintsInCombination = ValueSpaceUtility
						.getConstraintByDecodingKey(key, constraints);
				double sum = 0.;
				for (MaxEntConstraint constraint : constraintsInCombination) {
					sum += constraint.getMultiplier();
				}
				sum = Math.exp(sum);
				cardOfNotPassedValues -= cardinality;
				constCombinationToPartitionValueMap.put(key, sum * cardinality);
				partitionFunctionValue += sum * cardinality;
			} else {
				constCombinationToPartitionValueMap.put(key, 0.);
			}

		}
		partitionFunctionValue += cardOfNotPassedValues;
	}

	private List<Attribute> computeAttributesWithinConnectedComponent() {
		Set<Attribute> attributeSet = new HashSet<>();
		for (MaxEntConstraint constraint : constraints) {
			for (Proposition proposition : ((FrequencyConstraint) constraint)
					.getAssociatedPropostions()) {
				attributeSet.add(proposition.getAttribute());
			}
		}
		return new ArrayList<>(attributeSet);
	}

	public List<MaxEntConstraint> getConstraints() {
		return constraints;
	}

	public double getPartitionFunctionValue() {
		return partitionFunctionValue;
	}

	public List<Attribute> getAttributesWithInConnectedComponent() {
		return attributesWithInConnectedComponent;
	}

	public double getSumOfMultipliersActivatedByConstraint(
			MaxEntConstraint constraint) {
		List<String> keys = ValueSpaceUtility
				.getKeysOfCombinationsContainConstriant(constraint,
						cardinalityOfPartitions.keySet(), constraints);
		double result = 0.;
		for (String key : keys) {
			result += constCombinationToPartitionValueMap.get(key);
		}
		return result;
	}

}
