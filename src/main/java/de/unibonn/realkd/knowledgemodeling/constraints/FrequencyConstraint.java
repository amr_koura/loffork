package de.unibonn.realkd.knowledgemodeling.constraints;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author bkang
 */
public class FrequencyConstraint implements MaxEntConstraint {

	private final Pattern pattern;

	private double multiplier;

	private final Set<Integer> rowIndices;

	private final Set<Integer> attributeIndices;

	private final List<Proposition> associatedPropositions;

	/*
	 * WARNING: only defined when there is a datatable
	 */
	public FrequencyConstraint(Pattern pattern) {
		this.pattern = pattern;
		this.multiplier = 0.;
		this.rowIndices = new HashSet<>(pattern.getSupportSet());
		this.attributeIndices = new HashSet<>();
		for (Attribute attribute : pattern.getAttributes()) {
			attributeIndices.add(((LogicallyDescribedLocalPattern) pattern).getPropositionalLogic().getDatatable()
					.getAttributes().indexOf(attribute));
		}

		this.associatedPropositions = new ArrayList<>();
		for (Proposition proposition : ((LogicallyDescribedLocalPattern) pattern).getDescription().getElements()) {
			associatedPropositions.add(proposition);
		}

	}

	public LogicallyDescribedLocalPattern getPattern() {
		return (LogicallyDescribedLocalPattern) pattern;
	}

	public List<Proposition> getAssociatedPropostions() {return associatedPropositions; }

	@Override
	public Set<Integer> getRowIndices() {
		return rowIndices;
	}

	@Override
	public Set<Integer> getAttributeIndices() {
		return attributeIndices;
	}

	@Override
	public double getMultiplier() {
		return multiplier;
	}

	@Override
	public void updateMultiplier(double newMultiplier) {
		this.multiplier = newMultiplier;
	}

	@Override
	public double getMeasurement() {
		return pattern.getValue(InterestingnessMeasure.FREQUENCY);
	}

	@Override
	public String getDescription() {
		return pattern.getClass().toString();
	}
}
