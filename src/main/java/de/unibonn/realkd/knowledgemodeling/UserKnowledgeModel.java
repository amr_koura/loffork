package de.unibonn.realkd.knowledgemodeling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.knowledgemodeling.constraints.MaxEntConstraint;
import de.unibonn.realkd.knowledgemodeling.learning.ConnectedComponent;
import de.unibonn.realkd.knowledgemodeling.learning.ValueSpaceUtility;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author bkang
 */
public class UserKnowledgeModel {

    private List<MaxEntConstraint> constraints;

    private List<ConnectedComponent> connectedComponents;

    private DataTable dataTable;

    public UserKnowledgeModel(DataTable dataTable, List<ConnectedComponent> connectedComponents) {
        this.dataTable = dataTable;
        this.constraints = new ArrayList<>();
        this.connectedComponents = connectedComponents;
    }

    public double getExpectedMeasurement(Pattern pattern) {
        List<Proposition> propositions = ((LogicallyDescribedLocalPattern) pattern).getDescription().getElements();
        if (constraints.size() == 0) {
            return ValueSpaceUtility.getInstance().getFractionOfSupportValuesOfPropSet(new HashSet<>(propositions), dataTable.getSize());
        } else {
            return computeExpectedMeasurementByQuery((LogicallyDescribedLocalPattern) pattern);
        }

    }

    public void update(List<MaxEntConstraint> constraints, List<ConnectedComponent> connectedComponents) {
        this.constraints = constraints;
        this.connectedComponents = connectedComponents;
    }

    private double computeExpectedMeasurementByQuery(LogicallyDescribedLocalPattern pattern) {
        Map<ConnectedComponent,Set<Proposition>> connectedComponentWisePropositionList = new HashMap<>();

        List<Proposition> independentPropositions = new ArrayList<>();
        for (Proposition proposition : pattern.getDescription().getElements()) {
            boolean isContained = false;
            for (ConnectedComponent connectedComponent : connectedComponents) {
                Attribute attribute = proposition.getAttribute();
                if (connectedComponent.getAttributesWithInConnectedComponent().contains(attribute)) {
                    if (!connectedComponentWisePropositionList.containsKey(connectedComponent)) {
                        connectedComponentWisePropositionList.put(connectedComponent, new HashSet<Proposition>());
                    }
                    connectedComponentWisePropositionList.get(connectedComponent).add(proposition);
                    isContained = true;
                }
            }
            if (!isContained) {
                independentPropositions.add(proposition);
            }
        }

        double measure = 1.;

        for (ConnectedComponent connectedComponent : connectedComponentWisePropositionList.keySet()) {
            measure *= connectedComponent.computeSumOfMultipliersActivatedByWildCardProps(connectedComponentWisePropositionList.get(connectedComponent))/connectedComponent.getPartitionFunctionValue();
        }

        for (Proposition proposition : independentPropositions) {
            Set<Proposition> propositionSet = new HashSet<>();
            propositionSet.add(proposition);
            measure *= ValueSpaceUtility.getFractionOfSupportValuesOfPropSet(propositionSet, dataTable.getSize());
        }

        return measure;
    }
}
