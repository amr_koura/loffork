package de.unibonn.realkd.knowledgemodeling.training;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcessObserver;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcessState;
import de.unibonn.realkd.knowledgemodeling.constraints.FrequencyConstraint;
import de.unibonn.realkd.knowledgemodeling.constraints.MaxEntConstraint;
import de.unibonn.realkd.knowledgemodeling.learning.KnowledgeModelLearner;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.DefaultAssociationBuilder;

/**
 * @author bkang
 */
public class KnowledgeModelTrainer implements DiscoveryProcessObserver {

	private KnowledgeModelLearner knowledgeModelLearner;

	private DiscoveryProcessState discoveryProcessState;

	private Set<Pattern> oldResultSnapShot;

	public KnowledgeModelTrainer(DataTable dataTable,
			DiscoveryProcessState discoveryProcessState) {
		this.knowledgeModelLearner = new KnowledgeModelLearner(dataTable);
		this.discoveryProcessState = discoveryProcessState;
		this.oldResultSnapShot = new HashSet<>();
		init(dataTable);
	}

	private void init(DataTable dataTable) {
		PropositionalLogic propositionalLogic = new PropositionalLogic(
				dataTable);
		List<MaxEntConstraint> constraints = new ArrayList<>();
		for (Proposition<?> proposition : propositionalLogic.getPropositions()) {
			@SuppressWarnings("rawtypes")
			List<Proposition> description = new ArrayList<>();
			description.add(proposition);
			FrequencyConstraint frequencyConstraint = new FrequencyConstraint(
					new DefaultAssociationBuilder().build(new Description(
							propositionalLogic, description)));
			knowledgeModelLearner.tellConstraint(frequencyConstraint);
			constraints.add(frequencyConstraint);

		}
		knowledgeModelLearner.doUpdate();
		for (MaxEntConstraint constraint : constraints) {
			System.out.println(constraint.getMeasurement()
					+ " "
					+ knowledgeModelLearner.getUserKnowledgeModel()
							.getExpectedMeasurement(
									((FrequencyConstraint) constraint)
											.getPattern()));
		}
	}

	@Override
	public void justBeganNewRound() {
		updateResultSnapshot();
	}

	@Override
	public void roundEnded() {
		Set<Pattern> newAddedPatterns = getAddedToResultThisRound();
		for (Pattern p : newAddedPatterns) {
			if (p.hasMeasure(InterestingnessMeasure.FREQUENCY)) {
				knowledgeModelLearner
						.tellConstraint(new FrequencyConstraint(p));
			}
		}
		knowledgeModelLearner.doUpdate();
	}

	private Set<Pattern> getAddedToResultThisRound() {
		Set<Pattern> newResults = new HashSet<>(
				discoveryProcessState.getResultPatterns());
		newResults.removeAll(oldResultSnapShot);
		return newResults;
	}

	private void updateResultSnapshot() {
		oldResultSnapShot.clear();
		oldResultSnapShot.addAll(discoveryProcessState.getResultPatterns());
	}

	public KnowledgeModelLearner getKnowledgeModelLearner() {
		return knowledgeModelLearner;
	}

	@Override
	public void markAsSeen(Pattern p) {

	}

	@Override
	public void aboutToSavePattern(Pattern p) {

	}

	@Override
	public void aboutToDeletePatternFromCandidates(Pattern p) {

	}

	@Override
	public void aboutToDeletePatternFromResults(Pattern p) {

	}
}
