/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.parameter;

import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.unibonn.realkd.common.logger.LogMessageType;

/**
 * <p>
 * Base class for parameters with static name and description that can also
 * depend on other "upstream" parameters. As long as there is at least one
 * invalid upstream parameter, this is also considered invalid.
 * </p>
 * 
 * <p>
 * Can be extended by specific parameters in order to provide convenience
 * constructor and/or to add functionality corresponding to specializations of
 * the MiningParameter interface.
 * </p>
 * 
 * @author mboley
 * 
 */
public class DefaultParameter<T> implements Parameter<T>, DependentParameter<T> {

	private T value = null;

	private final String name;

	private final String description;

	private final StringParser<T> stringParser;

	private final Class<?> type;

	private final List<ParameterListener> listener;

	private final Parameter<?>[] dependsOnParameters;

	private final String solutionHint;

	private final ValueValidator<? super T> validator;

	/**
	 * 
	 *            the object of type AbstractMiningAlgorithm that this parameter
	 *            is linked to
	 * @param initValue
	 *            value with which this parameter is initialized (may be null)
	 * @param parser
	 *            string parser that is used by implementation of setByString
	 *            method
	 */
	public DefaultParameter(String name, String description, Class<?> type,
			T initValue, StringParser<T> parser, ValueValidator<? super T> validator,
			String hint,
			@SuppressWarnings("rawtypes") Parameter... dependsOnParameters) {
		DEFAULT.log("mining_parameter", "creating parameter " + name
				+ " with parser " + parser, LogMessageType.DEBUG_MESSAGE);
		// this.algorithm = algorithm;
		this.name = name;
		this.description = description;
		this.value = initValue;
		this.stringParser = parser;
		this.type = type;
		this.listener = new ArrayList<>();
		this.validator = validator;
		this.dependsOnParameters = dependsOnParameters;
		this.solutionHint = hint;
	}

	@Override
	public final String getName() {
		return name;
	}

	@Override
	public final String getDescription() {
		return description;
	}

	@Override
	public final Class<?> getType() {
		return type;
	}

	@Override
	public final T getCurrentValue() {
		return value;
	}

	@Override
	public final void set(T newValue) {
		// encapsulated field is allowed to hold "null/not yet set/invalid"
		// values

		if ((newValue == null && value == null)) {
			return;
		}
		
		if (newValue!=null && newValue.equals(value)) {
			return;
		}

		this.value = newValue;

		for (ParameterListener dependentParameter : listener) {
			dependentParameter.notifyValueChanged(this);
		}
	}

	@Override
	public final void setByString(String... strings) {
		if (stringParser == null) {
			throw new UnsupportedOperationException("Parameter '" + getName()
					+ "' cannot be set by string.");
		}
		set(stringParser.parse(strings));
	}

	final StringParser<T> getStringParser() {
		return this.stringParser;
	}

	@Override
	public void addListener(ParameterListener dependentParameter) {
		checkNotNull(dependentParameter);
		this.listener.add(dependentParameter);
	}

	@SuppressWarnings("rawtypes")
	public final List<Parameter> getDependsOnParameters() {
		return new ArrayList<Parameter>(Arrays.asList(dependsOnParameters));
	}

	@Override
	public final boolean isContextValid() {
		for (Parameter<?> param : this.dependsOnParameters) {
			if (!param.isValid()) {
				DEFAULT.log(
						"mining_parameter",
						"invalid because '" + param.getCurrentValue()
								+ "' is invalid for '" + param.getName() + "'.",
						LogMessageType.DEBUG_MESSAGE);
				return false;
			}
		}
		return true;
	}

	@Override
	public final String getValueCorrectionHint() {
		return solutionHint;
	}

	@Override
	public final boolean isValid() {
		// can never be valid if context (parameter this depends on) is not
		// valid
		if (!isContextValid()) {
			return false;
		}
		// null value is never valid
		if (getCurrentValue() == null) {
			return false;
		}
		// values that are parameter containers themselves in invalid state can
		// never be valid
		if (getCurrentValue() instanceof ParameterContainer
				&& !((ParameterContainer) getCurrentValue()).isStateValid()) {
			return false;
		}
		return validator.valid(getCurrentValue());
	}

}
