/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common.parameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides default mutable implementation of interface ParameterContainer that
 * can be wrapped by classes that entail parameters.
 * 
 * @author mboley
 * 
 */
public class DefaultParameterContainer implements ParameterContainer {

	@SuppressWarnings("rawtypes")
	private final LinkedHashMap<String, Parameter> parameterMap = new LinkedHashMap<>();

	private final String containerName;

	public DefaultParameterContainer(String name) {
		this.containerName = name;
	}

	@Override
	public final Parameter<?> findParameterByName(String name) {
		Parameter<?> miningParameter = parameterMap.get(name);

		if (miningParameter == null) {
			for (Parameter<?> parameter : parameterMap.values()) {
				try {
					if (parameter.getCurrentValue() instanceof ParameterContainer) {
						miningParameter = ((ParameterContainer) parameter
								.getCurrentValue()).findParameterByName(name);
					}
				} catch (IllegalArgumentException ignored) {
					;
				} finally {
					if (miningParameter != null) {
						break;
					}
				}
			}
		}

		if (miningParameter == null) {
			throw new IllegalArgumentException("Parameter '" + name
					+ "' is unknown for '" + this.containerName + "'.");
		}

		return miningParameter;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Parameter> getTopLevelParameters() {
		List<Parameter> result = new ArrayList<>();

		for (String key : parameterMap.keySet()) {
			result.add(parameterMap.get(key));
		}

		return result;
	}

	@Override
	public boolean isStateValid() {
		for (Parameter<?> param : this.getTopLevelParameters()) {
			if (!param.isValid()) {
				return false;
			}
		}
		return true;
	}

	public void addParameter(Parameter<?> parameter) {
		if (parameterMap.containsKey(parameter.getName())) {
			throw new IllegalArgumentException(
					"Parameter with name is already present");
		}
		parameterMap.put(parameter.getName(), parameter);
	}

	public void addAllParameters(Collection<Parameter<?>> parameters) {
		for (Parameter<?> param : parameters) {
			this.addParameter(param);
		}
	}

	@Override
	public void passValuesToParameters(final Map<String, String[]> nameValueMap) {
		Map<String, String[]> crossOutMap = new HashMap<>(nameValueMap);
		this.unloadMapValuesToParameters(crossOutMap);
	}

	@Override
	public void unloadMapValuesToParameters(
			final Map<String, String[]> crossOutMap) {
		// traverser in order provided by getParameters
		for (Parameter<?> parameter : this.getTopLevelParameters()) {
			if (crossOutMap.containsKey(parameter.getName())) {
				String[] value = crossOutMap.get(parameter.getName());
				try {
					parameter.setByString(value);
				} catch (IllegalArgumentException illegalArgumentException) {
					String valueString = value[0];
					for (int i = 1; i < value.length; i++) {
						valueString += ("," + value[i]);
					}
					System.out.println("Warning: '" + valueString
							+ "' could not be parsed for '"
							+ parameter.getName() + "'");

				}
				// cross out used parameter
				crossOutMap.remove(parameter.getName());
			}

			// recursion into current value if itself parameter container
			if (parameter.getCurrentValue() instanceof ParameterContainer) {
				((ParameterContainer) parameter.getCurrentValue())
						.unloadMapValuesToParameters(crossOutMap);
			}
		}

	}

}
