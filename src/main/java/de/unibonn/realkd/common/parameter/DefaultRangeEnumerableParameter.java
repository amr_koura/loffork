/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.parameter;

import java.util.Arrays;
import java.util.List;

/**
 * Default implementation for RangeEnumerableParameters. Per definition the
 * value is valid whenever it is an element of the current set of options.
 * 
 * This check is currently performed by string equivalence (through the toString
 * method).
 * 
 * Concrete range is computed by an object of the inner type
 * {@link RangeComputer} and is buffered. Range is refreshed whenever value of
 * dependent parameters change.
 * 
 * @author mboley
 * 
 */
public class DefaultRangeEnumerableParameter<T> implements
		RangeEnumerableParameter<T>, DependentParameter<T>, ParameterListener {

	private class ElementOfRangeParser implements StringParser<T> {

		@Override
		public T parse(String... strings) {
			if (strings.length != 1) {
				throw new IllegalArgumentException(
						"Can parse only exactly one string argument.");
			}
			for (T element : DefaultRangeEnumerableParameter.this.getRange()) {
				if (element.toString().equals(strings[0])) {
					return element;
				}
			}
			throw new IllegalArgumentException("Could not find element '"
					+ strings[0] + "' in range.");
		}
	}

	private class ValidatorIsElementOfRange implements ValueValidator<T> {

		@Override
		public boolean valid(T value) {
			return DefaultRangeEnumerableParameter.this.getRange().contains(
					value);
		}

	}

	public interface RangeComputer<T> {

		/**
		 * Can be called whenever context is valid.
		 * 
		 * @return non-null range
		 */
		public List<T> computeRange();

	}

	private static final String HINT = "Choose an element from list";

	private final DefaultParameter<T> defaultParameter;

	private final RangeComputer<T> rangeComputer;

//	private List<T> currentRange = Arrays.asList();

	/**
	 * Calls range computation for the first time if context is valid and
	 * buffers its result. Also, if range is non-empty and current value is
	 * null, sets first option as current value.
	 */
	public DefaultRangeEnumerableParameter(String name, String description,
			Class<?> type, RangeComputer<T> rangeComputer,
			Parameter<?>... dependenParams) {
		for (Parameter<?> parameter : dependenParams) {
			parameter.addListener(this);
		}
		this.rangeComputer = rangeComputer;
		this.defaultParameter = new DefaultParameter<T>(name, description,
				type, null, new ElementOfRangeParser(),
				new ValidatorIsElementOfRange(), HINT, dependenParams);
		tryToInitValueIfCurrentlyNull();
	}
	
	private void tryToInitValueIfCurrentlyNull() {
		List<T> currentRange = this.getRange();
		if (getCurrentValue() == null && !currentRange.isEmpty()) {
			this.set(currentRange.get(0));
		}
	}

	/**
	 * Returns the buffered range.
	 */
	@Override
	public final List<T> getRange() {
		if (!isContextValid()) {
			return Arrays.asList();
		}
		return rangeComputer.computeRange();
	}

	@Override
	public final boolean isContextValid() {
		return this.defaultParameter.isContextValid();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public final List<Parameter> getDependsOnParameters() {
		return this.defaultParameter.getDependsOnParameters();
	}

	@Override
	public final boolean isValid() {
		return this.defaultParameter.isValid();
	}

	@Override
	public final String getValueCorrectionHint() {
		return this.defaultParameter.getValueCorrectionHint();
	}

	@Override
	public final String getName() {
		return this.defaultParameter.getName();
	}

	@Override
	public final String getDescription() {
		return this.defaultParameter.getDescription();
	}

	@Override
	public final Class<?> getType() {
		return this.defaultParameter.getType();
	}

	@Override
	public final void set(T value) {
		this.defaultParameter.set(value);
	}

	@Override
	public final void setByString(String... value) {
		this.defaultParameter.setByString(value);
	}

	@Override
	public final T getCurrentValue() {
		return defaultParameter.getCurrentValue();
	}

	@Override
	public final void addListener(final ParameterListener listener) {
		// adding referrer that notifies listener with update for this parameter
		// (instead of wrapped)
		this.defaultParameter.addListener(new ParameterListener() {
			@Override
			public void notifyValueChanged(Parameter<?> parameter) {
				listener.notifyValueChanged(DefaultRangeEnumerableParameter.this);
			}
		});
	}

	/**
	 * Refreshes the buffered range.
	 */
	public final void notifyValueChanged(Parameter<?> parameter) {
		tryToInitValueIfCurrentlyNull();
	}

}
