/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.parameter;

import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.unibonn.realkd.common.logger.LogMessageType;

/**
 * Implementation should be changed in future version to mimic more flexible
 * approach of DefaultRangeEnumerableParameter.
 * 
 * @author mboley
 * 
 */
public abstract class AbstractNonEmptySubListParameter<T extends List<?>>
		implements SubListParameter<T>, DependentParameter<T> {

	public class SublistOfRangeParser<T extends List> implements
			StringParser<T> {

		@Override
		public T parse(String... strings) {
			DEFAULT.log("mining_parameter",
					"parse strings " + Arrays.toString(strings),
					LogMessageType.DEBUG_MESSAGE);

			T result = (T) new ArrayList<>();
			for (String string : strings) {
				boolean found = false;
				for (Object element : AbstractNonEmptySubListParameter.this
						.getList()) {
					if (element.toString().equals(string)) {
						found = true;
						result.add(element);
						break;
					}
				}
				if (!found) {
					throw new IllegalArgumentException("cannot parse " + string
							+ " (not in list)");
				}

			}
			DEFAULT.log("mining_parameter", "sublist parsed: " + result,
					LogMessageType.DEBUG_MESSAGE);
			return result;
		}

	}

	public class NonEmptySubListOfRangeValidator implements ValueValidator<T> {

		@Override
		public boolean valid(T value) {
			if (value.isEmpty()) {
				return false;
			}
			for (Object element : value) {
				if (!AbstractNonEmptySubListParameter.this.getList().contains(
						element)) {
					return false;
				}
			}
			return true;
		}

	}

	private final DefaultParameter<T> defaultParameter;

	public AbstractNonEmptySubListParameter(String name, String description,
			Class<?> type, T initialValue, String hint,
			Parameter<?>... dependenParams) {
		this.defaultParameter = new DefaultParameter<T>(name, description,
				type, initialValue, new SublistOfRangeParser<T>(),
				new NonEmptySubListOfRangeValidator(), hint, dependenParams);
	}

	@Override
	public final T getList() {
		DEFAULT.log("mining_parameter", "range requested",
				LogMessageType.DEBUG_MESSAGE);
		if (!isContextValid()) {
			return (T) new ArrayList<>();
		}
		T concreteRange = getConcreteRange();

		return concreteRange;
	}

	protected abstract T getConcreteRange();

	@Override
	public boolean isContextValid() {
		return this.defaultParameter.isContextValid();
	}

	@Override
	public List<Parameter> getDependsOnParameters() {
		return this.defaultParameter.getDependsOnParameters();
	}

	@Override
	public boolean isValid() {
		return this.defaultParameter.isValid();
	}

	@Override
	public String getValueCorrectionHint() {
		return this.defaultParameter.getValueCorrectionHint();
	}

	@Override
	public String getName() {
		return this.defaultParameter.getName();
	}

	@Override
	public String getDescription() {
		return this.defaultParameter.getDescription();
	}

	@Override
	public Class<?> getType() {
		return this.defaultParameter.getType();
	}

	@Override
	public void set(T value) {
		this.defaultParameter.set(value);
	}

	@Override
	public void setByString(String... value) {
		this.defaultParameter.setByString(value);
	}

	@Override
	public T getCurrentValue() {
		return this.defaultParameter.getCurrentValue();
	}

	@Override
	public void addListener(final ParameterListener listener) {
		// adding referrer that notifies listener with update for this parameter
		// (instead of wrapped)
		this.defaultParameter.addListener(new ParameterListener() {
			@Override
			public void notifyValueChanged(Parameter<?> parameter) {
				listener.notifyValueChanged(AbstractNonEmptySubListParameter.this);
			}
		});
	}

}
