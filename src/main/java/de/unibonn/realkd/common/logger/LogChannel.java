/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.logger;

public enum LogChannel {

	DATA_INITIALIZATION("data_initialization"), DISCOVERY_PROCESS(
			"discovery_process"), MINING_ENGINE("mining_engine"), UTILITY_MODEL_LEARNING(
			"utility_model_learning"), KNOWLEDGE_MODEL_LEARNING(
			"knowledge_model_learning"), UI("ui"), EXPERIMENT("experiment"), DEFAULT(
			"default"), CUSTOM_DASHBOARD("custom_dashboard");

	private final String channelName;

	LogChannel(String channelName) {
		this.channelName = channelName;
	}

	public String toString() {
		return channelName;
	}

	public void log(String logMessage, LogMessageType type) {
	    	// Logging has been disabled for now since it caused problems when realKD is
	    	// used in Creedo as a jar

		//Logger.logFromChannel(this.toString(), logMessage, type);
	}

	public void log(String superChannel, String logMessage, LogMessageType type) {
	    // Logging has been disabled for now since it caused problems when realKD is
	    // used in Creedo as a jar

	    // Logger.logFromChannel(superChannel, this.toString(), logMessage, type);
	}

}
