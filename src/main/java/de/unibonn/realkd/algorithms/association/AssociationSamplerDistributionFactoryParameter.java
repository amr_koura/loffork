/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.association;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.sampling.DistributionFactory;
import de.unibonn.realkd.algorithms.sampling.FrequencyDistributionFactory;
import de.unibonn.realkd.algorithms.sampling.RareDistributionTimesPowerOfFrequencyFactory;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;

public class AssociationSamplerDistributionFactoryParameter extends
		DefaultRangeEnumerableParameter<DistributionFactory> implements
		RangeEnumerableParameter<DistributionFactory> {

	private static final Class<DistributionFactory> TYPE = DistributionFactory.class;
	private static final String DESCRIPTION = "The distribution on the pattern space that is used to generate random seeds for Association pattern search";
	private static final String NAME = "Association sampling distribution";
	private static List<DistributionFactory> OPTIONS = new ArrayList<>();
	static {
		OPTIONS.add(new FrequencyDistributionFactory(1));
		OPTIONS.add(new FrequencyDistributionFactory(2));
		OPTIONS.add(new FrequencyDistributionFactory(3));
		OPTIONS.add(new RareDistributionTimesPowerOfFrequencyFactory(1));
		OPTIONS.add(new RareDistributionTimesPowerOfFrequencyFactory(2));
		OPTIONS.add(new RareDistributionTimesPowerOfFrequencyFactory(3));
	}

//	private static final DistributionFactory DEFAULT = !OPTIONS.isEmpty() ? OPTIONS
//			.get(0) : null;

	public AssociationSamplerDistributionFactoryParameter() {
		super(NAME, DESCRIPTION, TYPE, new RangeComputer<DistributionFactory>() {

			@Override
			public List<DistributionFactory> computeRange() {
				return OPTIONS;
			}
		});
	}

//	@Override
//	protected final List<DistributionFactory> getConcreteRange() {
//		return OPTIONS;
//	}

}
