/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.association;

import java.util.Collection;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.common.FreePropositionalLogicParameter;
import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.algorithms.sampling.ConsaptBasedSamplingMiner;
import de.unibonn.realkd.algorithms.sampling.DistributionFactory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.DefaultAssociationBuilder;

public class AssociationSampler extends AbstractMiningAlgorithm {

	private final ConsaptBasedSamplingMiner samplingMiner;

	public AssociationSampler(DataWorkspace workspace) {
		super();

		FreePropositionalLogicParameter propositionalLogicParameter = new FreePropositionalLogicParameter(
				workspace);

		// Instantiate wrapped sampler
		this.samplingMiner = new ConsaptBasedSamplingMiner(
				new DefaultAssociationBuilder(), propositionalLogicParameter,
				new AssociationSamplerDistributionFactoryParameter(),
				new AssociationTargetFunctionParameter());

		registerParameter(samplingMiner.getPropositionalLogicParameter());
		registerParameter(samplingMiner.getDistributionFactoryParameter());
		registerParameter(samplingMiner.getNumberOfResultsParameter());
		registerParameter(samplingMiner.getPostProcessorParameter());
		registerParameter(samplingMiner.getTargetFunctionParameter());
	}

	@Override
	protected Collection<Pattern> concreteCall() {
		return samplingMiner.call();
	}

	@Override
	protected void onStopRequest() {
		samplingMiner.requestStop();
	}

	public void setDistributionFactory(DistributionFactory distributionFactory) {
		this.samplingMiner.getDistributionFactoryParameter().set(
				distributionFactory);
	}

	@Override
	public String toString() {
		return "AssociationSampler|"
				+ samplingMiner.getDistributionFactoryParameter()
						.getCurrentValue() + "|"
				+ samplingMiner.getPostProcessorParameter().getCurrentValue();
	}

	@Override
	public String getName() {
		return "Association Sampler";
	}

	@Override
	public String getDescription() {
		return "Direct Association Sampler";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.ASSOCIATION_MINING;
	}

	// public Parameter<Comparator<Pattern>> getOptimizationOrderParameter() {
	// return samplingMiner.getOptimizationFunctionParameter();
	// }

	public void setNumberOfResults(int numberOfResults) {
		this.samplingMiner.getNumberOfResultsParameter().set(numberOfResults);
	}

	public Parameter<PatternOptimizationFunction> getTargetFunctionParameter() {
		return this.samplingMiner.getTargetFunctionParameter();
	}
}
