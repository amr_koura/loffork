package de.unibonn.realkd.algorithms.common;

import de.unibonn.realkd.data.table.AttributeGroupsStore;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Pattern;

public interface PatternConstraint {

	public abstract boolean satisfies(Pattern pattern);

	public static PatternConstraint POSITIVE_FREQUENCY = new PatternConstraint() {

		@Override
		public boolean satisfies(Pattern pattern) {
			if (!(pattern instanceof LogicallyDescribedLocalPattern)) {
				throw new IllegalArgumentException(
						"Constraint only defined for logically described pattern");
			}
			return ((LogicallyDescribedLocalPattern) pattern).getFrequency() > 0;
		}
	};

	public static PatternConstraint DESCRIPTOR_DOES_NOT_CONTAIN_TWO_ELEMENTS_REFERRING_TO_SAME_META_ATTRIBUTE = new PatternConstraint() {

		@Override
		public boolean satisfies(Pattern pattern) {
			if (!(pattern instanceof LogicallyDescribedLocalPattern)) {
				throw new IllegalArgumentException(
						"Constraint only defined for logically described pattern");
			}
			LogicallyDescribedLocalPattern logicalPattern = (LogicallyDescribedLocalPattern) pattern;
			for (int i = 0; i < logicalPattern.getDescription().getElements()
					.size(); i++) {
				for (int j = i + 1; j < logicalPattern.getDescription()
						.getElements().size(); j++) {
					AttributeGroupsStore groupStore = logicalPattern
							.getPropositionalLogic().getDatatable()
							.getAttributeGroupsStore();
					if (groupStore.isPartOfMacroAttributeWith(logicalPattern
							.getDescription().getElement(i).getAttribute(),
							logicalPattern.getDescription().getElement(j)
									.getAttribute())) {

						return false;

					}
				}
			}
			return true;
		}
	};

	public static class MinimumMeasureValeConstraint implements
			PatternConstraint {

		private final double threshold;

		private final InterestingnessMeasure measure;

		public MinimumMeasureValeConstraint(
				InterestingnessMeasure interestingnessMeasure, double threshold) {
			this.threshold = threshold;
			this.measure = interestingnessMeasure;
		}

		@Override
		public boolean satisfies(Pattern pattern) {
			if (!pattern.hasMeasure(measure)) {
				throw new IllegalArgumentException(
						"Constraint only defined for patterns that have "
								+ measure.getName());
			}
			return pattern.getValue(measure) >= threshold;
		}

	}

}
