/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.common;

import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

/**
 * @author mboley
 * 
 */
public class MinimumFrequencyThresholdParameter extends
		DefaultParameter<Double> {

	private static final String HINT = "Must be between 0 and 1.";

	private static final double DEFAULT = 0.1;

	private static final String DESCRIPTION = "The minimum frequency a pattern has to have in order to be part of the result.";

	private static final String NAME = "Frequency threshold";

	public MinimumFrequencyThresholdParameter() {
		super(NAME, DESCRIPTION, Double.class, DEFAULT,
				StringParser.DOUBLE_PARSER, new ValueValidator<Double>() {

					@Override
					public boolean valid(Double value) {
						return (value <= 1.0 && value >= 0.0);
					}
				}, HINT);
	}

	// @Override
	// protected boolean concretelyValid() {
	// return (getCurrentValue() <= 1.0 && getCurrentValue() >= 0.0);
	// }

}
