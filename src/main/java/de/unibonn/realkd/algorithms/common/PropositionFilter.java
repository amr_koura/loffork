package de.unibonn.realkd.algorithms.common;

import java.util.List;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * Interface for proposition filter strategies. Can be used for algorithm that
 * want to allow the specification of a customized subset of propositions to be
 * used for mining.
 * 
 * @author mboley
 */
public interface PropositionFilter {

	@SuppressWarnings("rawtypes")
	public List<Proposition> filter(PropositionalLogic propositionalLogic);

	/**
	 * Default proposition filter that does not filter out any proposition.
	 */
	public static PropositionFilter NO_FILTER = new PropositionFilter() {
		@SuppressWarnings("rawtypes")
		@Override
		public List<Proposition> filter(PropositionalLogic propositionalLogic) {
			return propositionalLogic.getPropositions();
		}
	};

}
