package de.unibonn.realkd.algorithms.common;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

/**
 * Propositional logic parameter that accepts propositional logics as values
 * that are linked to the same datatable as given by the current value of a
 * specific datatable parameter.
 * 
 * @see FreePropositionalLogicParameter
 * 
 * @author mboley
 * 
 */
public class MatchingPropositionalLogicParameter extends
		DefaultRangeEnumerableParameter<PropositionalLogic> {

	private static final String DESCRIPTION = "The collections of basic statements avaible to the algorithm to construct patterns.";

	private static final String NAME = "Propositions";

	public MatchingPropositionalLogicParameter(
			final DataWorkspace dataWorkspace,
			final Parameter<DataTable> datatableParameter) {
		super(NAME, DESCRIPTION, PropositionalLogic.class,
				new RangeComputer<PropositionalLogic>() {
					@Override
					public List<PropositionalLogic> computeRange() {
						List<PropositionalLogic> result = new ArrayList<>();
						for (PropositionalLogic logic : dataWorkspace
								.getAllPropositionalLogics()) {
							if (logic.getDatatable() == datatableParameter
									.getCurrentValue()) {
								result.add(logic);
							}
						}
						return result;
					}
				}, datatableParameter);
	}

}
