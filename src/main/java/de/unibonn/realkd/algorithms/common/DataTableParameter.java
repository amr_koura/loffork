/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.common;

import java.util.List;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.table.DataTable;

public class DataTableParameter extends
		DefaultRangeEnumerableParameter<DataTable> {

	private static final String DESCRIPTION = "The input data for the algorithm.";

	private static final String NAME = "Datatable";

	private final DataWorkspace dataWorkspace;

	// private static DataTable getFirstDataTableWorkspace(DataWorkspace
	// dataWorkspace) {
	// List<DataTable> dataTables = dataWorkspace.getAllDatatables();
	// return dataTables.size() == 0 ? null : dataTables.get(0);
	// }

	public DataTableParameter(final DataWorkspace dataWorkspace) {
		super(NAME, DESCRIPTION, DataTable.class, new RangeComputer<DataTable>() {

			@Override
			public List<DataTable> computeRange() {
				return dataWorkspace.getAllDatatables();
			}
		});
		if (dataWorkspace == null) {
			throw new IllegalArgumentException(
					"Data workspace must not be null");
		}
		this.dataWorkspace = dataWorkspace;
	}

//	@Override
//	protected List<DataTable> getConcreteRange() {
//		return dataWorkspace.getAllDatatables();
//	}
}
