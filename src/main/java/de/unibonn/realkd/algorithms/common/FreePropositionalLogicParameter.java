package de.unibonn.realkd.algorithms.common;

import java.util.List;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * Propositional logic parameter that accepts any propositional logic in some
 * given data workspace.
 * 
 * @see MatchingPropositionalLogicParameter
 * 
 * @author mboley
 * 
 */
public class FreePropositionalLogicParameter extends
		DefaultRangeEnumerableParameter<PropositionalLogic> {

	private static final String DESCRIPTION = "The collections of basic statements avaible to the algorithm to construct patterns.";

	private static final String NAME = "Propositions";

	public FreePropositionalLogicParameter(final DataWorkspace dataWorkspace) {
		super(NAME, DESCRIPTION, PropositionalLogic.class,
				new RangeComputer<PropositionalLogic>() {
					@Override
					public List<PropositionalLogic> computeRange() {
						return dataWorkspace.getAllPropositionalLogics();
					}
				});
	}

}
