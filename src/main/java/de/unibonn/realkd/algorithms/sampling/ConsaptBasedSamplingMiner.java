/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.sampling;

import static de.unibonn.realkd.common.logger.LogChannel.MINING_ENGINE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import mime.plain.PlainItem;
import mime.plain.PlainItemSet;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.common.NumberOfResultsParameter;
import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternFromLogicalDescriptionBuilder;
import edu.uab.consapt.sampling.TwoStepPatternSampler;

public class ConsaptBasedSamplingMiner<T extends LogicallyDescribedLocalPattern>
		extends AbstractMiningAlgorithm {

	private final PatternFromLogicalDescriptionBuilder<T> patternBuilder;
	private final NumberOfResultsParameter numberOfResults;
	private final SamplingPostProcessorParameter postProcessorParameter;
	private final Parameter<PropositionalLogic> propositionalLogicParameter;
	private final Parameter<DistributionFactory> distributionFactoryParameter;
	private final Parameter<PatternOptimizationFunction> targetFunctionParameter;

	/**
	 * Keeps a reference to the current pattern sampler during the execution of
	 * the run method, which can thereby be accessed from another thread through
	 * the setStop().
	 */
	private TwoStepPatternSampler sampler;

	public ConsaptBasedSamplingMiner(
			PatternFromLogicalDescriptionBuilder<T> patternBuilder,
			Parameter<PropositionalLogic> propositionalLogicParameter,
			RangeEnumerableParameter<DistributionFactory> distributionFactoryParameter,
			Parameter<PatternOptimizationFunction> targetFunctionParameter) {

		this.patternBuilder = patternBuilder;

		this.propositionalLogicParameter = propositionalLogicParameter;
		this.postProcessorParameter = new SamplingPostProcessorParameter();
		this.distributionFactoryParameter = distributionFactoryParameter;
		this.targetFunctionParameter = targetFunctionParameter;
		this.numberOfResults = new NumberOfResultsParameter();

		// Register parameters
		registerParameter(this.propositionalLogicParameter);
		registerParameter(this.postProcessorParameter);
		registerParameter(this.distributionFactoryParameter);
		registerParameter(this.targetFunctionParameter);
		registerParameter(this.numberOfResults);

		// Set default values
		this.numberOfResults.set(100);
		this.postProcessorParameter
				.set(SinglePatternPostProcessor.OPPORTUNISTIC_LINEAR_PATTERNPRUNER);
	}

	public final Parameter<PropositionalLogic> getPropositionalLogicParameter() {
		return this.propositionalLogicParameter;
	}

	public final PropositionalLogic getPropositionalLogic() {
		return this.propositionalLogicParameter.getCurrentValue();
	}

	private void setSampler(TwoStepPatternSampler sampler) {
		this.sampler = sampler;
	}

	@Override
	protected void onStopRequest() {
		if (this.sampler != null) {
			this.sampler.setStop(true);
		}
	}

	public NumberOfResultsParameter getNumberOfResultsParameter() {
		return numberOfResults;
	}

	public int getMaxNumResults() {
		return numberOfResults.getCurrentValue();
	}

	public void setNumberOfResults(int numberOfResults) {
		this.numberOfResults.set(numberOfResults);
	}

	public SamplingPostProcessorParameter getPostProcessorParameter() {
		return postProcessorParameter;
	}

	public void setPostProcessorParameter(
			SinglePatternPostProcessor postProcessorParameter) {
		this.postProcessorParameter.set(postProcessorParameter);
	}

	public final Collection<Pattern> concreteCall() {
		MINING_ENGINE.log("sampling_miner", "Starting mine process",
				LogMessageType.DEBUG_MESSAGE);

		Collection<Pattern> results = new HashSet<>();

		// Read values from parameters
		DistributionFactory distributionFactory = distributionFactoryParameter
				.getCurrentValue();
		// Comparator<Pattern> pruningCondition = optimizationOrderParameter
		// .getCurrentValue();
		PropositionalLogic propositionalLogic = propositionalLogicParameter
				.getCurrentValue();
		SinglePatternPostProcessor postProcessor = postProcessorParameter
				.getCurrentValue();

		try {
			setSampler(distributionFactory.getDistribution(propositionalLogic));

			while (!stopRequested() && results.size() != getMaxNumResults()) {
				// Create plain pattern
				PlainItemSet plainPattern = sampler.getNext();

				if (plainPattern == null || stopRequested()) {
					continue;
				}

				@SuppressWarnings("rawtypes")
				List<Proposition> propositions = new ArrayList<>();

				for (PlainItem item : plainPattern) {
					Proposition<?> proposition = propositionalLogic
							.getPropositions().get(
									Integer.parseInt(item.getName()));
					propositions.add(proposition);
				}

				// Now create Description object using the propositions
				Description description = new Description(propositionalLogic,
						propositions);

				// use builder here instead of getPattern(plainPattern)
				LogicallyDescribedLocalPattern pattern = patternBuilder
						.build(description);

				pattern = postProcessor.prune(pattern,
						targetFunctionParameter.getCurrentValue(),
						patternBuilder);

				// check stop condition before adding pattern to the results
				// list to avoid concurrent modification
				//
				// Mario: I think think the following two lines can stay
				// commented out now since we moved to Callable and no external
				// access to results is possible until the thread running this
				// code reaches return statement
				//
				// if (stopRequested()) {
				// continue;
				// }

				results.add(pattern);
			}
		} finally {
			MINING_ENGINE.log("sampling_miner", "Freeing ressources",
					LogMessageType.DEBUG_MESSAGE);
			setSampler(null);
		}

		return results;
	}

	@Override
	public String getName() {
		return "Consapt based sampling algorithm";
	}

	@Override
	public String getDescription() {
		return "General consapt based sampling algorithm that can be wrapped by more specific implementations";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.OTHER;
	}

	// public Parameter<Comparator<Pattern>> getOptimizationFunctionParameter()
	// {
	// return optimizationOrderParameter;
	// }

	public Parameter<DistributionFactory> getDistributionFactoryParameter() {
		return distributionFactoryParameter;
	}

	public Parameter<PatternOptimizationFunction> getTargetFunctionParameter() {
		return targetFunctionParameter;
	}
}