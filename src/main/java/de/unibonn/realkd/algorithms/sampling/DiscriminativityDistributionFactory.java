/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.sampling;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mime.plain.weighting.PosNegDbInterface;
import de.unibonn.realkd.algorithms.emm.PosNegDatabaseCreator;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import edu.uab.consapt.sampling.TwoStepPatternSampler;
import edu.uab.consapt.sampling.TwoStepPatternSamplerFactory;

public class DiscriminativityDistributionFactory implements
		DistributionFactory, ParameterContainer {

	private final int powerOfFrequency;
	private final int powerOfPosFrequency;
	private final int powerOfNegFrequency;

	@SuppressWarnings("rawtypes")
	private final Parameter<List<Attribute>> targetAttributesParameter;

	private final Parameter<PosNegDatabaseCreator> dBCreatorParameter;
	private final DefaultParameterContainer parameterContainer;

	public DiscriminativityDistributionFactory(
			@SuppressWarnings("rawtypes") final Parameter<List<Attribute>> targetAttributesParameter,
			int powerOfFrequency,
			int powerOfPosFrequency, int powerOfNegFrequency) {

		this.targetAttributesParameter = targetAttributesParameter;
		this.powerOfFrequency = powerOfFrequency;
		this.powerOfPosFrequency = powerOfPosFrequency;
		this.powerOfNegFrequency = powerOfNegFrequency;

		this.dBCreatorParameter = new DefaultRangeEnumerableParameter<PosNegDatabaseCreator>(
				"Splitting method",
				"The way data is split into a positive and a negative portion",
				PosNegDatabaseCreator.class,
				new RangeComputer<PosNegDatabaseCreator>() {

					@Override
					public List<PosNegDatabaseCreator> computeRange() {
						List<PosNegDatabaseCreator> result = new ArrayList<>();
						if (targetAttributesParameter.getCurrentValue().size() > 0) {
							result.add(PosNegDatabaseCreator.PosNegDatabaseByFirstAttribute.INSTANCE);
						}
						if (targetAttributesParameter.getCurrentValue().size() > 1) {
							result.add(PosNegDatabaseCreator.PosNegDatabaseByFirstTwoAttributes.INSTANCE);
						}
						if (targetAttributesParameter.getCurrentValue().size() > 1
								&& allMetric(targetAttributesParameter
										.getCurrentValue())) {
							result.add(PosNegDatabaseCreator.PosNegDatabaseUsingPCA.INSTANCE);
						}

						return result;
					}

					private boolean allMetric(
							@SuppressWarnings("rawtypes") List<Attribute> attributes) {
						for (Attribute<?> attribute : attributes) {
							if (!(attribute instanceof MetricAttribute)) {
								return false;
							}
						}
						return true;
					}
				}, targetAttributesParameter);
		this.parameterContainer = new DefaultParameterContainer(
				"DiscriminativityDistributionFactory");
		this.parameterContainer.addParameter(dBCreatorParameter);
	}

	@Override
	public TwoStepPatternSampler getDistribution(
			PropositionalLogic propositionalLogic) {

		// PosNegDbInterface db = posNegDatabase.createDb(propositionalLogic,
		// targetAttributes);

		PosNegDbInterface db = dBCreatorParameter.getCurrentValue()
				.createDb(propositionalLogic,
						targetAttributesParameter.getCurrentValue());

		try {
			return TwoStepPatternSamplerFactory
					.createDiscrTimesFreqDistribution(db, powerOfFrequency,
							powerOfPosFrequency - 1, powerOfNegFrequency - 1);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public String toString() {
		return "P=" + ((powerOfPosFrequency > 0) ? "pfreq" : "")
				+ ((powerOfPosFrequency > 1) ? "^" + powerOfPosFrequency : "")
				+ ((powerOfNegFrequency > 0) ? "(1-nfreq)" : "")
				+ ((powerOfNegFrequency > 1) ? "^" + powerOfNegFrequency : "")
				+ ((powerOfFrequency > 0) ? "freq" : "")
				+ ((powerOfFrequency > 1) ? "^" + powerOfFrequency : "");
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DiscriminativityDistributionFactory))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiscriminativityDistributionFactory other = (DiscriminativityDistributionFactory) obj;
		if (this.powerOfFrequency == other.powerOfFrequency
				&& this.powerOfNegFrequency == other.powerOfNegFrequency
				&& this.powerOfPosFrequency == other.powerOfPosFrequency)
			// && this.propositionalLogic.equals(other.propositionalLogic))
			// && this.targetAttributes.equals(other.targetAttributes))
			return true;
		return false;
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return parameterContainer.findParameterByName(name);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Parameter> getTopLevelParameters() {
		return parameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return parameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		this.parameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(
			final Map<String, String[]> crossOutMap) {
		this.parameterContainer.unloadMapValuesToParameters(crossOutMap);
	}
}
