/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.sampling;

import java.util.List;

import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.PatternFromLogicalDescriptionBuilder;

/**
 * Collection of post-processors that can be used to prune sampled candidate
 * patterns.
 * 
 * @author mboley
 * 
 */
public enum SinglePatternPostProcessor {

	/**
	 * Fixes random order of descriptor elements and tries to remove each
	 * element in that order. Element removed if resulting pattern quality is
	 * not smaller (according to comparator argument) than of pattern with
	 * element.
	 * 
	 */
	OPPORTUNISTIC_LINEAR_PATTERNPRUNER {
		@Override
		public LogicallyDescribedLocalPattern prune(
				LogicallyDescribedLocalPattern origin,
				PatternOptimizationFunction optimizationFunction,
				PatternFromLogicalDescriptionBuilder<?> builder) {
			LogicallyDescribedLocalPattern current = origin;
			List<Integer> priorityList = Sampling.getPermutation(origin
					.getDescription().size());
			for (Integer index : priorityList) {
				// LogicallyDescribedLocalPattern candidate = current
				// .getNewPatternWithDescription(current.getDescription()
				// .getGeneralization(
				// origin.getDescription().getElements()
				// .get(index)));
				LogicallyDescribedLocalPattern candidate = builder
						.build(current.getDescription().getGeneralization(
								origin.getDescription().getElements()
										.get(index)));

				if (optimizationFunction.value(current) <= optimizationFunction
						.value(candidate)) {
					current = candidate;
				}
			}
			return current;
		}

		@Override
		public String toString() {
			return "LinearRandomPruner";
		}
	},

	/**
	 * Trivial pruner that does not change the input pattern and returns the
	 * same object.
	 * 
	 */
	NO_POSTPROCESSOR {
		@Override
		public LogicallyDescribedLocalPattern prune(
				LogicallyDescribedLocalPattern origin,
				PatternOptimizationFunction optimizationFunction,
				PatternFromLogicalDescriptionBuilder<?> builder) {
			return origin;
		}

		@Override
		public String toString() {
			return "NoPostProcessor";
		}
	};

	/**
	 * 
	 * @param origin
	 *            the seed for the pruning procedure
	 * @param optimizationFunction
	 *            the function to maximize by the pruning procedure
	 * @return
	 */
	public abstract LogicallyDescribedLocalPattern prune(
			LogicallyDescribedLocalPattern origin,
			PatternOptimizationFunction optimizationFunction,
			PatternFromLogicalDescriptionBuilder<?> builder);

}