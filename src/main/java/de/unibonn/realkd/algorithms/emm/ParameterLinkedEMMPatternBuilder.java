/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.emm;

import de.unibonn.realkd.algorithms.common.FreePropositionalLogicParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.PatternFromLogicalDescriptionBuilder;
import de.unibonn.realkd.patterns.emm.DefaultExceptionalModelPatternBuilder;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;

/**
 * 
 * @author mboley
 * 
 */
public class ParameterLinkedEMMPatternBuilder implements
		PatternFromLogicalDescriptionBuilder<ExceptionalModelPattern> {

	private final Parameter<PropositionalLogic> propositionalLogicParameter;

	private final TargetAttributesParameter targetAttributesParameter;

	private final ModelClassParameter emmModelClassParameter;

	private final ModelDistanceFunctionParameter distanceFunctionParameter;

	public ParameterLinkedEMMPatternBuilder(
			Parameter<PropositionalLogic> propositionalLogicParameter,
			TargetAttributesParameter targetAttributesParameter,
			ModelClassParameter emmModelClassParameter,
			ModelDistanceFunctionParameter distanceFunctionParameter) {

		this.propositionalLogicParameter = propositionalLogicParameter;
		this.targetAttributesParameter = targetAttributesParameter;
		this.emmModelClassParameter = emmModelClassParameter;
		this.distanceFunctionParameter = distanceFunctionParameter;
	}

	@Override
	public ExceptionalModelPattern build(Description description) {
	    return new DefaultExceptionalModelPatternBuilder(propositionalLogicParameter.getCurrentValue(),
		    targetAttributesParameter.getCurrentValue(), emmModelClassParameter.getCurrentValue(),
		    distanceFunctionParameter.getCurrentValue()).build(description);
	}
}
