/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.emm;

import java.util.Collection;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.beamsearch.BeamSearch;
import de.unibonn.realkd.algorithms.common.DataTableParameter;
import de.unibonn.realkd.algorithms.common.MatchingPropositionalLogicParameter;
import de.unibonn.realkd.algorithms.common.PatternConstraint;
import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterAdapter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Pattern;

public class ExceptionalModelBeamSearch extends AbstractMiningAlgorithm {

	private final DataTableParameter datatableParameter;

	private final TargetAttributesParameter targets;

	private final EMMTargetFunctionParameter targetFunctionParameter;

	private final ModelClassParameter emmModelClassParameter;

	private final ModelDistanceFunctionParameter modelDistanceFunctionParameter;

	private final BeamSearch beamSearch;

	private final Parameter<PropositionalLogic> propLogicParameter;

	public ExceptionalModelBeamSearch(DataWorkspace workspace) {
		datatableParameter = new DataTableParameter(workspace);
		propLogicParameter = new MatchingPropositionalLogicParameter(workspace,
				datatableParameter);
		targets = new TargetAttributesParameter(datatableParameter);
		emmModelClassParameter = new ModelClassParameter(targets);
		modelDistanceFunctionParameter = new ModelDistanceFunctionParameter(
				emmModelClassParameter);
		targetFunctionParameter = new EMMTargetFunctionParameter();

		// create wrapped beam search instance and configure it with all options
		// that can be set statically without waiting for any user input coming
		// in from through our parameters
		beamSearch = new BeamSearch(workspace,
				new ParameterLinkedEMMPatternBuilder(propLogicParameter,
						targets, emmModelClassParameter,
						modelDistanceFunctionParameter));
		beamSearch.setPropositionFilter(new TargetAttributePropositionFilter(
				targets));
		beamSearch.addPruningConstraint(PatternConstraint.POSITIVE_FREQUENCY);

		new ParameterAdapter<>(beamSearch.getPropositionalLogicParameter(),
				propLogicParameter);

		new ParameterAdapter<>(beamSearch.getTargetFunctionParameter(),
				targetFunctionParameter);

		registerParameter(datatableParameter);
		registerParameter(propLogicParameter);
		registerParameter(targets);
		registerParameter(emmModelClassParameter);
		registerParameter(modelDistanceFunctionParameter);
		registerParameter(targetFunctionParameter);
		registerParameter(beamSearch.getNumberOfResultsParameter());
		registerParameter(beamSearch.getBeamWidthParameter());
	}

	@Override
	public String toString() {
		return "EMMBeamSearch";
	}

	@Override
	public String getName() {
		return "EMM BeamSearch";
	}

	@Override
	public String getDescription() {
		return "Performs beam search for exceptional model patterns.";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.EM_MINING;
	}

	public TargetAttributesParameter getTargetAttributesParameter() {
		return targets;
	}

	public Parameter<PatternOptimizationFunction> getTargetFunctionParameter() {
		return targetFunctionParameter;
	}

	public ModelClassParameter getModelClassParameter() {
		return this.emmModelClassParameter;
	}

	public ModelDistanceFunctionParameter getModelDistanceFunctionParameter() {
		return this.modelDistanceFunctionParameter;
	}

	@Override
	protected Collection<Pattern> concreteCall() {
		return beamSearch.call();
	}

	@Override
	protected void onStopRequest() {
		beamSearch.requestStop();
	}

}
