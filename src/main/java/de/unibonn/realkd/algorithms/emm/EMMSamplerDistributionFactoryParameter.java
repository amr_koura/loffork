/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.emm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.unibonn.realkd.algorithms.sampling.DiscriminativityDistributionFactory;
import de.unibonn.realkd.algorithms.sampling.DistributionFactory;
import de.unibonn.realkd.algorithms.sampling.WeightedDiscriminativityDistributionFactory;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;

public class EMMSamplerDistributionFactoryParameter extends
		DefaultRangeEnumerableParameter<DistributionFactory> implements
		RangeEnumerableParameter<DistributionFactory> {

	private static final Class<DistributionFactory> TYPE = DistributionFactory.class;
	private static final String DESCRIPTION = "The probability distribution on the pattern space that is used to generate random seeds for EMM pattern search";
	private static final String NAME = "EMM Sampling distribution";

	public EMMSamplerDistributionFactoryParameter(
			final Parameter<PropositionalLogic> propositionalLogicParameter,
			final Parameter<List<Attribute>> targetAttributeParameter) {
		super(NAME, DESCRIPTION, TYPE,
				new RangeComputer<DistributionFactory>() {

					private final List<DistributionFactory> options = Arrays
							.asList((DistributionFactory) new DiscriminativityDistributionFactory(
									targetAttributeParameter,
									0, 1, 1),
									(DistributionFactory) new DiscriminativityDistributionFactory(
											targetAttributeParameter,
											1, 1, 1),
									(DistributionFactory) new DiscriminativityDistributionFactory(
											targetAttributeParameter,
											0, 2, 1),
									(DistributionFactory) new DiscriminativityDistributionFactory(
											targetAttributeParameter,
											0, 2, 2),
									(DistributionFactory) new WeightedDiscriminativityDistributionFactory(
											propositionalLogicParameter,
											targetAttributeParameter,
											0,
											2,
											1,
											new RowWeightComputer.UniformRowWeightComputer()));

					@Override
					public List<DistributionFactory> computeRange() {
						return options;
					}
				}, propositionalLogicParameter, targetAttributeParameter);

	}
}
