/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.unibonn.realkd.algorithms.emm;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.patterns.emm.MeanDeviationModel;
import de.unibonn.realkd.patterns.emm.ModelDistanceFunction;
import de.unibonn.realkd.patterns.emm.ProbabilisticModel;
import de.unibonn.realkd.patterns.emm.TheilSenLinearRegressionModel;

public class ModelDistanceFunctionParameter extends
		DefaultRangeEnumerableParameter<ModelDistanceFunction> {

	private static final List<ModelDistanceFunction> DISTANCE_FUNCTIONS = new ArrayList<>();
	static {
		DISTANCE_FUNCTIONS.add(ProbabilisticModel.TOTALVARIATION);
		DISTANCE_FUNCTIONS.add(TheilSenLinearRegressionModel.COSINEDISTANCE);
		DISTANCE_FUNCTIONS.add(MeanDeviationModel.MANHATTEN_MEAN_DEVIATION);
		DISTANCE_FUNCTIONS.add(MeanDeviationModel.POSITIVE_MEAN_DEVIATION);
		DISTANCE_FUNCTIONS.add(MeanDeviationModel.NEGATIVE_MEAN_DEVIATION);
	}

	private static final Class<ModelDistanceFunction> TYPE = ModelDistanceFunction.class;
	private static final String DESCRIPTION = "The function for measuring distance between models.";
	private static final String NAME = "Model distance function";

//	private final ModelClassParameter modelClassParameter;

	public ModelDistanceFunctionParameter(
			final ModelClassParameter modelClassParameter) {
		super(NAME, DESCRIPTION, TYPE,
				new RangeComputer<ModelDistanceFunction>() {

					@Override
					public List<ModelDistanceFunction> computeRange() {
						List<ModelDistanceFunction> result = new ArrayList<>();
						for (ModelDistanceFunction distanceFunction : DISTANCE_FUNCTIONS) {
							if (distanceFunction
									.isApplicable(modelClassParameter
											.getCurrentValue().getModelClass())) {
								result.add(distanceFunction);
							}
						}
						return result;
					}
				}, modelClassParameter);
//		this.modelClassParameter = modelClassParameter;
	}

//	@Override
//	protected List<ModelDistanceFunction> getConcreteRange() {
//		List<ModelDistanceFunction> result = new ArrayList<>();
//		for (ModelDistanceFunction distanceFunction : DISTANCE_FUNCTIONS) {
//			if (distanceFunction.isApplicable(modelClassParameter
//					.getCurrentValue().getModelClass())) {
//				result.add(distanceFunction);
//			}
//		}
//		return result;
//	}

}
