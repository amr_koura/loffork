package de.unibonn.realkd.algorithms.emm.dssd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;

/**
 * @author Vladimir Dzyuba, KU Leuven
 */
public class SubgroupSetSelectorParameter extends
		DefaultRangeEnumerableParameter<SubgroupSetSelector> {
	private static final List<SubgroupSetSelector> OPTIONS;
	static {
		final List<SubgroupSetSelector> options = new ArrayList<>(5);
		options.add(SubgroupSetSelector.QUALITY);
		options.add(SubgroupSetSelector.DESCRIPTION);
		options.add(SubgroupSetSelector.COVER_SEQUENTIAL);
		options.add(SubgroupSetSelector.COVER_ADDITIVE);
		options.add(SubgroupSetSelector.COVER_MULTIPLICATIVE);

		OPTIONS = Collections.unmodifiableList(options);
	}

	private static final Class<SubgroupSetSelector> TYPE = SubgroupSetSelector.class;

	public SubgroupSetSelectorParameter(final String name,
			final String description) {
		super(name, description, TYPE,
				new RangeComputer<SubgroupSetSelector>() {

					@Override
					public List<SubgroupSetSelector> computeRange() {
						return OPTIONS;
					}
				});
	}
}
