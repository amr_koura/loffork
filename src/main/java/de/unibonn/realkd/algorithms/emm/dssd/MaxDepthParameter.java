package de.unibonn.realkd.algorithms.emm.dssd;

import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

/**
 * Maximal search depth parameter for
 * {@link de.unibonn.realkd.algorithms.emm.dssd.DiverseSubgroupSetDiscovery DSSD},
 * currently interpreted as a constraint on the pattern description length.
 *
 * @author Vladimir Dzyuba, KU Leuven
 */
public class MaxDepthParameter extends DefaultParameter<Integer> {
    private static final String NAME = "Max.depth";
    private static final String DESCRIPTION = "Maximal search depth, i.e. maximal length of pattern descriptions";
    private static final int DEFAULT = 0;
    private static final String HINT = "Must be non-negative, where 0 implies no limit.";

    private static ValueValidator<Integer> MUST_NOT_BE_NEGATIVE = new ValueValidator<Integer>() {
        @Override
        public boolean valid(final Integer value) {
            return value >= 0;
        }
    };

    public MaxDepthParameter() {
        super(NAME, DESCRIPTION,
                Integer.class, DEFAULT, StringParser.INTEGER_PARSER,
                MUST_NOT_BE_NEGATIVE, HINT);
    }
}
