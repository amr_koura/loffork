package de.unibonn.realkd.algorithms.emm.dssd;

import de.unibonn.realkd.algorithms.common.NumberOfResultsParameter;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

/**
 * Number of intermediate results for
 * {@link de.unibonn.realkd.algorithms.emm.dssd.DiverseSubgroupSetDiscovery DSSD},
 * i.e. a number of subgroups that are passed from beam search to the post-selector.
 *
 * @author Vladimir Dzyuba, KU Leuven
 */
public class NumberOfIntermediateResultsParameter extends DefaultParameter<Integer> {
    private static final String NAME = "Number of intermediate results";
    private static final String DESCRIPTION =
            "Number of subgroups that are passed from beam search to the post-selector";
    private static final int DEFAULT = 10000;
    private static final String HINT = "Must be positive";

    public NumberOfIntermediateResultsParameter(final NumberOfResultsParameter numberOfResultsParameter) {
        super(NAME, DESCRIPTION,
                Integer.class, DEFAULT, StringParser.INTEGER_PARSER,
                new LargerThanNumberOfResultsValidator(numberOfResultsParameter), HINT, numberOfResultsParameter);
    }

    private static final class LargerThanNumberOfResultsValidator implements ValueValidator<Integer> {
        private final NumberOfResultsParameter mainParameter;

        public LargerThanNumberOfResultsValidator(final NumberOfResultsParameter mainParameter) {
            this.mainParameter = mainParameter;
        }

        @Override
        public boolean valid(final Integer value) {
            return value > mainParameter.getCurrentValue();
        }
    }
}
