package de.unibonn.realkd.algorithms.emm.dssd;

/**
 * <p>Additive cover-based subgroup selection.</p>
 * The coverage bonus is computed as {@code Math.pow(weight, coverCount)}.
 * {@code weight} is currently fixed to the value {@code 0.9}.
 *
 * @see de.unibonn.realkd.algorithms.emm.dssd.CoverBasedSubgroupSetSelector
 *
 * @author Vladimir Dzyuba, KU Leuven
 */
public class MultiplicativeCoverBasedSubgroupSetSelector extends CoverBasedSubgroupSetSelector {
    protected final double weight;

    public MultiplicativeCoverBasedSubgroupSetSelector(final double weight) {
        this.weight = weight;
    }

    public MultiplicativeCoverBasedSubgroupSetSelector() {
        this(0.9);
    }

    @Override
    protected double bonusForAlreadyCoveredRecord(final int count) { // weight ^ count
        double update = weight;
        for (int i = 0; i < count - 1; i++)
            update *= weight;

        return update;
    }

    @Override
    public String toString() {
        return "Multiplicative cover-based selector";
    }
}
