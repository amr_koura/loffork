package de.unibonn.realkd.algorithms.emm;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.algorithms.emm.TargetAttributesParameter;
import de.unibonn.realkd.algorithms.common.PropositionFilter;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * PropositionFilter for EMM algorithms that wish to filter out propositions
 * that relate to their target attributes.
 * 
 * @author mboley
 * 
 */
public class TargetAttributePropositionFilter implements PropositionFilter {

	private final TargetAttributesParameter targetAttributesParameter;

	public TargetAttributePropositionFilter(
			TargetAttributesParameter targetAttributesParameter) {
		this.targetAttributesParameter = targetAttributesParameter;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Proposition> filter(PropositionalLogic propositionalLogic) {
		List<Proposition> propositions = new ArrayList<>();
		for (Proposition<?> proposition : propositionalLogic.getPropositions()) {
			if (!targetAttributesParameter.getCurrentValue().contains(
					proposition.getAttribute())
					&& !propositionalLogic
							.getDatatable()
							.getAttributeGroupsStore()
							.isPartOfMacroAttributeWithAtLeastOneOf(
									proposition.getAttribute(),
									this.targetAttributesParameter
											.getCurrentValue())) {
				propositions.add(proposition);
			}
		}
		return propositions;
	}

}
