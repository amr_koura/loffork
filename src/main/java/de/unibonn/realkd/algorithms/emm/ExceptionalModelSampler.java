/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.emm;

import java.util.Collection;
import java.util.List;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.common.DataTableParameter;
import de.unibonn.realkd.algorithms.common.MatchingPropositionalLogicParameter;
import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.algorithms.sampling.ConsaptBasedSamplingMiner;
import de.unibonn.realkd.algorithms.sampling.DistributionFactory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;

public class ExceptionalModelSampler extends AbstractMiningAlgorithm {

	private final Parameter<PropositionalLogic> propositionalLogicParameter;
	private final TargetAttributesParameter targets;
	private final ModelClassParameter modelClassParameter;
	private final ModelDistanceFunctionParameter distanceFunctionParameter;
	private final EMMSamplerDistributionFactoryParameter distributionFactoryParameter;
	private final DataTableParameter dataTableParameter;
	private final Parameter<PatternOptimizationFunction> targetFunctionParameter;
	private final ConsaptBasedSamplingMiner beamSearch;
	
	public ExceptionalModelSampler(DataWorkspace workspace) {
		this.dataTableParameter = new DataTableParameter(workspace);
		this.propositionalLogicParameter = new MatchingPropositionalLogicParameter(
				workspace,dataTableParameter);
		this.targets = new TargetAttributesParameter(dataTableParameter);
		this.modelClassParameter = new ModelClassParameter(targets);
		this.distanceFunctionParameter = new ModelDistanceFunctionParameter(
				modelClassParameter);
		this.distributionFactoryParameter = new EMMSamplerDistributionFactoryParameter(
				propositionalLogicParameter, targets);
		this.targetFunctionParameter = new EMMTargetFunctionParameter();

		this.beamSearch = new ConsaptBasedSamplingMiner(new ParameterLinkedEMMPatternBuilder(
				propositionalLogicParameter, targets,
				modelClassParameter, distanceFunctionParameter),
				propositionalLogicParameter,
				distributionFactoryParameter, targetFunctionParameter);
		
		registerParameter(dataTableParameter);
		registerParameter(propositionalLogicParameter);
		registerParameter(targets);
		registerParameter(modelClassParameter);
		registerParameter(distanceFunctionParameter);
		registerParameter(distributionFactoryParameter);
		registerParameter(beamSearch.getNumberOfResultsParameter());
		registerParameter(beamSearch.getPostProcessorParameter());
		registerParameter(targetFunctionParameter);
	}

	@Override
	protected Collection<Pattern> concreteCall() {
		return beamSearch.call();
	}
	
	@Override
	protected void onStopRequest() {
		beamSearch.requestStop();
	}

	public void setDistributionFactory(DistributionFactory distributionFactory) {
		this.distributionFactoryParameter.set(distributionFactory);
	}

	public DistributionFactory getDistributionFactory() {
		return distributionFactoryParameter.getCurrentValue();
	}

	@SuppressWarnings("rawtypes")
	public List<Attribute> getTargetAttributes() {
		return this.targets.getCurrentValue();
	}

	@Override
	public String toString() {
		return "EMMSampler|" + getDistributionFactory() + "|"
				+ targetFunctionParameter.getCurrentValue();
	}

	@Override
	public String getName() {
		return "EMM Sampler";
	}

	@Override
	public String getDescription() {
		return "Direct EMM Pattern Sampler";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.EM_MINING;
	}

	public Parameter<PropositionalLogic> getPropositionalLogicParameter() {
		return this.propositionalLogicParameter;
	}

	public TargetAttributesParameter getTargetAttributesParameter() {
		return targets;
	}

	public ModelClassParameter getModelClassParameter() {
		return modelClassParameter;
	}

	public ModelDistanceFunctionParameter getModelDistanceFunctionParameter() {
		return distanceFunctionParameter;
	}

	public void setNumberOfResults(int numberOfResults) {
		this.beamSearch.getNumberOfResultsParameter().set(numberOfResults);
	}

	public Parameter<DistributionFactory> getDistributionFactoryParameter() {
		return this.distributionFactoryParameter;
	}
}
