/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.emm;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;

import com.google.common.collect.Sets;

import de.unibonn.realkd.algorithms.emm.PCAEvaluator;
import de.unibonn.realkd.algorithms.emm.PosNegDecider;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.emm.ContingencyTableCellKey;
import de.unibonn.realkd.patterns.emm.ContingencyTableModel;

public interface PosNegDecider {

	public class SingleAttributePosNegDecider implements PosNegDecider {

		private Attribute attribute;
		private double thresholdValue;
		private String positiveValue;

		public SingleAttributePosNegDecider(Attribute attribute) {
			this.attribute = attribute;
			if (attribute instanceof MetricAttribute) {
				thresholdValue = ((MetricAttribute) attribute).getMean();
			} else {
				positiveValue = ((CategoricalAttribute) attribute)
						.getCategories().get(0);
			}
		}

		public boolean isPos(int rowIx) {
			if (attribute.isValueMissing(rowIx)) {
				return false;
			}
			if (attribute instanceof MetricAttribute) {
				return ((MetricAttribute) attribute).getValue(rowIx) >= thresholdValue;
			}
			return attribute.getValue(rowIx).equals(positiveValue);
		}
	}

	public class MultipleAttributesPosNegDecider implements PosNegDecider {

		List<PosNegDecider> deciders;

		public MultipleAttributesPosNegDecider(List<Attribute> attributes) {
			deciders = newArrayListWithCapacity(attributes.size());
			for (Attribute attribute : attributes) {
				deciders.add(new SingleAttributePosNegDecider(attribute));
			}
		}

		public boolean isPos(int rowIx) {
			for (PosNegDecider decider : deciders) {
				if (!decider.isPos(rowIx)) {
					return false;
				}
			}
			return true;
		}
	}

	public class CTPosNegDecider implements PosNegDecider {

		private List<Attribute> attributes;
		private List<List<String>> keys;
		private List<Boolean> isPos = newArrayList();
		private DataTable dataTable;

		public CTPosNegDecider(DataTable dataTable, List<Attribute> attributes) {
			this.attributes = attributes;
			this.dataTable = dataTable;
			ContingencyTableModel ct;
			ct = new ContingencyTableModel(dataTable, attributes);

			keys = newArrayList(Sets.cartesianProduct(
					newHashSet(attributes.get(1).getName() + "_lower",
							attributes.get(1).getName() + "_upper"),
					newHashSet(attributes.get(0).getName() + "_lower",
							attributes.get(0).getName() + "_upper")));

			for (List<String> key : keys) {
				if (ct.getProbabilities().getNormalizedValue(
						new ContingencyTableCellKey(key)) >= .25) {
					isPos.add(false);
				} else {
					isPos.add(true);
				}
			}
			System.out.println("keys: " + keys);
			System.out.println("positive: " + isPos);
		}

		public boolean isPos(int rowIx) {
			if (dataTable.atLeastOneAttributeValueMissingFor(rowIx, attributes)) {
				return false;
			}
			List<String> key = newArrayList();
			for (int i = 1; i >= 0; i--) {
				if (((MetricAttribute) attributes.get(i)).getValue(rowIx) >= ((MetricAttribute) attributes
						.get(i)).getMean()) {
					key.add(attributes.get(i).getName() + "_upper");
				} else {
					key.add(attributes.get(i).getName() + "_lower");
				}
			}
			return isPos.get(keys.indexOf(key));
		}
	}

	public class PCAPosNegDecider implements PosNegDecider {

		PCAEvaluator pcaEvaluator;

		public PCAPosNegDecider(DataTable dataTable, List<Attribute> attributes) {
			pcaEvaluator = new PCAEvaluator(dataTable, attributes);
		}

		@Override
		public boolean isPos(int rowIx) {
			return pcaEvaluator.getDevFirstDimension(rowIx) >= 0;
		}
	}

	public boolean isPos(int rowIx);
}