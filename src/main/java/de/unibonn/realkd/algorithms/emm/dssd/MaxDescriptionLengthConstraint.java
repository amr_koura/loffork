package de.unibonn.realkd.algorithms.emm.dssd;

import de.unibonn.realkd.algorithms.common.PatternConstraint;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Vladimir Dzyuba, KU Leuven
 */
public final class MaxDescriptionLengthConstraint implements PatternConstraint {
    private final int maxLength;

    public MaxDescriptionLengthConstraint(final int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public boolean satisfies(final Pattern pattern) {
        final int descriptionLength = ((LogicallyDescribedLocalPattern) pattern).getDescription().size();
        return descriptionLength <= maxLength;
    }
}
