/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.emm.dssd;

import java.util.Collection;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.beamsearch.BeamSearch;
import de.unibonn.realkd.algorithms.common.DataTableParameter;
import de.unibonn.realkd.algorithms.common.FreePropositionalLogicParameter;
import de.unibonn.realkd.algorithms.common.MinimumFrequencyThresholdParameter;
import de.unibonn.realkd.algorithms.common.NumberOfResultsParameter;
import de.unibonn.realkd.algorithms.common.PatternConstraint;
import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.algorithms.emm.EMMTargetFunctionParameter;
import de.unibonn.realkd.algorithms.emm.ModelClassParameter;
import de.unibonn.realkd.algorithms.emm.ModelDistanceFunctionParameter;
import de.unibonn.realkd.algorithms.emm.ParameterLinkedEMMPatternBuilder;
import de.unibonn.realkd.algorithms.emm.TargetAttributePropositionFilter;
import de.unibonn.realkd.algorithms.emm.TargetAttributesParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterAdapter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Implementation of "M. van Leeuwen, A. Knobbe: Diverse subgroup set discovery,
 * <i>Data Mining and Knowledge Discovery, September 2012, Volume 25, Issue 2,
 * pp 208-242</i>".
 * 
 * @author mboley
 * @author Vladimir Dzyuba, KU Leuven
 */
public class DiverseSubgroupSetDiscovery extends AbstractMiningAlgorithm {

	private static final String NAME = "Diverse subgroup set discovery";
	private static final String DESCRIPTION = "Discovers a set of subgroups that are interesting individually and mutually not redundant.";

	private static final String BEAM_SELECTOR_NAME = "Beam selection heuristic";
	private static final String BEAM_SELECTOR_DESCRIPTION = "Applied to select a diverse set of candidate patterns at each level of beam search";
	private static final SubgroupSetSelector BEAM_SELECTOR_DEFAULT = SubgroupSetSelector.COVER_MULTIPLICATIVE;

	private static final String POST_SELECTOR_NAME = "Post-selection heuristic";
	private static final String POST_SELECTOR_DESCRIPTION = "Applied to select a diverse subset of the output of beam search";
	private static final SubgroupSetSelector POST_SELECTOR_DEFAULT = SubgroupSetSelector.DESCRIPTION;

	private final BeamSearch beamSearch;
	private final DataTableParameter datatableParameter;
	private final FreePropositionalLogicParameter propositionalLogicParameter;
	private final TargetAttributesParameter targetAttributesParameter;
	private final ModelClassParameter modelClassParameter;
	private final ModelDistanceFunctionParameter modelDistanceFunctionParameter;
	private final MinimumFrequencyThresholdParameter minimumFrequencyThreshold;
	private final SubgroupSetSelectorParameter beamSelectorParameter;
	private final SubgroupSetSelectorParameter postSelectorParameter;
	private final NumberOfResultsParameter numberOfResultsParameter;
    private final NumberOfIntermediateResultsParameter numberOfIntermediateResultsParameter;
    private final MaxDepthParameter maxDepthParameter;
	private final Parameter<PatternOptimizationFunction> targetFunctionParameter;

	public DiverseSubgroupSetDiscovery(DataWorkspace workspace) {
		super();

		// Create all parameters that have to be set in order for us to be
		// executed successfully (in realKD algorithm objects serve as
		// containers for their parameter values). These parameters are
		// essentially boxes in which a user can plug in values via the Creedo
		// UI or by specifying them through the command line.
		this.datatableParameter = new DataTableParameter(workspace);
		this.propositionalLogicParameter = new FreePropositionalLogicParameter(
				workspace);
		this.targetAttributesParameter = new TargetAttributesParameter(
				datatableParameter);
		// attributes parameter depends on datatable parameter because user can
		// only select attributes present in the selected datatable (dependency
		// on
		// propositions is only due to legacy and will be removed soon)
		this.modelClassParameter = new ModelClassParameter(
				targetAttributesParameter);
		// model class parameter depends on selected target attributes because
		// certain model classes are only available for specific target
		// attribute
		// configurations. For example fitted univariate Gaussian distributions
		// are only available if exactly one numeric target has been selected
		this.modelDistanceFunctionParameter = new ModelDistanceFunctionParameter(
				modelClassParameter);
		this.targetFunctionParameter = new EMMTargetFunctionParameter();
		this.minimumFrequencyThreshold = new MinimumFrequencyThresholdParameter();
		this.beamSelectorParameter = new SubgroupSetSelectorParameter(
				BEAM_SELECTOR_NAME, BEAM_SELECTOR_DESCRIPTION);
		this.beamSelectorParameter.set(BEAM_SELECTOR_DEFAULT);
		this.postSelectorParameter = new SubgroupSetSelectorParameter(
				POST_SELECTOR_NAME, POST_SELECTOR_DESCRIPTION);
		this.postSelectorParameter.set(POST_SELECTOR_DEFAULT);
		this.numberOfResultsParameter = new NumberOfResultsParameter();
        this.numberOfIntermediateResultsParameter =
                new NumberOfIntermediateResultsParameter(this.numberOfResultsParameter);
        this.maxDepthParameter = new MaxDepthParameter();

		// Create and configure wrapped beam search algorithm.
		this.beamSearch = new BeamSearch(workspace,
				new ParameterLinkedEMMPatternBuilder(
						propositionalLogicParameter, targetAttributesParameter,
						modelClassParameter, modelDistanceFunctionParameter));
		beamSearch.setPropositionFilter(new TargetAttributePropositionFilter(
				targetAttributesParameter));

		// Add parameter adapter for automatically pushing updates to target
		// function parameter of this to target function parameter of wrapped
		// beam search.
		new ParameterAdapter<>(beamSearch.getTargetFunctionParameter(),
				this.targetFunctionParameter);

		// Register all parameters that we want to expose to our user (and want
		// to be checked for consistency when we are executed). We also need to
		// pass on all parameters of the wrapped beam search that we do not set
		// by ourselves on execution.
		//
		// Note: parameters have to be registered in order compatible with their
		// dependency. Order of registration is also used by Creedo UI and by
		// command line interpreter to sort parameters specified by user.
		registerParameter(this.targetAttributesParameter);
		registerParameter(this.modelClassParameter);
		registerParameter(this.modelDistanceFunctionParameter);
		registerParameter(this.targetFunctionParameter);
        registerParameter(this.numberOfResultsParameter);
        registerParameter(beamSearch.getBeamWidthParameter());
        registerParameter(this.maxDepthParameter);
        registerParameter(this.numberOfIntermediateResultsParameter);
        registerParameter(this.minimumFrequencyThreshold);
		registerParameter(this.beamSelectorParameter);
		registerParameter(this.postSelectorParameter);
	}

	@Override
	protected Collection<Pattern> concreteCall() {
		// At this moment super-classes will already have checked consistent
		// assignment of all our registered parameters.

		// Apply all parameters that we received from our user to correctly
		// configure wrapped beam search.
		beamSearch.clearPruningConstraints();
		beamSearch
				.addPruningConstraint(new PatternConstraint.MinimumMeasureValeConstraint(
						InterestingnessMeasure.FREQUENCY,
						minimumFrequencyThreshold.getCurrentValue()));
        final int maxDepth = this.maxDepthParameter.getCurrentValue();
        if (maxDepth > 0) {
            beamSearch.addPruningConstraint(new MaxDescriptionLengthConstraint(maxDepth));
        }

        beamSearch.setNumberOfResults(this.numberOfIntermediateResultsParameter.getCurrentValue());

		// in particular, diverse subgroup set selectors
		final SubgroupSetSelector beamSelector = this.beamSelectorParameter
				.getCurrentValue();
		beamSelector.setK(beamSearch.getBeamWidth());
		beamSelector.setQualityMeasure(this.targetFunctionParameter.getCurrentValue());
		beamSearch.setNodeForNextLevelSelector(beamSelector);

		final SubgroupSetSelector postSelector = this.postSelectorParameter
				.getCurrentValue();
		postSelector.setK(this.numberOfResultsParameter.getCurrentValue());
		postSelector.setQualityMeasure(this.targetFunctionParameter.getCurrentValue());
		beamSearch.setPostProcessor(postSelector);

		// call and return results of wrapped beam search
		return beamSearch.call();
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.EM_MINING;
	}

    @Override
    public String toString() {
        return NAME;
    }
}
