/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.emm;

import java.util.HashSet;
import java.util.List;

import mime.plain.weighting.PosNegDbInterface;
import de.unibonn.realkd.algorithms.sampling.ConsaptUtils;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;

public interface PosNegDatabaseCreator {

	public static class PosNegDatabaseByFirstAttribute implements
			PosNegDatabaseCreator {
		
		public static PosNegDatabaseByFirstAttribute INSTANCE=new PosNegDatabaseByFirstAttribute();
		
		private PosNegDatabaseByFirstAttribute() {
			;
		}

		@Override
		public PosNegDbInterface createDb(PropositionalLogic probLogic,
				List<Attribute> targetAttributes) {
			return ConsaptUtils.createPosNegTransactionDBByFirstAttribute(
					probLogic, targetAttributes);
		}
		
		@Override
		public String toString() {
			return "split on first attribute";
		}
		
		
	}

	public static class PosNegDatabaseByFirstTwoAttributes implements
			PosNegDatabaseCreator {
		
		public static PosNegDatabaseByFirstTwoAttributes INSTANCE=new PosNegDatabaseByFirstTwoAttributes();
		
		private PosNegDatabaseByFirstTwoAttributes() {
			;
		}

		@Override
		public PosNegDbInterface createDb(PropositionalLogic probLogic,
				List<Attribute> targetAttributes) {
			return ConsaptUtils.createPosNegTransactionDBByFirstTwoAttributes(
					probLogic, targetAttributes);
		}
		
		@Override
		public String toString() {
			return "split on all attributes";
		}

	}

	public static class TestPosNegDatabase implements PosNegDatabaseCreator {

		@Override
		public PosNegDbInterface createDb(PropositionalLogic probLogic,
				List<Attribute> targetAttributes) {
			return ConsaptUtils.createPosNegDb(probLogic, new HashSet<>(
					targetAttributes), new PosNegDecider.CTPosNegDecider(
					probLogic.getDatatable(), targetAttributes));
		}
	}

	public static class PosNegDatabaseUsingPCA implements PosNegDatabaseCreator {
		
		public static PosNegDatabaseUsingPCA INSTANCE=new PosNegDatabaseUsingPCA();
		
		private PosNegDatabaseUsingPCA() {
			;
		}

		@Override
		public PosNegDbInterface createDb(PropositionalLogic probLogic,
				List<Attribute> targetAttributes) {
			return ConsaptUtils.createPosNegTransactionDBUsingPCA(probLogic,
					targetAttributes);
		}
	}

	public PosNegDbInterface createDb(PropositionalLogic probLogic,
			List<Attribute> targetAttributes);
}
