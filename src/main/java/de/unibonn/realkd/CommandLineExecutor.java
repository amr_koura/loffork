/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.algorithms.association.AssociationMiningBeamSearch;
import de.unibonn.realkd.algorithms.association.AssociationSampler;
import de.unibonn.realkd.algorithms.emm.ExceptionalModelBeamSearch;
import de.unibonn.realkd.algorithms.emm.ExceptionalModelSampler;
import de.unibonn.realkd.algorithms.emm.dssd.DiverseSubgroupSetDiscovery;
import de.unibonn.realkd.algorithms.outlier.OneClassModelMiner;
import de.unibonn.realkd.algorithms.outlier.LOF.ILOFOutlier;
import de.unibonn.realkd.algorithms.outlier.LOF.LOFOutlier;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.DataWorkspaceFactory;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVFileBuilder;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author bjacobs, mboley
 */
public class CommandLineExecutor {

	private static Map<String, MiningAlgorithmFactory> ALGORITHM_FACTORIES = new HashMap<>();
	{
		ALGORITHM_FACTORIES.put("EMM_BEAMSEARCH", new MiningAlgorithmFactory() {

			@Override
			public MiningAlgorithm create(DataWorkspace dataTub) {
				return new ExceptionalModelBeamSearch(dataTub);
			}

		});

		ALGORITHM_FACTORIES.put("ASSOCIATION_BEAMSEARCH",
				new MiningAlgorithmFactory() {

					@Override
					public MiningAlgorithm create(DataWorkspace dataTub) {
						return new AssociationMiningBeamSearch(dataTub);
					}

				});

		ALGORITHM_FACTORIES.put("EMM_SAMPLER", new MiningAlgorithmFactory() {

			@Override
			public MiningAlgorithm create(DataWorkspace dataTub) {
				return new ExceptionalModelSampler(dataTub);
			}

		});

		ALGORITHM_FACTORIES.put("ASSOCIATION_SAMPLER",
				new MiningAlgorithmFactory() {

					@Override
					public MiningAlgorithm create(DataWorkspace dataTub) {
						return new AssociationSampler(dataTub);
					}

				});

		ALGORITHM_FACTORIES.put("OUTLIER", new MiningAlgorithmFactory() {

			@Override
			public MiningAlgorithm create(DataWorkspace dataTub) {
				return new OneClassModelMiner(dataTub);
			}

		});
		
		
		ALGORITHM_FACTORIES.put("LOF", new MiningAlgorithmFactory() {

			@Override
			public MiningAlgorithm create(DataWorkspace dataTub) {
				return new LOFOutlier(dataTub);
			}

		});
		
		ALGORITHM_FACTORIES.put("ILOF", new MiningAlgorithmFactory() {

			@Override
			public MiningAlgorithm create(DataWorkspace dataTub) {
				return new ILOFOutlier(dataTub);
			}

		});
		

		ALGORITHM_FACTORIES.put("DSSD", new MiningAlgorithmFactory() {

			@Override
			public MiningAlgorithm create(DataWorkspace workspace) {
				return new DiverseSubgroupSetDiscovery(workspace);
			}
		});

	}

	private final String USAGE = "Usage: kdtub load [datafile] [attributesfile] [attributegroupsfile] run [algorithmname] [parameter=value]*";

	public CommandLineExecutor() {
	}

	public void run(String[] args) {
		for (String arg : args) {
			System.out.print(arg + " ");
		}
		System.out.println();
		Configuration conf = null;
		try {
			conf = new Configuration(args);
		} catch (Exception e) {
			System.out.println(USAGE);
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}

		// start
		System.out.println("Starting algorithm...");
		System.out.println(conf.miningAlgorithm);
		Collection<Pattern> results = conf.miningAlgorithm.call();

		System.out.println("Saving results...");
		// save patterns
		try {
			saveResultsToFile(results, "resultpatterns.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Done!");
	}

	private void saveResultsToFile(Collection<Pattern> results, String s)
			throws IOException {
		StringBuilder builder = new StringBuilder();
		for (Pattern p : results) {
			builder.append(p.toString()).append("\r\n");
		}
		String fileContent = builder.toString();

		FileWriter fileWriter = new FileWriter(s);
		fileWriter.write(fileContent);
		fileWriter.flush();
		fileWriter.close();
	}

	private class Configuration {
		public final String dataFileName;
		public final String attributesFileName;
		public final String attributeGroupsFileName;
		public final String algorithmName;

		public Map<String, String[]> userProvidedAlgorithmParameterSettings;
		public final MiningAlgorithm miningAlgorithm;

		/**
		 * Tries to parse the arguments to a configuration
		 */
		public Configuration(String[] args) {
			List<String> argsList = Arrays.asList(args);

			// Check indices
			int loadIndex = argsList.indexOf("load");
			int runIndex = argsList.indexOf("run");

			if (loadIndex == -1 || runIndex == -1) {
				throw new IllegalArgumentException(
						"load and run commands must be provided.");
			}

			if (loadIndex > runIndex) {
				throw new IllegalArgumentException(
						"load command cannot come after run command.");
			}

			if (runIndex - loadIndex < 4) {
				throw new IllegalArgumentException(
						"load command requires three parameters.");
			}

			if (argsList.size() - runIndex < 2) {
				throw new IllegalArgumentException(
						"run command requires at least one parameter.");
			}

			// Store data

			dataFileName = argsList.get(loadIndex + 1);
			attributesFileName = argsList.get(loadIndex + 2);
			attributeGroupsFileName = argsList.get(loadIndex + 3);

			algorithmName = argsList.get(runIndex + 1);

			List<String> subList = argsList.subList(runIndex + 2,
					argsList.size());

			userProvidedAlgorithmParameterSettings = extractParameterSettings(subList);

			// Get algorithm
			MiningAlgorithmFactory factory = ALGORITHM_FACTORIES
					.get(algorithmName);

			if (factory == null) {
				throw new IllegalArgumentException("No such algorithm found: "
						+ algorithmName + "\n Valid options: \n"
						+ ALGORITHM_FACTORIES.keySet());
			}

			DataTable table = null;
			try {
				// table = new DataTableFromCSVBuilder().setDelimiter(';')
				// .setMissingSymbol("?")
				// .setAttributeMetadataCSV(attributesFileContent)
				// .setDataCSV(dataFileContent)
				// .setAttributeGroupCSV(attributeGroupsFileContent)
				// .build();
				table = new DataTableFromCSVFileBuilder().setDelimiter(';')
						.setMissingSymbol("?")
						.setAttributeMetadataCSVFilename(attributesFileName)
						.setDataCSVFilename(dataFileName)
						.setAttributeGroupCSVFilename(attributeGroupsFileName)
						.build();
			} catch (DataFormatException e) {
				throw new IllegalArgumentException(
						"Table could not be created: " + e.getMessage(), e);
			}

			DataWorkspace dataWorkspace = DataWorkspaceFactory.INSTANCE
					.createDataWorkspace(); // new DefaultDataWorkspace();
			dataWorkspace.add(table);
			dataWorkspace.add(new PropositionalLogic(table));

			// Instantiate algorithm
			miningAlgorithm = factory.create(dataWorkspace);

			Map<String, String[]> crossOutMap = new HashMap<>(
					userProvidedAlgorithmParameterSettings);
			// Set algorithm parameters
			// loop inversion (unknown parameters are simply ignored here)
			miningAlgorithm.unloadMapValuesToParameters(crossOutMap);

			// Trigger exception for unused parameters and warn
			for (String key : crossOutMap.keySet()) {
				try {
					miningAlgorithm.findParameterByName(key);
				} catch (IllegalArgumentException e) {
					System.out.println("WARNING: " + e.getMessage());
					printValidParameters();
				}
			}

		}

		private void printValidParameters() {
			System.out.print("Valid parameters are: ");
			for (Parameter<?> param : miningAlgorithm.getTopLevelParameters()) {
				System.out.print("'" + param.getName() + "' ");
			}
			System.out.println();
		}

		private Map<String, String[]> extractParameterSettings(
				List<String> subList) {
			Map<String, String[]> result = new LinkedHashMap<>();

			for (String entry : subList) {
				String[] split = entry.split("=");
				String key = split[0];
				String[] value = split[1].split(",");
				/*
				 * String key = removeQuotes(split[0]); String value =
				 * removeQuotes(split[1]);
				 */
				result.put(key, value);
			}

			return result;
		}

		/*
		 * private String removeQuotes(String s) { if ((s.startsWith("\"") &&
		 * s.endsWith("\"")) || (s.startsWith("'") && s.endsWith("'"))) { return
		 * s.substring(1, s.length() - 1); }
		 * 
		 * return s; }
		 */
	}

}
