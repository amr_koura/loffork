package de.unibonn.realkd.discoveryprocess;

import static de.unibonn.realkd.common.logger.LogChannel.DISCOVERY_PROCESS;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Models the interaction of a user with the output of pattern mining algorithms
 * in an incremental round-based fashion (together with
 * {@link DiscoveryProcessState}) and allows clients to observe this
 * interaction.
 * <p/>
 * At all times patterns under consideration are partitioned into three sets:
 * <p/>
 * 1. result patterns---containing patterns that the user currently considers as
 * the results of her analysis; these patterns are persistent through round
 * transitions.
 * <p/>
 * 2. discarded patterns---containing patterns that have been explicitly marked
 * by the user to not be shown again; also this list is persistent through round
 * transitions.
 * <p/>
 * 3. candidate patterns---containing new patterns that are currently presented
 * to the user to potentially augment her result set; a new round starts with a
 * new set of candidates.
 * <p/>
 * The class provides three public methods to move patterns between these three
 * sets (addCandidateToResults, deletePatternFromCandidates,
 * deletePatternFromResults) and one method to start a new round with a new set
 * of candidate patterns (nextRound). Additionally, by the endRound-method,
 * there is the possibility to enter a state where the current round has ended
 * (i.e., the move operations between the sets has no effect), but no new round
 * with new candidates has started yet.
 * <p/>
 * Observers (implementing {@link DiscoveryProcessObserver}) can register to
 * receive notifications about all these operations happening.
 * 
 * @see DiscoveryProcessState
 * 
 * @author mboley, bjacobs, bkang
 * 
 */
public class DiscoveryProcess {

	private interface OpenClosedState {

		public void nextRound(List<Pattern> nextRoundPatterns);

		public void endRound();

		public boolean isClosed();

		public void deletePatternFromResults(Pattern p);

		public void addCandidateToResults(Pattern p);

		public void deletePatternFromCandidates(Pattern p);
	}

	/**
	 * Represents the open state of a discovery process.
	 */
	private class OpenState implements OpenClosedState {
		@Override
		public void nextRound(List<Pattern> nextRoundPatterns) {
			endRound();
			notifyObserversRoundAboutToStart(nextRoundPatterns);
			setDiscoveryProcessState(openState);
		}

		@Override
		public void endRound() {
			setDiscoveryProcessState(closedState);
			notifyObserversRoundEnded();
		}

		@Override
		public boolean isClosed() {
			return false;
		}

		@Override
		public void deletePatternFromResults(Pattern p) {
			DISCOVERY_PROCESS.log(
					"Notifying observers about deletion from result of " + p,
					LogMessageType.INTRA_COMPONENT_MESSAGE);
			for (DiscoveryProcessObserver observer : processObservers) {
				observer.aboutToDeletePatternFromResults(p);
			}
			discoveryProcessState.removeResultPattern(p);
			discoveryProcessState.addDiscardedPattern(p);
		}

		@Override
		public void addCandidateToResults(Pattern p) {
			for (DiscoveryProcessObserver observer : processObservers) {
				observer.aboutToSavePattern(p);
			}
			discoveryProcessState.removeCandidatePattern(p);
			discoveryProcessState.addResultPattern(p);
		}

		@Override
		public void deletePatternFromCandidates(Pattern p) {
			for (DiscoveryProcessObserver observer : processObservers) {
				observer.aboutToDeletePatternFromCandidates(p);
			}
			discoveryProcessState.removeCandidatePattern(p);
			discoveryProcessState.addDiscardedPattern(p);
		}

	}

	/**
	 * Represents the end state of a discovery process.
	 */
	private class ClosedState implements OpenClosedState {
		@Override
		public void nextRound(List<Pattern> nextRoundPatterns) {
			setDiscoveryProcessState(openState);
			notifyObserversRoundAboutToStart(nextRoundPatterns);

		}

		@Override
		public void endRound() {
			;
			DISCOVERY_PROCESS.log(
					"WARNING: endRound() called when round was already ended!",
					LogMessageType.GLOBAL_STATE_CHANGE);
			// throw new
			// IllegalStateException("The discovery round is closed.");
		}

		@Override
		public boolean isClosed() {
			return true;
		}

		@Override
		public void deletePatternFromResults(Pattern p) {
			;
		}

		@Override
		public void addCandidateToResults(Pattern p) {
			;
		}

		@Override
		public void deletePatternFromCandidates(Pattern p) {
			;
		}

	}

	private OpenClosedState openClosedState;

	private OpenState openState;

	private ClosedState closedState;

	private final List<DiscoveryProcessObserver> processObservers;

	private final DiscoveryProcessState discoveryProcessState;

	private void notifyObserversRoundAboutToStart(
			List<Pattern> nextRoundPatterns) {
		DISCOVERY_PROCESS.log("take over cache content into candidates",
				LogMessageType.INTRA_COMPONENT_MESSAGE);
		this.discoveryProcessState
				.setCandidateResultPatterns(nextRoundPatterns);
		for (DiscoveryProcessObserver observer : processObservers) {
			observer.justBeganNewRound();
		}
	}

	private void notifyObserversRoundEnded() {
		DISCOVERY_PROCESS.log("notify observers that round about to end",
				LogMessageType.INTRA_COMPONENT_MESSAGE);
		for (DiscoveryProcessObserver observer : processObservers) {
			observer.roundEnded();
		}
	}

	private void setDiscoveryProcessState(OpenClosedState state) {
		this.openClosedState = state;
	}

	public DiscoveryProcess() {
		this.closedState = new ClosedState();
		this.openState = new OpenState();
		this.openClosedState = openState;
		this.processObservers = new ArrayList<>();
		this.discoveryProcessState = new DiscoveryProcessState();
	}

	public void addObserver(DiscoveryProcessObserver observer) {
		this.processObservers.add(observer);
	}

	public void markAsSeen(Pattern p) {
		for (DiscoveryProcessObserver observer : processObservers) {
			observer.markAsSeen(p);
		}
	}

	/**
	 * Delete a pattern from the candidate set and add it to the discarded set.
	 * Has no effect if process closed for inputs. Observers are notified about
	 * this action.
	 * 
	 * @param p
	 *            pattern to be deleted from the candidates.
	 */
	public void deletePatternFromCandidates(Pattern p) {
		openClosedState.deletePatternFromCandidates(p);
	}

	/**
	 * Delete a pattern from the result set and add it to the discarded set. Has
	 * no effect if process closed for inputs. Observers are notified about this
	 * action.
	 * 
	 * @param p
	 *            pattern to be deleted from the results.
	 */
	public void deletePatternFromResults(Pattern p) {
		openClosedState.deletePatternFromResults(p);
	}

	/**
	 * Delete a pattern from the candidate set and add it to the result set. Has
	 * no effect if process closed for inputs. Observers are notified about this
	 * action.
	 * 
	 * @param p
	 *            pattern to be added to the results.
	 */
	public void addCandidateToResults(Pattern p) {
		openClosedState.addCandidateToResults(p);
	}

	public DiscoveryProcessState getDiscoveryProcessState() {
		return this.discoveryProcessState;
	}

	/**
	 * Starts a new discovery round with the patterns provided as candidates.
	 * Opens process for inputs if it was closed before.
	 * 
	 * @param nextRoundPatterns
	 *            patterns to be used in the new round
	 */
	public void nextRound(List<Pattern> nextRoundPatterns) {
		openClosedState.nextRound(nextRoundPatterns);
	}

	/**
	 * Ends a discovery round, i.e., closes the discovery process for actions
	 * that move patterns between sets. Observers are notified about this action
	 * (unless round was already ended before---in this case a warning is
	 * logged).
	 * 
	 */
	public void endRound() {
		openClosedState.endRound();
	}

	/**
	 * @return false if actions for moving patterns between sets will currently
	 *         have an effect and true if not, i.e., the process state is
	 *         currently closed
	 * 
	 */
	public boolean isClosed() {
		return openClosedState.isClosed();
	}

	@Override
	public String toString() {
		return "Observers: " + this.processObservers + " State: "
				+ this.discoveryProcessState;
	}
}
