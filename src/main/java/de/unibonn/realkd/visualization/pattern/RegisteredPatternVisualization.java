package de.unibonn.realkd.visualization.pattern;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.patterns.Pattern;

public enum RegisteredPatternVisualization implements PatternVisualization {

	FREQUENCY_PIE(new FrequencyPieVisualization()),

	COLORED_TARGET_CONTINGENCY_TABLE(new ColoredTargetContingencyTable()),

//	TARGET_POINT_CLOUD(new TargetPointCloud()),

//	TARGET_POINT_CLOUD_PCA(new TargetPointCloudPCA()),

	TARGET_POINT_CLOUD_WITH_LINES(new TargetPointCloudWithLines()),

	TARGET_SHIFT_BOXPLOT(new BoxPlotTargetShiftVisualization()),

	TARGET_SHIFT_GAUSSIAN_FIT(new GaussianTargetShiftVisualization()),

	TARGET_HISTOGRAM(new TargetHistogram()),
	
	TWO_NUMERIC_ATTRIBUTE_PATTERN_SCATTER_PLOT(new TwoNumericAttributePatternScatterPlot()),

	LIFT_LOGPROBABILITY_STACKED_BARCHART(
			new LiftLogProbabilityStackedBarChart());

	private PatternVisualization patternView;

	private RegisteredPatternVisualization(PatternVisualization patternView) {
		this.patternView = patternView;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return this.patternView.isApplicable(pattern);
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return this.patternView.drawDetailed(pattern);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		return this.patternView.draw(pattern);
	}

}
