package de.unibonn.realkd.visualization.pattern;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.visualization.JFChartPainter;

public class TwoNumericAttributePatternScatterPlot implements
		PatternVisualization {

	@Override
	public boolean isApplicable(Pattern pattern) {
		return checkExactlyTwoNumericAttributes(pattern.getAttributes());
	}

	private boolean checkExactlyTwoNumericAttributes(List<Attribute> attributes) {
		if (attributes.size() != 2) {
			return false;
		}
		for (Attribute attribute : attributes) {
			if (!(attribute instanceof MetricAttribute)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public JFreeChart draw(Pattern pattern) {

		JFChartPainter painter = new JFChartPainter();

		List<List<JFChartPainter.Point>> pointLists = newArrayList();
		SortedSet<Integer> complementRows = new TreeSet<>(pattern
				.getDataArtifact().getObjectIds());
		complementRows.removeAll(pattern.getSupportSet());
		pointLists.add(JFChartPainter.createPoints(complementRows, pattern
				.getAttributes().get(0), pattern.getAttributes().get(1)));

		pointLists
				.add(JFChartPainter.createPoints(pattern.getSupportSet(),
						pattern.getAttributes().get(0), pattern.getAttributes()
								.get(1)));

		return painter.createPointCloud("", pointLists, pattern.getAttributes().get(0).getName(),
				pattern.getAttributes().get(1).getName());
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

}
