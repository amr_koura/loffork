package de.unibonn.realkd.visualization.pattern;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.patterns.Pattern;

public interface PatternVisualization {

	public boolean isApplicable(Pattern pattern);

//	public JFreeChart draw(Pattern pattern);

	public JFreeChart draw(Pattern pattern);
	
	public JFreeChart drawDetailed(Pattern pattern);

}
