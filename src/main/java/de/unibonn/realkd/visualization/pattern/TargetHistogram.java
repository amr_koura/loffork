package de.unibonn.realkd.visualization.pattern;

import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.visualization.JFChartPainter;
import de.unibonn.realkd.visualization.ReducedEnglishDecimalFormat;

import org.jfree.chart.JFreeChart;

public class TargetHistogram implements PatternVisualization {

	static String sgLabel = "subgroup";

	static String globalLabel = "global";

	TargetHistogram() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return (pattern instanceof ExceptionalModelPattern && ((ExceptionalModelPattern) pattern)
				.getTargetAttributes().size() == 1);
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		JFChartPainter painter = new JFChartPainter();

		Data data = new Data();

		Attribute targetAttribute = ((ExceptionalModelPattern) pattern)
				.getTargetAttributes().get(0);
		if (targetAttribute instanceof MetricAttribute) {
			data = getNumericHistogramSpecs((MetricAttribute) targetAttribute,
					pattern);
		} else if (targetAttribute instanceof CategoricalAttribute) {
			data = getCategoricHistgramSpecs(
					(CategoricalAttribute) targetAttribute, pattern);
		}

		return painter.createLayeredBarChart("", data.series,
				data.column, data.values);
	}

	private static double[] getNumericHistogramValues(
			ExceptionalModelPattern p, int numOfBins) {
		double[] res = new double[2 * numOfBins];

		MetricAttribute attr = ((MetricAttribute) ((ExceptionalModelPattern) p)
				.getTargetAttributes().get(0));
		double range = attr.getMax() - attr.getMin();

		Set<Integer> supportSet = p.getSupportSet();

		List<Integer> rowIndices = attr.getSortedNonMissingRowIndices();
		int pos = 0;
		for (int i = 0; i < numOfBins; i++) {
			double bucketBound = attr.getMin() + (i + 1) * range
					/ (double) numOfBins;
			res[i + numOfBins] = 0.0;
			while (pos < rowIndices.size()
					&& ((MetricAttribute)attr).getValue(rowIndices.get(pos)) <= bucketBound) {
				res[i + numOfBins] += 1.0;
				if (supportSet.contains(rowIndices.get(pos))) {
					res[i] += 1.0;
				}
				pos += 1;
			}
			res[i + numOfBins] /= rowIndices.size();
			res[i] /= supportSet.size();
		}

		return res;
	}

	private static int getNumberOfBinsForNumeric(MetricAttribute attribute) {
		return (int) Math.round(Math.ceil(Math.log(attribute
				.getNumberOfNonMissingValues()) / Math.log(2)));
	}

	private static class Data {
		String[] series;
		String[] column;
		double[] values;
	}

	private static Data getCategoricHistgramSpecs(
			CategoricalAttribute targetAttribute, Pattern pattern) {
		int numberOfCategories = ((CategoricalAttribute) targetAttribute)
				.getCategories().size();
		Data data = new Data();
		data.series = new String[numberOfCategories * 2];
		data.column = new String[numberOfCategories * 2];
		data.values = new double[numberOfCategories * 2];
		Set<Integer> supportSet = pattern.getSupportSet();

		List<Double> CategoryFrequenciesOnRows = ((CategoricalAttribute) targetAttribute)
				.getCategoryFrequenciesOnRows(supportSet);
		List<Double> CategoryFrequencies = ((CategoricalAttribute) targetAttribute)
				.getCategoryFrequencies();

		for (int i = 0; i < numberOfCategories; i++) {
			data.series[i] = sgLabel;
			data.series[numberOfCategories + i] = globalLabel;

			data.column[i] = ((CategoricalAttribute) targetAttribute)
					.getCategories().get(i);
			data.column[numberOfCategories + i] = ((CategoricalAttribute) targetAttribute)
					.getCategories().get(i);

			data.values[i] = CategoryFrequenciesOnRows.get(i);
			data.values[numberOfCategories + i] = CategoryFrequencies.get(i);
		}
		return data;
	}

	private static Data getNumericHistogramSpecs(
			MetricAttribute targetAttribute, Pattern pattern) {
		int numOfBins = getNumberOfBinsForNumeric(targetAttribute);
		Data data = new Data();
		data.series = new String[numOfBins * 2];
		data.column = new String[numOfBins * 2];

		ReducedEnglishDecimalFormat formatter = new ReducedEnglishDecimalFormat();
		double range = targetAttribute.getMax() - targetAttribute.getMin();
		for (int i = 0; i < numOfBins; i++) {
			data.series[i] = sgLabel;
			data.series[numOfBins + i] = globalLabel;

			data.column[i] = formatter.format(targetAttribute.getMin()
					+ (i + 1) * range / (double) numOfBins);
			data.column[numOfBins + i] = formatter.format(targetAttribute
					.getMin() + (i + 1) * range / (double) numOfBins);
		}

		data.values = getNumericHistogramValues(
				(ExceptionalModelPattern) pattern, numOfBins);
		return data;
	}

}
