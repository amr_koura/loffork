package de.unibonn.realkd.visualization.pattern;

import java.util.Set;
import java.util.TreeSet;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.CategoricalKeyComputer;
import de.unibonn.realkd.patterns.emm.ContingencyTable;
import de.unibonn.realkd.patterns.emm.ContingencyTableModel;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.NumericalKeyComputer;
import de.unibonn.realkd.visualization.JFChartPainter;

import org.jfree.chart.JFreeChart;

public class ColoredTargetContingencyTable implements PatternVisualization {

	ColoredTargetContingencyTable() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return ((pattern instanceof ExceptionalModelPattern)
				&& (((ExceptionalModelPattern) pattern).getTargetAttributes()
						.size() == 2) && (((ExceptionalModelPattern) pattern)
					.getGlobalModel() instanceof ContingencyTableModel));
	}

//	@Override
//	public JFreeChart draw(Pattern pattern) {
//		return draw(pattern, false);
//	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		ExceptionalModelPattern emPattern = (ExceptionalModelPattern) pattern;
		ContingencyTable globalProbs = ((ContingencyTableModel) emPattern
				.getGlobalModel()).getProbabilities();
		ContingencyTable localProbs = ((ContingencyTableModel) emPattern
				.getLocalModel()).getProbabilities();
		Set<String> xValues; 
		Set<String> yValues;
		int numberOfCols = 2;
		int numberOfRows = 2;
		Attribute secondAttribute = emPattern.getTargetAttributes().get(1);
		if (secondAttribute instanceof CategoricalAttribute) {
			numberOfRows = ((CategoricalAttribute) secondAttribute)
					.getCategories().size();

			yValues = new TreeSet<>(new CategoricalKeyComputer(
					(CategoricalAttribute) secondAttribute).getDistinctKeys());
		} else {
			yValues = new TreeSet<>(new NumericalKeyComputer(
					(MetricAttribute) secondAttribute).getDistinctKeys());
		}
		Attribute firstAttribute = emPattern.getTargetAttributes().get(0);
		if (firstAttribute instanceof CategoricalAttribute) {
			numberOfCols = ((CategoricalAttribute) firstAttribute)
					.getCategories().size();

			xValues = new TreeSet<>(new CategoricalKeyComputer(
					(CategoricalAttribute) firstAttribute).getDistinctKeys());
		} else {
			xValues = new TreeSet<>(new NumericalKeyComputer(
					(MetricAttribute) firstAttribute).getDistinctKeys());
		}



		JFChartPainter painter = new JFChartPainter();

		return painter.createContingencyTable("", firstAttribute,
				secondAttribute, globalProbs, localProbs,
				xValues, yValues,
				numberOfRows, numberOfCols, false);
	}
	
	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		ExceptionalModelPattern emPattern = (ExceptionalModelPattern) pattern;
		ContingencyTable globalProbs = ((ContingencyTableModel) emPattern
				.getGlobalModel()).getProbabilities();
		ContingencyTable localProbs = ((ContingencyTableModel) emPattern
				.getLocalModel()).getProbabilities();
		Set<String> xValues; 
		Set<String> yValues;
		int numberOfCols = 2;
		int numberOfRows = 2;
		Attribute secondAttribute = emPattern.getTargetAttributes().get(1);
		if (secondAttribute instanceof CategoricalAttribute) {
			numberOfRows = ((CategoricalAttribute) secondAttribute)
					.getCategories().size();

			yValues = new TreeSet<>(new CategoricalKeyComputer(
					(CategoricalAttribute) secondAttribute).getDistinctKeys());
		} else {
			yValues = new TreeSet<>(new NumericalKeyComputer(
					(MetricAttribute) secondAttribute).getDistinctKeys());
		}
		Attribute firstAttribute = emPattern.getTargetAttributes().get(0);
		if (firstAttribute instanceof CategoricalAttribute) {
			numberOfCols = ((CategoricalAttribute) firstAttribute)
					.getCategories().size();

			xValues = new TreeSet<>(new CategoricalKeyComputer(
					(CategoricalAttribute) firstAttribute).getDistinctKeys());
		} else {
			xValues = new TreeSet<>(new NumericalKeyComputer(
					(MetricAttribute) firstAttribute).getDistinctKeys());
		}



		JFChartPainter painter = new JFChartPainter();

		return painter.createContingencyTable("", firstAttribute,
				secondAttribute, globalProbs, localProbs,
				xValues, yValues,
				numberOfRows, numberOfCols, true);
	}

}
