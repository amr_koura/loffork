package de.unibonn.realkd.visualization.pattern;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.visualization.JFChartPainter;

public class TargetPointCloud implements PatternVisualization {

	TargetPointCloud() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return pattern instanceof ExceptionalModelPattern
				&& checkExactlyTwoNumericTargets(((ExceptionalModelPattern) pattern)
						.getTargetAttributes());
	}

	protected boolean checkExactlyTwoNumericTargets(
			List<Attribute> targetAttributes) {
		if (targetAttributes.size() != 2) {
			return false;
		}
		for (Attribute attribute : targetAttributes) {
			if (!(attribute instanceof MetricAttribute)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		ExceptionalModelPattern exceptionalModelPattern = (ExceptionalModelPattern) pattern;

		JFChartPainter painter = new JFChartPainter();

		List<List<JFChartPainter.Point>> pointLists = newArrayList();
		pointLists
				.add(JFChartPainter.createPoints(exceptionalModelPattern.getGlobalModel()
						.getDataTable().getObjectIds(), exceptionalModelPattern
						.getGlobalModel().getAttributes().get(0),
						exceptionalModelPattern.getGlobalModel()
								.getAttributes().get(1)));

		pointLists.add(JFChartPainter.createPoints(exceptionalModelPattern.getLocalModel()
				.getRows(), exceptionalModelPattern.getLocalModel()
				.getAttributes().get(0), exceptionalModelPattern
				.getLocalModel().getAttributes().get(1)));

		return painter.createPointCloud("", pointLists, exceptionalModelPattern
				.getTargetAttributes().get(0).getName(),
				exceptionalModelPattern.getTargetAttributes().get(1).getName());

	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}
}
