package de.unibonn.realkd.visualization.pattern;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.visualization.JFChartPainter;

public class LiftLogProbabilityStackedBarChart implements PatternVisualization {

	LiftLogProbabilityStackedBarChart() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return pattern instanceof Association;
	}

	@Override
	public JFreeChart draw(Pattern pattern) {

		JFChartPainter painter = new JFChartPainter();

		Description description = ((Association) pattern).getDescription();
		int length = description.size(); // ((Association)
											// pattern).getIndividualFrequences().size();
		String series[] = new String[length + 1];
		String column[] = new String[length + 1];
		double[] values = new double[length + 1];

		// values[0] = Math.log(((Association) pattern).getFrequency());
		values[0] = Math.log(description.getSupportSet().size()
				/ (double) description.getPropositionalLogic().getSize());
		series[0] = "s0";
		column[0] = "frequency";

		int i = 1;

		for (Proposition<?> prop : ((Association) pattern).getDescription()
				.getElements()) {
			values[i] = Math.log((double) prop.getSupportCount()
					/ pattern.getDataArtifact().getSize());
			series[i] = "s" + i;
			column[i] = "expected freq.";
			i++;
		}

		return painter.createStackedBarChart("", series, column, values);
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

}
