package de.unibonn.realkd.visualization.pattern;

import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.AbstractModel;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.TheilSenLinearRegressionModel;
import de.unibonn.realkd.visualization.JFChartPainter;

public class TargetPointCloudWithLines extends TargetPointCloud {

	@Override
	public boolean isApplicable(Pattern pattern) {
		return pattern instanceof ExceptionalModelPattern
				&& ((ExceptionalModelPattern) pattern).getGlobalModel() instanceof TheilSenLinearRegressionModel
				&& checkExactlyTwoNumericTargets(((ExceptionalModelPattern) pattern)
						.getTargetAttributes());
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		ExceptionalModelPattern emPattern = (ExceptionalModelPattern) pattern;

//		if (!checkExactlyTwoNumericTargets(emPattern.getTargetAttributes())) {
//			return "client/images/nopic.png";
//		}

		JFChartPainter painter = new JFChartPainter();
		AbstractModel model1 = emPattern
				.getLocalModel();

		List<JFChartPainter.Point> localPoints = JFChartPainter.createPoints(model1.getRows(), model1.getAttributes().get(0),
		model1.getAttributes().get(1));
		AbstractModel model = emPattern
				.getGlobalModel();
		List<JFChartPainter.Point> globalPoints = JFChartPainter.createPoints(model.getDataTable()
		.getObjectIds(), model.getAttributes().get(0), model
		.getAttributes().get(1));

		XYSeries localPointSeries = new XYSeries("localPoints");
		XYSeries localLineSeries = new XYSeries("localLine");

		XYSeries globalPointSeries = new XYSeries("globalPoints");
		XYSeries globalLineSeries = new XYSeries("globalLine");
		for (JFChartPainter.Point point : globalPoints) {
			globalPointSeries.add(point.x, point.y);
			globalLineSeries
					.add(point.x, ((TheilSenLinearRegressionModel) emPattern
							.getGlobalModel()).predict(point.x));
			localLineSeries.add(point.x,
					((TheilSenLinearRegressionModel) emPattern.getLocalModel())
							.predict(point.x));
		}
		for (JFChartPainter.Point point : localPoints) {
			localPointSeries.add(point.x, point.y);
		}

		final XYSeriesCollection pointsDataset = new XYSeriesCollection();
		pointsDataset.addSeries(globalPointSeries);
		pointsDataset.addSeries(localPointSeries);
		final XYSeriesCollection linePointsDataset = new XYSeriesCollection();
		linePointsDataset.addSeries(globalLineSeries);
		linePointsDataset.addSeries(localLineSeries);

		return painter.createPointCloudWithLines("", emPattern
				.getTargetAttributes().get(0).getName(), emPattern
				.getTargetAttributes().get(1).getName(), pointsDataset,
				linePointsDataset);
	}
}
