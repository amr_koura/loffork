package de.unibonn.realkd.visualization.pattern;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Set;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.algorithms.emm.PCAEvaluator;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.AbstractModel;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.visualization.JFChartPainter;

public class TargetPointCloudPCA implements PatternVisualization {

	TargetPointCloudPCA() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return pattern instanceof ExceptionalModelPattern
				&& checkTargetsAtLeastTwoNumeric(((ExceptionalModelPattern) pattern)
						.getTargetAttributes());
	}

	protected boolean checkTargetsAtLeastTwoNumeric(
			List<Attribute> targetAttributes) {
		if (targetAttributes.size() < 2) {
			return false;
		}
		for (Attribute attribute : targetAttributes) {
			if (!(attribute instanceof MetricAttribute)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

	protected List<JFChartPainter.Point> createCloudLocalModel(
			AbstractModel model, PCAEvaluator pcaEvaluator) {
		return createCloudForRows(model.getRows(), model.getAttributes(),
				pcaEvaluator);
	}

	protected List<JFChartPainter.Point> createCloudGlobalModel(
			AbstractModel model, PCAEvaluator pcaEvaluator) {
		List<Attribute> attributes = model.getAttributes();
		// Set<Integer> rows = new HashSet<>();
		// int size = model.getDataTable().getSize();
		// for (int i = 0; i < size; i++) {
		// rows.add(i);
		// }
		return createCloudForRows(model.getDataTable().getObjectIds(),
				attributes, pcaEvaluator);
	}

	private List<JFChartPainter.Point> createCloudForRows(Set<Integer> rows,
			List<Attribute> attributes, PCAEvaluator pcaEvaluator) {
		if (attributes.size() == 1) {
			return createCloud(rows, attributes.get(0), pcaEvaluator);
		} else if (attributes.size() > 1) {
			return createCloud(rows, attributes.get(0), attributes.get(1),
					pcaEvaluator);
		}
		return newArrayList();
	}

	private List<JFChartPainter.Point> createCloud(Set<Integer> rows,
			Attribute attribute, PCAEvaluator pcaEvaluator) {
		return createCloud(rows, attribute, attribute, pcaEvaluator);
	}

	private List<JFChartPainter.Point> createCloud(Set<Integer> rows,
			Attribute attribute1, Attribute attribute2,
			PCAEvaluator pcaEvaluator) {
		List<JFChartPainter.Point> pointCloud = newArrayList();
		// int size = attribute1.getDataTable().getSize();
		for (Integer i : rows) {
			pointCloud.add(new JFChartPainter.Point(pcaEvaluator
					.getDevFirstDimension(i), pcaEvaluator.getDevForDimension(
					i, 1)));
		}
		return pointCloud;
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		ExceptionalModelPattern exceptionalModelPattern = (ExceptionalModelPattern) pattern;

		PCAEvaluator pcaEvaluator = new PCAEvaluator(exceptionalModelPattern
				.getPropositionalLogic().getDatatable(),
				exceptionalModelPattern.getTargetAttributes());

		JFChartPainter painter = new JFChartPainter();

		List<List<JFChartPainter.Point>> pointClouds = newArrayList();
		pointClouds.add(createCloudGlobalModel(
				exceptionalModelPattern.getGlobalModel(), pcaEvaluator));
		pointClouds.add(createCloudLocalModel(
				exceptionalModelPattern.getLocalModel(), pcaEvaluator));

		return painter.createPointCloud("", pointClouds, "First Comp",
				"Second Comp");
	}
}
