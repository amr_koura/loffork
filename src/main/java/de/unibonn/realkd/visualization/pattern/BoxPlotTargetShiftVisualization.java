package de.unibonn.realkd.visualization.pattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jfree.chart.JFreeChart;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.visualization.JFChartPainter;

public class BoxPlotTargetShiftVisualization implements PatternVisualization {

	/**
	 * constructor only to be invoked from Visualization Register
	 */
	BoxPlotTargetShiftVisualization() {
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return (pattern instanceof ExceptionalModelPattern
				&& ((ExceptionalModelPattern) pattern).getTargetAttributes()
						.size() == 1 && ((ExceptionalModelPattern) pattern)
				.getTargetAttributes().get(0) instanceof MetricAttribute);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		JFChartPainter painter = new JFChartPainter();
		List<Double> globalValues = new ArrayList<>();
		List<Double> subgroupValues = new ArrayList<>();
		Attribute<?> targetAttribute = ((ExceptionalModelPattern) pattern)
				.getTargetAttributes().get(0);
		Set<Integer> supportSet = pattern.getSupportSet();

		for (int objectId : supportSet) {
			if (targetAttribute.isValueMissing(objectId)) {
				continue;
			}
			subgroupValues.add(((MetricAttribute) targetAttribute)
					.getValue(objectId));
		}
		globalValues.addAll(((MetricAttribute) targetAttribute)
				.getNonMissingValues());

		DefaultBoxAndWhiskerCategoryDataset boxDataset = new DefaultBoxAndWhiskerCategoryDataset();
		boxDataset.add(subgroupValues, "subgroup", "");
		boxDataset.add(globalValues, "global", "");


		return painter.createBoxPlotChart("", boxDataset, targetAttribute.getName());
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

}
