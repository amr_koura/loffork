package de.unibonn.realkd.visualization.pattern;

import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.GaussianModel;
import de.unibonn.realkd.visualization.JFChartPainter;

public class GaussianTargetShiftVisualization implements PatternVisualization {

	static String SUBGROUP_LABEL = "subgroup";

	static String GLOBAL_LABEL = "global";

	public static class GaussianParams {
		public double min;
		public double max;
		public double mean;
		public double stDev;

		public GaussianParams(double min, double max, double mean, double stDev) {
			this.min = min;
			this.max = max;
			this.mean = mean;
			this.stDev = stDev;
		}
	}

	/**
	 * constructor only to be invoked from Visualization Register
	 */
	GaussianTargetShiftVisualization() {
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return (pattern instanceof ExceptionalModelPattern
				&& ((ExceptionalModelPattern) pattern).getGlobalModel() instanceof GaussianModel && pattern
				.getSupportSet().size() > 1);
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		JFChartPainter painter = new JFChartPainter();

		List<GaussianParams> gaussianParams = new ArrayList<>();

		GaussianModel globalModel = (GaussianModel) ((ExceptionalModelPattern) pattern)
				.getGlobalModel();
		GaussianModel localModel = (GaussianModel) ((ExceptionalModelPattern) pattern)
				.getLocalModel();

		gaussianParams
				.add(new GaussianParams(localModel.getMin(), localModel
						.getMax(), localModel.getMean(), sqrt(localModel
						.getVariance())));
		gaussianParams.add(new GaussianParams(globalModel.getMin(), globalModel
				.getMax(), globalModel.getMean(), sqrt(globalModel
				.getVariance())));

		return painter.createGaussians("", gaussianParams, ((ExceptionalModelPattern) pattern).getAttributes().get(0).getName());
	}

}
