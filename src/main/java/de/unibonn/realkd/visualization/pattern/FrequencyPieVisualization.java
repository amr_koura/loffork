package de.unibonn.realkd.visualization.pattern;

import de.unibonn.realkd.patterns.association.Association;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.visualization.JFChartPainter;

public class FrequencyPieVisualization implements PatternVisualization {

	/**
	 * constructor only to be invoked from Visualization Register
	 */
	FrequencyPieVisualization() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		if (pattern instanceof Association) {
			return false;
		}
		else if (pattern.hasMeasure(InterestingnessMeasure.FREQUENCY)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {

		JFChartPainter painter = new JFChartPainter();

		String[] items = { "Frequency", "" };
		double freqency = pattern.getValue(InterestingnessMeasure.FREQUENCY);
		double[] values = { freqency, 1 - freqency };
		boolean isPatternView = true;
		// String result;

		return painter.createPieChart("", items, values, isPatternView);
	}

}
