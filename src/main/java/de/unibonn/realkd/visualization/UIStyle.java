package de.unibonn.realkd.visualization;

import java.awt.Color;
import java.awt.Font;

import org.jfree.chart.JFreeChart;

public class UIStyle {

	private static UIStyle uiStyle;
	// declared style constants
	public final int TRANSPARENT = 0;
	public boolean borderVisible = false;
	// font style
	public Font attributeFont = new Font("Arial", Font.BOLD, 13);
	public Font smallAttributeFont = new Font("Arial", Font.PLAIN, 13);
	public Font cellFont_Lp = new Font("Arial", Font.PLAIN, 10);
	public Font cellFont_Hp = new Font("Arial", Font.PLAIN, 12);
	public Font textAnnotation = new Font("Arial", Font.PLAIN, 10);
	public Font itemFont = new Font("Arial", Font.BOLD, 8);
	public Font legendFont = new Font("Arial", Font.BOLD, 10);
	public Font tickLabelFont = new Font("Arial", Font.PLAIN, 8);
	public Font labelFont = new Font("Arial", Font.PLAIN, 10);
	// paint style
	public Color tickLabelPaint = Color.black;
	public Color axisTickLabelPaint = Color.black;
	public Color domainGridLinePaint = Color.black;
	public Color itemPaint = Color.white;
	public Color backGroundPaint = Color.white;
	public Color outLinePaint = Color.gray;
	public Color localPaint = new Color(255, 99, 71);
	public Color globalPaint = new Color(71, 99, 255);

	// Private constructor for singleton class
	private UIStyle() {

	}

	public static UIStyle getInstance() {
		if (uiStyle == null) {
			uiStyle = new UIStyle();
		}
		return uiStyle;
	}

	// function to set chart style
	public void setChartStyle(JFreeChart chart) {
		chart.setBackgroundPaint(null);
		chart.setBorderPaint(null);
		chart.setBorderVisible(uiStyle.borderVisible);
	}

}
