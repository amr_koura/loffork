package de.unibonn.realkd.visualization.attribute;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.visualization.JFChartPainter;

public class CategoricalAttributePieView implements AttributeView {
	@Override
	public boolean isApplicable(Attribute attribute) {
		return (attribute instanceof CategoricalAttribute);
	}

	@Override
	public JFreeChart draw(Attribute attribute) {

		JFChartPainter painter = new JFChartPainter();
		CategoricalAttribute categoricalAttribute = (CategoricalAttribute) attribute;
		boolean isPatternView = false;
		int numOfCategories = categoricalAttribute.getCategories().size();
		String[] items = new String[numOfCategories];
		double[] values = new double[numOfCategories];

		for (int i = 0; i < numOfCategories; i++) {
			items[i] = categoricalAttribute.getCategories().get(i);
			values[i] = categoricalAttribute.getCategoryFrequencies().get(i);
			// / categoricalAttribute.getValues().size();
		}

		String result;

		JFreeChart chart = painter.createPieChart("", items, values,
				isPatternView);
		return chart;
	}

	@Override
	public int getDefaultWidth() {
		return 220;
	}

	@Override
	public int getDefaultHeight() {
		return 220;
	}

}
