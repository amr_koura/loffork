package de.unibonn.realkd.visualization.attribute;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.visualization.JFChartPainter;

import org.jfree.chart.JFreeChart;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;

public class NumericAttributeBoxPlotView implements AttributeView {

	@Override
	public boolean isApplicable(Attribute attribute) {
		return (attribute instanceof MetricAttribute);
	}

	@Override
	public JFreeChart draw(Attribute attribute) {

		JFChartPainter painter = new JFChartPainter();
		MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;

		List<Double> values = new ArrayList<>(
				defaultMetricAttribute.getNonMissingValues());
		// for (String value : numericAttribute.getValues()) {
		// values.add(Double.parseDouble(value));
		// }

		DefaultBoxAndWhiskerCategoryDataset boxDataset = new DefaultBoxAndWhiskerCategoryDataset();
		boxDataset.add(values, "rowKey", "");

		return painter.createBoxPlotChart("", boxDataset, attribute.getName());
	}

	@Override
	public int getDefaultWidth() {
		return 220;
	}

	@Override
	public int getDefaultHeight() {
		return 124;
	}
}
