package de.unibonn.realkd.visualization;

import static com.google.common.collect.Lists.newArrayList;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.emm.ContingencyTable;
import de.unibonn.realkd.patterns.emm.ContingencyTableCellKey;
import de.unibonn.realkd.visualization.pattern.GaussianTargetShiftVisualization;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.CategoryTextAnnotation;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAnchor;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.AbstractPieLabelDistributor;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.SeriesRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BoxAndWhiskerRenderer;
import org.jfree.chart.renderer.category.LayeredBarRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.NormalDistributionFunction2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;
import org.jfree.util.SortOrder;

public class JFChartPainter {

	// private static final String SERVLET_CHART_URL_PREFIX =
	// "/servlet/DisplayChart?filename=";
	// private static final int TRANSPARENT = 0;
	// call standard style instance
	private UIStyle uiStyle = UIStyle.getInstance();

	public JFChartPainter() {
	}

	private void formatJFreeChart(JFreeChart chart) {
		chart.setBackgroundPaint(null);
		chart.setBorderPaint(null);
		chart.setBorderVisible(uiStyle.borderVisible);
		chart.getPlot().setBackgroundPaint(uiStyle.backGroundPaint);
		chart.getPlot().setBackgroundAlpha(uiStyle.TRANSPARENT);
		chart.getPlot().setOutlinePaint(null);
	}

	private void formatAxis(Axis axis) {
		axis.setTickLabelFont(uiStyle.tickLabelFont);
		axis.setLabelFont(uiStyle.labelFont);
		axis.setTickLabelPaint(uiStyle.tickLabelPaint);
		axis.setLabelPaint(uiStyle.axisTickLabelPaint);

		if (axis instanceof NumberAxis) {
			((NumberAxis) axis)
					.setNumberFormatOverride(new ReducedEnglishDecimalFormat());
		}
	}

	public JFreeChart createContingencyTable(String title,
			Attribute firstAttribute, Attribute secondAttribute,
			ContingencyTable globalProbs, ContingencyTable localProbs,
			Set<String> xValues, Set<String> yValues, int numberOfRows,
			int numberOfCols, boolean detailed) {
		float leftPadding = 0.3f;
		float bottomPadding = 0.3f;
		JFreeChart chart = ChartFactory.createXYLineChart(title, "", "",
				new XYSeriesCollection(new XYSeries("")),
				PlotOrientation.VERTICAL, false, true, false);
		formatJFreeChart(chart);

		XYPlot plot = (XYPlot) chart.getPlot();
		plot.getDomainAxis().setRange(0, numberOfCols + leftPadding);
		plot.getDomainAxis().setVisible(false);
		plot.getRangeAxis().setRange(0, numberOfRows + bottomPadding);
		plot.getRangeAxis().setVisible(false);
		// add cells
		addContingencyTableCells(plot, localProbs, globalProbs, xValues,
				yValues, numberOfCols, leftPadding, bottomPadding, detailed);

		Font attributeFont;
		if (detailed) {
			// attributeFont = new Font("Arial", Font.BOLD, 13);
			attributeFont = uiStyle.attributeFont;
		} else {
			// attributeFont = new Font("Arial", Font.PLAIN, 12);
			attributeFont = uiStyle.smallAttributeFont;
		}
		// add first attribute to bottom
		XYTextAnnotation a1 = new XYTextAnnotation(firstAttribute.getName(),
				leftPadding + numberOfCols / 2.0, 0.1);
		if (detailed) {
			a1.setFont(attributeFont);
		}
		plot.addAnnotation(a1);
		// add second attribute to left
		XYTextAnnotation a2 = new XYTextAnnotation(secondAttribute.getName(),
				0.1, bottomPadding + numberOfRows / 2.0);
		a2.setRotationAngle(-Math.PI / 2);
		if (detailed) {
			a2.setFont(attributeFont);
		}
		plot.addAnnotation(a2);
		if (detailed) {
			addContingencyTableDetails(plot, firstAttribute, "horizontal",
					leftPadding);
			addContingencyTableDetails(plot, secondAttribute, "vertical",
					bottomPadding);
		}

		return chart;

		// ChartRenderingInfo info = new ChartRenderingInfo();
		// return formatHtmlURL(session, chart, info);
	}

	private void addContingencyTableCells(XYPlot plot,
			ContingencyTable localProbs, ContingencyTable globalProbs,
			Set<String> xValues, Set<String> yValues, int numberOfCols,
			float leftPadding, float bottomPadding, boolean detailed) {
		int x = 0;
		int y = 0;
		int precision = 2;
		// Font cellFont = new Font("Arial", Font.PLAIN, 10);
		Font cellFont = uiStyle.cellFont_Lp;
		if (detailed) {
			precision = 4;
			// cellFont = new Font("Arial", Font.PLAIN, 12);
			cellFont = uiStyle.cellFont_Hp;
		}

		for (String yValue : yValues) {
			for (String xValue : xValues) {
				List<String> key = new ArrayList<>();
				key.add(xValue);
				key.add(yValue);
				ContingencyTableCellKey cellKey = new ContingencyTableCellKey(
						key);
				double difference = localProbs.getNormalizedValue(cellKey)
						- globalProbs.getNormalizedValue(cellKey);
				Color color = getColor((float) difference);
				plot.addAnnotation(new XYShapeAnnotation(new Rectangle2D.Float(
						x + leftPadding, y + bottomPadding, 1f, 1f),
						new BasicStroke(), color, color));
				XYTextAnnotation a = new XYTextAnnotation(String.format(
						Locale.ENGLISH, "%." + precision + "f", difference),
						0.5 + x + leftPadding, 0.5 + y + bottomPadding);
				a.setFont(cellFont);
				plot.addAnnotation(a);
				x += 1;
				if (x % numberOfCols == 0) {
					y += 1;
					x = 0;
				}
			}
		}
	}

	/*
	 * this seems to add the detailed categories
	 */
	private void addContingencyTableDetails(XYPlot plot, Attribute attribute,
			String direction, float padding) {
		double i = 0.5;
		List<String> categories = null;
		if (attribute instanceof CategoricalAttribute) {
			categories = ((CategoricalAttribute) attribute).getCategories();
		} else if (attribute instanceof MetricAttribute) {
			categories = new ArrayList<>();
			categories.add("lower half");
			categories.add("upper half");
		} else {
			try {
				throw new Exception("Unknown target attribute type: "
						+ attribute.getClass().getName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (direction != "vertical" && direction != "horizontal") {
			try {
				throw new Exception(
						"direction argument should be either 'horizontal' or 'vertical'");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (String category : categories) {
			XYTextAnnotation a = null;
			if (direction == "vertical") {
				a = new XYTextAnnotation(category, 0.2, padding + i);
				a.setRotationAngle(-Math.PI / 2);
			} else if (direction == "horizontal") {
				a = new XYTextAnnotation(category, padding + i, 0.2);
			}
			// a.setFont(new Font("Arial", Font.PLAIN, 12));
			a.setFont(uiStyle.textAnnotation);
			plot.addAnnotation(a);
			i++;
		}
	}

	public JFreeChart createGaussians(String title,
			List<GaussianTargetShiftVisualization.GaussianParams> gaussianParams, String xAxisLabel) {
		XYSeriesCollection gaussians = new XYSeriesCollection();
		int i = 0;
		for (GaussianTargetShiftVisualization.GaussianParams gaussianParam : gaussianParams) {
			if (gaussianParam.stDev == 0) {
				gaussians.addSeries(new XYSeries(""));
			} else {
				Function2D normal = new NormalDistributionFunction2D(
						gaussianParam.mean, gaussianParam.stDev);
				XYDataset dataset = DatasetUtilities.sampleFunction2D(normal,
						gaussianParam.mean - 4 * gaussianParam.stDev,
						gaussianParam.mean + 4 * gaussianParam.stDev, 50,
						"Normal" + i++);
				gaussians
						.addSeries(((XYSeriesCollection) dataset).getSeries(0));
			}
		}
		final JFreeChart chart = ChartFactory.createXYLineChart(title, xAxisLabel, "",
				gaussians, PlotOrientation.VERTICAL, false, true, false);
		formatJFreeChart(chart);

		XYPlot normalPlot = (XYPlot) chart.getPlot();
		normalPlot.getRenderer().setSeriesPaint(0, uiStyle.localPaint);
		normalPlot.getRenderer().setSeriesPaint(1, uiStyle.globalPaint);

		NumberAxis domainAxis = (NumberAxis) normalPlot.getDomainAxis();
		formatAxis(domainAxis);

		NumberAxis rangeAxis = (NumberAxis) normalPlot.getRangeAxis();
		formatAxis(rangeAxis);

		return chart;

		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);
	}

	public JFreeChart createPointCloudWithLines(String title,
			String xAxisTitle, String yAxisTitle,
			XYSeriesCollection pointsDataset,
			XYSeriesCollection linePointsDataset) {
		final JFreeChart chart = ChartFactory.createScatterPlot(title, "", "",
				null, PlotOrientation.VERTICAL, false, true, false);
		formatJFreeChart(chart);

		XYPlot cloudPlot = (XYPlot) chart.getPlot();
		cloudPlot.setDataset(0, linePointsDataset);
		cloudPlot.setDataset(1, pointsDataset);
		cloudPlot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);

		ValueAxis domainAxis = cloudPlot.getDomainAxis();
		domainAxis.setLabel(xAxisTitle);
		formatAxis(domainAxis);

		ValueAxis rangeAxis = cloudPlot.getRangeAxis();
		rangeAxis.setLabel(yAxisTitle);
		formatAxis(rangeAxis);

		XYItemRenderer pointsRenderer = cloudPlot.getRenderer();
		pointsRenderer.setSeriesPaint(0, uiStyle.globalPaint);
		pointsRenderer.setSeriesPaint(1, uiStyle.localPaint);
		double size = 2.0;
		double delta = size / 2.0;
		Shape circle = new Ellipse2D.Double(-delta, -delta, size, size);
		Shape rectangle = new Rectangle2D.Double(-delta, -delta, size, size);
		pointsRenderer.setSeriesShape(0, circle);
		pointsRenderer.setSeriesShape(1, rectangle);
		cloudPlot.setRenderer(1, pointsRenderer);

		final XYLineAndShapeRenderer linePointsRenderer = new XYLineAndShapeRenderer();
		linePointsRenderer.setSeriesPaint(0, Color.black);
		linePointsRenderer.setSeriesLinesVisible(0, true);
		linePointsRenderer.setSeriesShapesVisible(0, false);

		linePointsRenderer.setSeriesPaint(1, Color.green);
		linePointsRenderer.setSeriesLinesVisible(1, true);
		linePointsRenderer.setSeriesShapesVisible(1, false);
		cloudPlot.setRenderer(0, linePointsRenderer);

		return chart;
		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);
	}

	public JFreeChart createPointCloud(String title,
			List<List<Point>> pointClouds, String xAxisTitle,
			String yAxisTitle) {
		XYSeriesCollection cloud = new XYSeriesCollection();
		int i = 0;
		List<Double> meansX = newArrayList(), minsX = newArrayList(), maxsX = newArrayList(), meansY = newArrayList(), minsY = newArrayList(), maxsY = newArrayList();
		for (List<Point> pointCloud : pointClouds) {
			i++;
			String seriesName = "cloud" + i;
			XYSeries pointCloudAsSeries = new XYSeries(seriesName);
			for (Point point : pointCloud) {
				pointCloudAsSeries.add(point.x, point.y);
			}
			cloud.addSeries(pointCloudAsSeries);

			double meanx = 0, meany = 0, minX = Double.MAX_VALUE, maxX = -Double.MAX_VALUE, minY = Double.MAX_VALUE, maxY = -Double.MAX_VALUE;
			for (Point point : pointCloud) {
				meanx += point.x;
				minX = Math.min(minX, point.x);
				maxX = Math.max(maxX, point.x);
				meany += point.y;
				minY = Math.min(minY, point.y);
				maxY = Math.max(maxY, point.y);
			}
			meansX.add(meanx / pointCloud.size());
			minsX.add(minX);
			maxsX.add(maxX);
			meansY.add(meany / pointCloud.size());
			minsY.add(minY);
			maxsY.add(maxY);
		}

		final JFreeChart chart = ChartFactory.createScatterPlot(title, "", "",
				cloud, PlotOrientation.VERTICAL, false, true, false);
		formatJFreeChart(chart);

		XYPlot cloudPlot = (XYPlot) chart.getPlot();
		cloudPlot.getRenderer(0).setSeriesPaint(0, uiStyle.globalPaint);
		cloudPlot.getRenderer(0).setSeriesPaint(1, uiStyle.localPaint);
		cloudPlot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);

		cloudPlot.addAnnotation(new XYLineAnnotation(meansX.get(0), minsY
				.get(0), meansX.get(0), maxsY.get(0), new BasicStroke(),
				Color.orange));
		cloudPlot.addAnnotation(new XYLineAnnotation(minsX.get(0), meansY
				.get(0), maxsX.get(0), meansY.get(0), new BasicStroke(),
				Color.orange));

		ValueAxis domainAxis = cloudPlot.getDomainAxis();
		domainAxis.setLabel(xAxisTitle);
		formatAxis(domainAxis);

		ValueAxis rangeAxis = cloudPlot.getRangeAxis();
		rangeAxis.setLabel(yAxisTitle);
		formatAxis(rangeAxis);

		XYItemRenderer renderer = cloudPlot.getRenderer();
		renderer.setSeriesPaint(0, Color.blue);
		double size = 2.0;
		double delta = size / 2.0;
		Shape circle = new Ellipse2D.Double(-delta, -delta, size, size);
		Shape rectangle = new Rectangle2D.Double(-delta, -delta, size, size);
		renderer.setSeriesShape(0, circle);
		renderer.setSeriesShape(1, rectangle);

		return chart;
	}

	public JFreeChart createPieChart(String title, String[] items,
			double[] values, boolean flag) {

		if (items.length != values.length) {
			throw new IllegalArgumentException("values length ("
					+ String.valueOf(values.length)
					+ ") does not match items length ("
					+ String.valueOf(items.length) + ")");
		}
		DefaultPieDataset dataset = new DefaultPieDataset();
		for (int i = 0; i < items.length; i++) {
			dataset.setValue(items[i], values[i]);
		}
		JFreeChart chart = ChartFactory.createPieChart(title, dataset, true,
				false, true);
		formatJFreeChart(chart);

		PiePlot piePlot = (PiePlot) chart.getPlot();
		piePlot.setLegendLabelGenerator(new StandardPieSectionLabelGenerator(
				"{0}"));

		if (items.length == 2) {
			piePlot.setSectionPaint(items[0], uiStyle.localPaint);
			piePlot.setSectionPaint(items[1], uiStyle.globalPaint);
		}

		LegendTitle legend = chart.getLegend();
		legend.setMargin(0, 0, 0, 0);

		Color transparent = new Color(0.0f, 0.0f, 0.0f, 0.0f);
		piePlot.setLabelOutlinePaint(transparent);
		piePlot.setLabelBackgroundPaint(transparent);
		piePlot.setLabelShadowPaint(transparent);

		formatJFreeChart(chart);

		if (flag == true) {
			// legend.setPosition(RectangleEdge.TOP);
			// legend.setHorizontalAlignment(HorizontalAlignment.CENTER);
			// legend.setBackgroundPaint(Color.WHITE);
			// piePlot.setLegendItemShape(new Rectangle(7, 7));
			// legend.setItemFont((new Font("Arial", Font.PLAIN, font)));
			legend.visible = false;
			piePlot.setSimpleLabels(true);

			piePlot.setLabelDistributor(new AbstractPieLabelDistributor() {
				@Override
				public int getItemCount() {
					return 1;
				}

				public void distributeLabels(double minY, double height) {
				}
			});
			// piePlot.setLabelFont(new Font("Arial", Font.PLAIN, font));
			piePlot.setLabelFont(uiStyle.labelFont);
			piePlot.setLabelGap(-0.3);
			piePlot.setMaximumLabelWidth(0.3);
		} else if (flag == false) {
			// legend.setItemPaint(Color.LIGHT_GRAY);
			// ......legend item paint changed to black
			legend.setItemPaint(UIStyle.getInstance().itemPaint);
			legend.setPosition(RectangleEdge.BOTTOM);
			legend.setHorizontalAlignment(HorizontalAlignment.CENTER);
			legend.setBackgroundPaint(null);
			// legend.setItemFont((new Font("Arial", Font.PLAIN, 7)));
			legend.setItemFont(uiStyle.legendFont);
			piePlot.setLegendItemShape(new Rectangle(16, 9));
			piePlot.setLabelGenerator(null);
		}

		piePlot.setLabelLinksVisible(false);
		piePlot.setBackgroundAlpha(uiStyle.TRANSPARENT);
		piePlot.setOutlinePaint(null);
		piePlot.setShadowPaint(null);

		return chart;

		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);
	}

	public JFreeChart createBoxPlotChart(String title,
			DefaultBoxAndWhiskerCategoryDataset boxDataset, String valueAxisLabel) {

		String categoryAxisLabel = "";
		boolean legend = false;

		JFreeChart chart = ChartFactory.createBoxAndWhiskerChart(title,
				categoryAxisLabel, valueAxisLabel, boxDataset, legend);
		formatJFreeChart(chart);

		CategoryPlot categoryplot = chart.getCategoryPlot();
		categoryplot.setOrientation(PlotOrientation.HORIZONTAL);
		categoryplot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);

		CategoryAxis domainAxis = categoryplot.getDomainAxis();
		domainAxis.setTickLabelsVisible(true);
		domainAxis.setMaximumCategoryLabelLines(1);
		domainAxis.setMaximumCategoryLabelWidthRatio((float) 0.3);
		formatAxis(domainAxis);

		ValueAxis rangeAxis = categoryplot.getRangeAxis();

		double min = BoxPlotUtils.getMinRange(boxDataset,
				boxDataset.getRowCount() - 1, 0);
		double max = BoxPlotUtils.getMaxRange(boxDataset,
				boxDataset.getRowCount() - 1, 0);
		double diff = Math.abs(max - min);
		double minRange = min - diff * 0.1;
		double maxRange = max + diff * 0.1;
		((NumberAxis) rangeAxis).setRange(minRange, maxRange);
		formatAxis(rangeAxis);

		BoxAndWhiskerRenderer boxandwhiskerrender = new BoxAndWhiskerRendererWithOutliers();
		categoryplot.setRenderer(boxandwhiskerrender);
		boxandwhiskerrender.setMaximumBarWidth(0.2);

		if (boxDataset.getRowCount() == 2) {
			boxandwhiskerrender.setSeriesPaint(0, uiStyle.localPaint);
			boxandwhiskerrender.setSeriesPaint(1, uiStyle.globalPaint);
		} else {
			boxandwhiskerrender.setSeriesPaint(0, uiStyle.globalPaint);
			domainAxis.setLabelPaint(Color.white);
			domainAxis.setTickLabelPaint(Color.white);
			rangeAxis.setLabelPaint(Color.white);
			rangeAxis.setTickLabelPaint(Color.white);
		}

		boxandwhiskerrender.setMeanVisible(true);
		boxandwhiskerrender.setMedianVisible(true);

		return chart;
		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);

	}

	public JFreeChart createHistogramPlot(String title, List<Double> values) {

		final int bins = 9;
		double[] value = new double[values.size()];
		for (int i = 0; i < values.size(); i++) {
			value[i] = values.get(i);
		}

		HistogramDataset histogramDataset = new HistogramDataset();
		histogramDataset.addSeries("key", value, bins);

		String xAxisLabel = "";
		String yAxisLabel = "";
		boolean legend = false;
		boolean tooltips = false;
		boolean urls = false;

		JFreeChart chart = ChartFactory.createHistogram(title, xAxisLabel,
				yAxisLabel, histogramDataset, PlotOrientation.VERTICAL, legend,
				tooltips, urls);
		formatJFreeChart(chart);

		XYPlot xyplot = chart.getXYPlot();
		xyplot.getRenderer(0).setSeriesPaint(0, uiStyle.localPaint);
		xyplot.getRenderer(0).setSeriesPaint(1, uiStyle.globalPaint);

		// xyplot.setBackgroundAlpha(uiStyle.TRANSPARENT);
		ValueAxis domainAxis = xyplot.getDomainAxis();
		formatAxis(domainAxis);

		ValueAxis rangeAxis = xyplot.getRangeAxis();
		formatAxis(rangeAxis);

		// test!!
		domainAxis.setLabelPaint(Color.white);
		domainAxis.setTickLabelPaint(Color.white);
		rangeAxis.setLabelPaint(Color.white);
		rangeAxis.setTickLabelPaint(Color.white);

		return chart;

		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);
	}

	public JFreeChart createLayeredBarChart(String title, String[] rows,
			String[] columns, double[] values) {
		if (rows.length != columns.length || rows.length != values.length) {
			throw new IllegalArgumentException("array lengths do not match");
		}
		DefaultCategoryDataset categorydataset = new DefaultCategoryDataset();

		for (int i = 0; i < values.length; i++) {
			categorydataset.addValue(values[i], rows[i], columns[i]);
		}

		String value = "";
		String categ = "";
		boolean hasLegend = false;
		boolean hasTooltip = false;
		boolean url = true;
		JFreeChart chart = ChartFactory.createBarChart(title, categ, value,
				categorydataset, PlotOrientation.VERTICAL, hasLegend,
				hasTooltip, url);
		formatJFreeChart(chart);

		CategoryPlot categoryplot = chart.getCategoryPlot();
		categoryplot.setDomainGridlinePaint(uiStyle.domainGridLinePaint);
		categoryplot.setRowRenderingOrder(SortOrder.DESCENDING);

		CategoryAxis domainAxis = categoryplot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		domainAxis.setMaximumCategoryLabelWidthRatio((float) 0.7);
		domainAxis.setTickLabelsVisible(true);
		formatAxis(domainAxis);

		/*
		 * for (int i = 0; i < columns.length; i++) { if (i != 0 && i !=
		 * columns.length / 2 && i != columns.length - 1 && i != columns.length
		 * / 2 - 1) { domainAxis.setTickLabelPaint(columns[i], new Color(0, 0,
		 * 0, Color.OPAQUE)); } }
		 */

		ValueAxis rangeAxis = categoryplot.getRangeAxis();
		formatAxis(rangeAxis);

		LayeredBarRenderer layeredbarrenderer = new LayeredBarRenderer();
		categoryplot.setRenderer(layeredbarrenderer);
		layeredbarrenderer.setSeriesPaint(0, uiStyle.localPaint);
		layeredbarrenderer.setSeriesPaint(1, uiStyle.globalPaint);

		return chart;

		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);
	}

	public JFreeChart createStackedBarChart(String title, String[] rows,
			String[] columns, double[] values) {

		if (rows.length != columns.length || rows.length != values.length) {
			throw new IllegalArgumentException("array lengths do not match");
		}
		DefaultCategoryDataset categorydataset = new DefaultCategoryDataset();
		for (int i = 0; i < values.length; i++) {
			categorydataset.addValue(values[i], rows[i], columns[i]);
		}

		String value = "log probability";
		String categ = "";
		boolean hasLegend = false;
		boolean hasTooltip = false;
		boolean url = true;

		JFreeChart chart = ChartFactory.createStackedBarChart(title, categ,
				value, categorydataset, PlotOrientation.HORIZONTAL, hasLegend,
				hasTooltip, url);
		formatJFreeChart(chart);

		CategoryPlot categoryplot = chart.getCategoryPlot();

		categoryplot.setDomainGridlinesVisible(false);

		categoryplot.setRowRenderingOrder(SortOrder.ASCENDING);
		categoryplot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);

		CategoryAxis categoryAxis = categoryplot.getDomainAxis();
		categoryAxis.setTickLabelsVisible(false);
		categoryAxis.setVisible(false);
		categoryAxis.setLabel("log prob.");

		CategoryAxis domainAxis = categoryplot.getDomainAxis();
		domainAxis.setLowerMargin(0.15);
		formatAxis(domainAxis);

		ValueAxis rangeAxis = categoryplot.getRangeAxis();
		formatAxis(rangeAxis);

		for (String string : columns) {
			CategoryTextAnnotation categorytextannotation = new CategoryTextAnnotation(
					string, string, 0.0D);
			// categorytextannotation.setFont(new Font("Arial", Font.PLAIN,
			// font));
			categorytextannotation.setFont(uiStyle.textAnnotation);
			categorytextannotation.setTextAnchor(TextAnchor.CENTER_RIGHT);
			categorytextannotation.setCategoryAnchor(CategoryAnchor.START);
			categoryplot.addAnnotation(categorytextannotation);
		}

		StackedBarRenderer renderer = (StackedBarRenderer) categoryplot
				.getRenderer();
		renderer.setDrawBarOutline(true);
		renderer.setBarPainter(new StandardBarPainter());
		renderer.setSeriesPaint(0, new Color(0xff, 0xae, 0xb9));
		// renderer.setSeriesOutlinePaint(0, Color.gray);
		renderer.setSeriesOutlinePaint(0, uiStyle.outLinePaint);

		for (int seriesIndex = 1; seriesIndex < values.length; seriesIndex++) {
			renderer.setSeriesPaint(seriesIndex, new Color(0x87, 0xCE, 0xff));
			// renderer.setSeriesOutlinePaint(seriesIndex, Color.gray);
			renderer.setSeriesOutlinePaint(seriesIndex, uiStyle.outLinePaint);
		}

		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		renderer.setMaximumBarWidth(0.25);

		return chart;
		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);
	}

	// private String formatHtmlURL(HttpSession session, JFreeChart chart,
	// ChartRenderingInfo info) throws IOException {
	// String result;
	// result = ServletUtilities.saveChartAsPNG(chart, width, height, info,
	// session);
	// result = session.getServletContext().getContextPath()
	// + SERVLET_CHART_URL_PREFIX + result;
	// return result;
	// }

	// public String createNoPic(HttpSession session) {
	// return session.getServletContext().getContextPath()
	// + SERVLET_CHART_URL_PREFIX + "nopic.jpg";
	// }

	/*
	 * from red to blue
	 */
	private Color getColor(float diff) {
		float rLow = 1f, gLow = 1f, bLow = 1f;
		float rHigh = 1f, gHigh = 0f, bHigh = 0f;
		float vLow = 0, vHigh = 1;
		if (diff > 0f) {
			rHigh = 0f;
			gHigh = 0f;
			bHigh = 1f;
		}
		float x = (float) Math.sqrt(Math.abs(diff));

		float prop = (x - vLow) / (vHigh - vLow);

		float rVal = rLow + prop * (rHigh - rLow);
		float gVal = gLow + prop * (gHigh - gLow);
		float bVal = bLow + prop * (bHigh - bLow);

		return new Color(rVal, gVal, bVal);
	}

	/**
	 * creates a list of Points for all pairs of attribute values for indices in
	 * rows where both attribute values are present
	 */
	public static List<Point> createPoints(Set<Integer> rows,
			Attribute attribute1, Attribute attribute2) {
		List<Point> pointCloud = newArrayList();
		for (Integer row : rows) {
			if (attribute1.isValueMissing(row)
					|| attribute2.isValueMissing(row)) {
				continue;
			}
			pointCloud.add(new Point(((MetricAttribute) attribute1)
					.getValue(row), ((MetricAttribute) attribute2)
					.getValue(row)));
		}
		return pointCloud;
	}

	@SuppressWarnings("serial")
	public class DataArrayLengthException extends Exception {

		public DataArrayLengthException(String details) {
			super(details);
		}
	}

	public static class Point {
		public double x;
		public double y;

		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
	}
}