/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table.attribute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import de.unibonn.realkd.data.table.attribute.Attribute;

public class DefaultAttribute<T> implements Attribute<T> {

	private final String name;

	private final String description;

	private final List<T> values;

	private final Collection<T> nonMissingValues;

	private final SortedSet<Integer> missingPositions;

	/**
	 * @param values
	 *            list of T-values that can contain null entries to indicate
	 *            missing values
	 */
	public DefaultAttribute(String name, String description, List<T> values) {
		this.name = name;
		this.description = description;
		this.values = new ArrayList<>(values);
		this.missingPositions = new TreeSet<>();
		this.nonMissingValues = new ArrayList<>();
		initCachedMissingPositionsInformation();
	}

	private void initCachedMissingPositionsInformation() {
		for (int i = 0; i < values.size(); i++) {
			if (values.get(i) == null) {
				missingPositions.add(i);
			} else {
				nonMissingValues.add(values.get(i));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#isValueMissing
	 * (int)
	 */
	@Override
	public boolean isValueMissing(int objectId) {
		return missingPositions.contains(objectId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#getValue(int)
	 */
	@Override
	public T getValue(int objectId) {
		if (values.get(objectId) == null) {
			throw new IllegalArgumentException("value for object " + objectId
					+ " missing");
		}
		return values.get(objectId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#getNonMissingValues
	 * ()
	 */
	@Override
	public Collection<T> getNonMissingValues() {
		return nonMissingValues;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#
	 * getNumberOfNonMissingValues()
	 */
	@Override
	public int getNumberOfNonMissingValues() {
		return values.size() - missingPositions.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#getMaxIndex()
	 */
	@Override
	public int getMaxIndex() {
		return this.values.size() - 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fraunhofer.iais.ocm.mining.data.attribute.Attribute#getMissingPositions
	 * ()
	 */
	@Override
	public SortedSet<Integer> getMissingPositions() {
		return missingPositions;
	}

	public String toString() {
		return name;
	}

}
