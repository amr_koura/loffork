/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table.attribute;

import java.util.Collection;
import java.util.SortedSet;

import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * Interface for data attributes that aggregate a list of values, each of which
 * is associated with an underlying (virtual) data object (corresponding to
 * integer indices). Values can be missing for any or all indices.
 * 
 * @author mboley
 * 
 * @param <T>
 *            the value type of the attribute
 */
public interface Attribute<T> {

	/**
	 * @return short screen name of attribute that can be displayed to users
	 */
	public abstract String getName();

	/**
	 * 
	 * @return extended screen description of attribute that can be displayed to
	 *         users in order to provide detailed information
	 */
	public abstract String getDescription();

	/**
	 * Checks if value for an object is present or missing. Only indices of
	 * non-missing objects can be used as argument for getValue
	 * 
	 * @see Attribute#getValue
	 * @param objectIndex
	 * @return whether value for object is missing
	 */
	public abstract boolean isValueMissing(int objectIndex);

	/**
	 * Returns the value corresponding to the object with the specified index if
	 * that value is non-missing or otherwise throws an Exception.
	 * 
	 * @see Attribute
	 * @param objectIndex
	 * @return the value for an object associated with objectIndex if
	 *         non-missing
	 * @throws IllegalArgumentException
	 *             if value is missing
	 */
	public abstract T getValue(int objectIndex);

	/**
	 * Returns the bag of all values of indices that are non-missing. May
	 * contain duplicate values.
	 * 
	 * @return bag of all non-missing values
	 */
	public abstract Collection<T> getNonMissingValues();

	/**
	 * Returns number of indices between 0 and maximal index for which values
	 * are non-missing
	 * 
	 * @return number of non-missing values
	 */
	public abstract int getNumberOfNonMissingValues();

	/**
	 * Returns the maximum integer index that can be accepted as an argument for
	 * isMissing (and getValue if not missing).
	 * 
	 * @see Attribute#isValueMissing(int)
	 * @see Attribute#getValue(int)
	 * @return maximum object index
	 * 
	 */
	public abstract int getMaxIndex();

	/**
	 * @return The row ids of rows where this attribute has/had a missing value
	 */
	public abstract SortedSet<Integer> getMissingPositions();

}