/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.DefaultAttribute;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;
import de.unibonn.realkd.data.table.attributegroups.JointMacroAttribute;

public class AttributeGroupsStore {

	private List<AttributeGroup> attributeGroups;

	private final Map<DefaultAttribute, Collection<AttributeGroup>> attributeGroupsOf = new LinkedHashMap<DefaultAttribute, Collection<AttributeGroup>>() {
		public Collection<AttributeGroup> get(Object key) {
			Collection<AttributeGroup> list = super.get(key);
			if (list == null && key instanceof DefaultAttribute)
				super.put((DefaultAttribute) key,
						list = new ArrayList<AttributeGroup>());
			return list;
		}
	};

	public AttributeGroupsStore() {
		this.attributeGroups = new ArrayList<>();
	}

	public List<AttributeGroup> getAttributeGroups() {
		return attributeGroups;
	}

	public Collection<AttributeGroup> getAttributeGroupsOf(Attribute attribute) {
		return this.attributeGroupsOf.get(attribute);
	}

	public void addAttributeGroup(AttributeGroup group) {
		this.attributeGroups.add(group);
		for (Object attribute : group.getElements()) {
			Collection<AttributeGroup> groups = this.attributeGroupsOf
					.get(attribute);
			groups.add(group);
		}
	}

	public boolean isPartOfMacroAttributeWithAtLeastOneOf(Attribute attribute,
			Collection<Attribute> otherAttributes) {
		for (Attribute otherAttribute : otherAttributes) {
			if (isPartOfMacroAttributeWith(attribute, otherAttribute)) {
				return true;
			}
		}
		return false;
	}

	public boolean isPartOfMacroAttributeWith(Attribute attribute,
			Attribute otherAttribute) {
		for (AttributeGroup group : getAttributeGroupsOf(attribute)) {
			if (!(group instanceof JointMacroAttribute)) {
				continue;
			}
			if (group.getElements().contains(otherAttribute)) {
				return true;
			}
		}

		return false;
	}
}
