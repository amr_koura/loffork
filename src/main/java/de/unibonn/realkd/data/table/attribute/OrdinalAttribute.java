/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table.attribute;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * 
 * @author bjacobs
 * 
 */
public interface OrdinalAttribute<T extends Comparable> extends Attribute<T> {

	public T getMedian();

	public T getMax();

	public T getMin();

	/**
	 * 
	 * @param frac
	 *            double from the closed unit interval (0,1]
	 * @return the smallest value p such that at least frac objects have
	 *         attribute value p or more
	 */
	public T getPercentile(double frac);

	public List<Integer> getSortedNonMissingRowIndices();

	public T getLowerQuartile();

	public T getUpperQuartile();

	public T getMedianOnRows(Set<Integer> rowSet);

	public Comparator<T> getComparator();

}
