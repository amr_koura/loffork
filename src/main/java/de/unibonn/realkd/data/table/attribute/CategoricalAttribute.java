/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table.attribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.unibonn.realkd.data.table.attribute.DefaultAttribute;

public class CategoricalAttribute extends DefaultAttribute<String> {

	private final List<String> categories;

	private final List<Double> categoryFrequencies;

	public CategoricalAttribute(String name, String description,
			List<String> values) {
		super(name, description, values);
		// numOfDataTableRows = values.size();
		categories = new ArrayList<>();
		categoryFrequencies = new ArrayList<>();
		Map<String, Double> categoryToFrequency = new HashMap<>();
		for (String value : getNonMissingValues()) {
			if (categoryToFrequency.containsKey(value)) {
				double newCount = categoryToFrequency.get(value) + 1.0;
				categoryToFrequency.put(value, newCount);
			} else {
				categoryToFrequency.put(value, 1.0);
			}
		}
		for (String category : categoryToFrequency.keySet()) {
			getCategories().add(category);
			getCategoryFrequencies().add(
					categoryToFrequency.get(category)
							/ getNumberOfNonMissingValues());
		}
	}

	public List<Double> getCategoryFrequencies() {
		return categoryFrequencies;
	}

	public List<String> getCategories() {
		return categories;
	}

	public List<Double> getCategoryFrequenciesOnRows(Set<Integer> rowSet) {
		Map<String, Double> categoryToFrequency = new HashMap<>();
		int numberOfNonMissingValues = 0;
		for (Integer row : rowSet) {
			if (isValueMissing(row)) {
				continue;
			}
			String value = getValue(row);
			if (categoryToFrequency.containsKey(value)) {
				double newCount = categoryToFrequency.get(value) + 1.0;
				categoryToFrequency.put(value, newCount);
			} else {
				categoryToFrequency.put(value, 1.0);
			}
			numberOfNonMissingValues++;
		}

		List<Double> result = new ArrayList<>(getCategories().size());
		for (int i = 0; i < getCategories().size(); i++) {
			if (categoryToFrequency.containsKey(getCategories().get(i))) {
				result.add(categoryToFrequency.get(getCategories().get(i))
						/ (double) numberOfNonMissingValues);
			} else {
				result.add(0.0);
			}
		}

		return result;
	}

}
