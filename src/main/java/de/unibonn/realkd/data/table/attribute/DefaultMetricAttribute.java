/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table.attribute;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.table.attribute.DefaultAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

public class DefaultMetricAttribute extends DefaultAttribute<Double> implements OrdinalAttribute<Double>, MetricAttribute {

	/**
	 * Compares two data object indices based on the value that they have for
	 * some specific numeric attribute. Does not test for missing values (hence
	 * can be used only on indices for both of which values are present).
	 * 
	 * @author mboley
	 * 
	 */
	private class IndexComparator implements Comparator<Integer> {

		private MetricAttribute attribute;

		public IndexComparator(MetricAttribute attribute) {
			this.attribute = attribute;
		}

		public int compare(Integer i, Integer j) {
			if (attribute.getValue(i) < attribute.getValue(j)) {
				return -1;
			} else if (attribute.getValue(i) > attribute.getValue(j)) {
				return 1;
			}
			return 0;
		}

	}

	public class DoubleComparator implements Comparator<Double> {
		@Override
		public int compare(Double o1, Double o2) {
			return o1.compareTo(o2);
		}
	}

	private double min, max, mean, median;

	private List<Integer> sortedIndices;

	private double variance, thirdCentralMoment;

	private double lowerQuartile;

	private double upperQuartile;

	private final DoubleComparator comparator;

	public DefaultMetricAttribute(String name, String description, List<Double> values) {
		super(name, description, values);

		this.comparator = new DoubleComparator();

		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;
		mean = 0.0;
		median = 0.0;

		for (int i = 0; i < values.size(); i++) {
			Double d = values.get(i);
			if (d != null) {
				if (d < min) {
					min = d;
				}
				if (d > max) {
					max = d;
				}
				mean += d;
			}
		}

		mean /= (values.size() - getMissingPositions().size());

		variance = 0.0;
		thirdCentralMoment = 0.0;

		List<Integer> sortedNonMissinRowIndices = new ArrayList<>(values.size());
		for (int i = 0; i < values.size(); i++) {
			if (!getMissingPositions().contains(i)) {
				sortedNonMissinRowIndices.add(i);
				variance += (mean - values.get(i)) * (mean - values.get(i))
						/ values.size();
				thirdCentralMoment += (mean - values.get(i))
						* (mean - values.get(i)) * (mean - values.get(i))
						/ values.size();

			}
		}
		Collections.sort(sortedNonMissinRowIndices, new IndexComparator(this));

		this.sortedIndices = sortedNonMissinRowIndices;

		int lowerHalfPosition = (int) (sortedNonMissinRowIndices.size() / 2);
		if (sortedNonMissinRowIndices.size() % 2 == 0) {
			median = (values.get(sortedNonMissinRowIndices
					.get(lowerHalfPosition)) + values
					.get(sortedNonMissinRowIndices.get(lowerHalfPosition + 1))) / 2.0;
		} else {
			median = values.get(sortedNonMissinRowIndices
					.get(lowerHalfPosition + 1));
		}

		int lowerQuarterPosition = (int) (sortedNonMissinRowIndices.size() / 4);
		if (sortedNonMissinRowIndices.size() % 4 == 0) {
			lowerQuartile = (values.get(sortedNonMissinRowIndices
					.get(lowerQuarterPosition)) + values
					.get(sortedNonMissinRowIndices
							.get(lowerQuarterPosition + 1))) / 2.0;
		} else {
			lowerQuartile = values.get(sortedNonMissinRowIndices
					.get(lowerQuarterPosition + 1));
		}

		int upperQuarterPosition = (int) (3 * sortedNonMissinRowIndices.size() / 4);
		if ((3 * sortedNonMissinRowIndices.size()) % 4 == 0) {
			upperQuartile = (values.get(sortedNonMissinRowIndices
					.get(upperQuarterPosition)) + values
					.get(sortedNonMissinRowIndices
							.get(upperQuarterPosition + 1))) / 2.0;
		} else {
			upperQuartile = values.get(sortedNonMissinRowIndices
					.get(upperQuarterPosition + 1));
		}

		/*
		 * for (Integer i : getMissingPositions()) { values.set(i,
		 * String.valueOf(median)); }
		 */
	}

	/**
	 * 
	 * @param frac
	 *            double from the closed unit interval (0,1]
	 * @return the smallest value p such that at least frac objects have
	 *         attribute value p or more
	 */
	public Double getPercentile(double frac) {
		int n = (int) Math.ceil(frac * sortedIndices.size() - 1);
		return getValue(sortedIndices.get(n));
	}

	@Override
	public double getMean() {
		return mean;
	}

	@Override
	public Double getMedian() {
		return median;
	}

	@Override
	public Double getMax() {
		return max;
	}

	@Override
	public Double getMin() {
		return min;
	}

	@Override
	public double getVariance() {
		return variance;
	}

	@Override
	public double getStandardDeviation() {
		return Math.sqrt(variance);
	}

	@Override
	public double getSkew() {
		return thirdCentralMoment / Math.pow(getStandardDeviation(), 3);
	}

	public List<Integer> getSortedNonMissingRowIndices() {
		return sortedIndices;
	}

	public Double getLowerQuartile() {
		return lowerQuartile;
	}

	public Double getUpperQuartile() {
		return upperQuartile;
	}

	@Override
	public double getMeanOnRows(Set<Integer> rowSet) {
		double result = 0.0;
		int numberOfNonMissingValues = 0;

		for (int rowIndex : rowSet) {
			if (isValueMissing(rowIndex)) {
				continue;
			}
			result += getValue(rowIndex);
			numberOfNonMissingValues++;
		}

		if (numberOfNonMissingValues == 0) {
			return Double.NaN;
		}

		return result / numberOfNonMissingValues;
	}

	public Double getMedianOnRows(Set<Integer> rowSet) {
		double median;
		List<Double> nonMissingRowValues = new ArrayList<>();

		for (int rowIndex : rowSet) {
			if (isValueMissing(rowIndex)) {
				continue;
			}
			nonMissingRowValues.add(getValue(rowIndex));
		}

		if (nonMissingRowValues.isEmpty()) {
			return Double.NaN;
		}

		Collections.sort(nonMissingRowValues);
		if (nonMissingRowValues.size() % 2 == 0) {
			int ind = nonMissingRowValues.size() / 2 - 1;
			median = (nonMissingRowValues.get(ind) + nonMissingRowValues
					.get(ind + 1)) / 2.0;
		} else {
			median = nonMissingRowValues.get(nonMissingRowValues.size() / 2);
		}
		return median;
	}

	@Override
	public Comparator<Double> getComparator() {
		return comparator;
	}

}
