/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table;

import static de.unibonn.realkd.common.logger.LogChannel.DATA_INITIALIZATION;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import de.unibonn.realkd.data.table.CSV;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVBuilder;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultMetricAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultOrdinalAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalComparator;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;
import de.unibonn.realkd.data.table.attributegroups.DefaultAttributeGroup;
import de.unibonn.realkd.data.table.attributegroups.DefaultNumericSequenceWithAbsoluteDifferences;
import de.unibonn.realkd.data.table.attributegroups.JointMacroAttribute;

/**
 * Builder that creates a DataTable object from raw csv data.
 * 
 * @author bjacobs
 */
public class DataTableFromCSVBuilder {
	private static final int ATTRIBUTE_NAME_COLUMN = 0;
	private static final int ATTRIBUTE_TYPE_COLUMN = 1;
	private static final int ATTRIBUTE_DESCRIPTION_COLUMN = 2;
	private static final int ATTRIBUTE_VALUES_SPECIFICATION = 3;

	private static final String ATTRIBUTE_VALUES_SPECIFICATION_DELIMITER = ",";

	private static final String TYPE_METRIC = "numeric";
	private static final String TYPE_ORDINAL = "ordinal";
	private static final String TYPE_CATEGORICAL = "categoric";
	private static final String TYPE_NAME = "name";

	private static final int ATTRIBUTE_GROUP_NAME_INDEX = 1;
	private static final int ATTRIBUTE_GROUP_TYPE_INDEX = 2;
	private static final int ATTRIBUTE_GROUP_CONTENT_INDEX = 3;
	private static final char ATTRIBUTE_GROUP_FIELD_DELIMITER = ';';
	private static final String ATTRIBUTE_GROUP_2ND_FIELD_DELIMITER = ",";
	private static final String ATTRIBUTE_GROUP_3RD_FIELD_DELIMITER = ":";

	private String name = "New Dataset";
	private String description = "";
	private char delimiter = ';';
	private String missingSymbol = "?";
	private String attributeCSV = null;
	private String attributeGroupCSV = "";
	private String dataCSV = null;

	public DataTableFromCSVBuilder() {
		;
	}

	/**
	 * Name of the datatable to be build.
	 */
	public DataTableFromCSVBuilder setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Description of the datatable to be build.
	 */
	public DataTableFromCSVBuilder setDescription(String description) {
		this.description = description;
		return this;
	}

	/**
	 * Delimiter used in CSV strings to separate values (default ';').
	 */
	public DataTableFromCSVBuilder setDelimiter(Character delimiter) {
		this.delimiter = delimiter;
		return this;
	}

	/**
	 * Symbol used in CSV strings to indicate missing values (default '?').
	 */
	public DataTableFromCSVBuilder setMissingSymbol(String symbol) {
		this.missingSymbol = symbol;
		return this;
	}

	public DataTableFromCSVBuilder setAttributeMetadataCSV(String attributeCSV) {
		this.attributeCSV = attributeCSV;
		return this;
	}

	public DataTableFromCSVBuilder setAttributeGroupCSV(String attributeGroupCSV) {
		this.attributeGroupCSV = attributeGroupCSV;
		return this;
	}

	public DataTableFromCSVBuilder setDataCSV(String dataCSV) {
		this.dataCSV = dataCSV;
		return this;
	}

	public DataTable build() throws DataFormatException {
		if (attributeCSV == null) {
			throw new IllegalStateException(
					"Did not set attribute metadata csv.");
		}
		if (dataCSV == null) {
			throw new IllegalStateException("Did not set data csv.");
		}

		List<List<String>> rawAttributeData = CSV.convertCSVStringToList(
				attributeCSV, delimiter);

		List<List<String>> rawData = CSV.convertCSVStringToList(dataCSV,
				delimiter);

		DATA_INITIALIZATION.log("Creating attributes",
				LogMessageType.INTRA_COMPONENT_MESSAGE);

		List<Integer> nameAttributeIndices = getNameAttributeIndices(rawAttributeData);
		List<String> objectNames = composeObjectNames(nameAttributeIndices,
				rawData);

		List<Attribute> attributes = new ArrayList<>();
		List<Attribute> attributesForGroupConstruction = new ArrayList<>();

		// loop over all columns to create attributes (except the first, because
		// there only row names are stored)
		for (int i = 0; i < rawAttributeData.size(); i++) {
			// Filter values for the current attribute from raw data
			List<String> values = new ArrayList<>(rawData.size());
			for (List<String> aRawData : rawData) {
				values.add(aRawData.get(i));
			}

			List<String> currentRawAttrData = rawAttributeData.get(i);
			String attributeType = currentRawAttrData
					.get(ATTRIBUTE_TYPE_COLUMN);
			String attributeName = currentRawAttrData
					.get(ATTRIBUTE_NAME_COLUMN);
			String attributeDescription = currentRawAttrData
					.get(ATTRIBUTE_DESCRIPTION_COLUMN);

			{
				DefaultAttribute attribute = null;
				switch (attributeType) {
				case TYPE_CATEGORICAL:
					List<String> categoryValues = new ArrayList<>(values.size());
					for (String stringValue : values) {
						if (stringValue.equals(missingSymbol)) {
							categoryValues.add(null);
						} else {
							categoryValues.add(stringValue);
						}
					}
					attribute = new CategoricalAttribute(attributeName,
							attributeDescription, categoryValues);
					break;

				case TYPE_ORDINAL:
					// Special case for ordinal attributes, should have 4th column

					String attributeValueSpecificationString = currentRawAttrData.get(
							ATTRIBUTE_VALUES_SPECIFICATION);

					String[] split = attributeValueSpecificationString.split(
							ATTRIBUTE_VALUES_SPECIFICATION_DELIMITER);

					OrdinalComparator comparator = new OrdinalComparator(
							Arrays.asList(split));

					List<String> cleanedValues = new ArrayList<>(values.size());
					for (String stringValue : values) {
						if (stringValue.equals(missingSymbol)) {
							cleanedValues.add(null);
						} else {
							cleanedValues.add(stringValue);
						}
					}

					attribute = new DefaultOrdinalAttribute(attributeName,
							attributeDescription, cleanedValues, comparator);
					break;

				case TYPE_METRIC:
					List<Double> doubleValues = new ArrayList<>(values.size());
					for (String stringValue : values) {
						try {
							doubleValues.add(Double.parseDouble(stringValue));
						} catch (NumberFormatException nfe) {
							doubleValues.add(null);
						}
					}
					attribute = new DefaultMetricAttribute(attributeName,
							attributeDescription, doubleValues);

					break;

				case TYPE_NAME:
					attributesForGroupConstruction.add(EMPTY_ATTRIBUTE);
					continue;
				}

				attributes.add(attribute);
				attributesForGroupConstruction.add(attribute);
			}
		}

		DataTable dataTable = new DataTable(name, objectNames, attributes,
				description);

		DATA_INITIALIZATION.log("Creating attribute groups",
				LogMessageType.INTRA_COMPONENT_MESSAGE);
		List<List<String>> rawAttributeGroups = CSV.convertCSVStringToList(
				attributeGroupCSV, ATTRIBUTE_GROUP_FIELD_DELIMITER);

		for (List<String> groupData : rawAttributeGroups) {
			dataTable.getAttributeGroupsStore().addAttributeGroup(
					AttributeGroupFactory.getGroup(groupData,
							attributesForGroupConstruction));
		}

		// dataTable.initPropositionStore();

		return dataTable;
	}

	/**
	 * Either creates a list of numbers corresponding to the index or composes a
	 * list of names that are composed by the values of the name-attributes.
	 */
	private List<String> composeObjectNames(List<Integer> nameAttributeIndices,
			List<List<String>> rawData) {
		List<String> result = new ArrayList<>();
		if (nameAttributeIndices.size() == 0) {
			// Just add index if no name attributes present
			for (int i = 1; i <= rawData.size(); i++) {
				result.add(String.valueOf(i));
			}
		} else {
			// Otherwise compose name
			for (List<String> data : rawData) {
				StringBuffer objName = new StringBuffer();
				Iterator<Integer> iterator = nameAttributeIndices.iterator();
				while (iterator.hasNext()) {
					int k = iterator.next();
					objName.append(data.get(k));
					if (iterator.hasNext()) {
						objName.append(", ");
					}
				}
				result.add(objName.toString());
			}
		}
		return result;
	}

	private List<Integer> getNameAttributeIndices(
			List<List<String>> rawAttributeData) {
		List<Integer> result = new ArrayList<>();
		for (int i = 0; i < rawAttributeData.size(); i++) {
			List<String> data = rawAttributeData.get(i);
			String type = data.get(ATTRIBUTE_TYPE_COLUMN);
			if (TYPE_NAME.equals(type)) {
				result.add(i);
			}
		}
		return result;
	}

	private static final DefaultAttribute EMPTY_ATTRIBUTE = new DefaultAttribute<>(
			"empty", "empty", new ArrayList<String>());

	/**
	 * Factories that contain the parse logic for the different types of
	 * attribute groups
	 * 
	 * @author mboley
	 * 
	 */
	private static enum AttributeGroupFactory {

		JOINT_MACRO_ATTRIBUTE("joint_macro_attribute") {
			@Override
			public AttributeGroup getAttributeGroup(List<String> content,
					List<Attribute> attributeList) {

				List<String> stringGroupIndices = Arrays.asList(content.get(
						ATTRIBUTE_GROUP_CONTENT_INDEX).split(
						ATTRIBUTE_GROUP_2ND_FIELD_DELIMITER));

				return new JointMacroAttribute(
						content.get(ATTRIBUTE_GROUP_NAME_INDEX),
						toAttributeList(stringGroupIndices, attributeList));
			}
		},
		HIERARCHY("hierarchy") {
			@Override
			public AttributeGroup getAttributeGroup(List<String> content,
					List<Attribute> attributeList) {

				List<String> stringGroupIndices = Arrays.asList(content.get(
						ATTRIBUTE_GROUP_CONTENT_INDEX).split(
						ATTRIBUTE_GROUP_2ND_FIELD_DELIMITER));

				return new DefaultAttributeGroup(
						content.get(ATTRIBUTE_GROUP_NAME_INDEX),
						toAttributeList(stringGroupIndices, attributeList));
			}
		},
		SEQUENCE("sequence") {

			private static final int ATTRIBUTE_INDEX = 0; // within sequence
			// record
			private static final int ELEMENT_NAME_INDEX = 1; // within sequence

			// record

			@Override
			public AttributeGroup getAttributeGroup(List<String> content,
					List<Attribute> attributeList) {

				List<String> stringGroupRecords = Arrays.asList(content.get(
						ATTRIBUTE_GROUP_CONTENT_INDEX).split(
						ATTRIBUTE_GROUP_2ND_FIELD_DELIMITER));

				List<String> stringGroupIndices = new ArrayList<>(
						stringGroupRecords.size());
				List<String> elementNames = new ArrayList<>(
						stringGroupRecords.size());
				for (String groupRecord : stringGroupRecords) {
					String[] groupRecordElements = groupRecord
							.split(ATTRIBUTE_GROUP_3RD_FIELD_DELIMITER);
					stringGroupIndices
							.add(groupRecordElements[ATTRIBUTE_INDEX]);
					elementNames.add(groupRecordElements[ELEMENT_NAME_INDEX]);
				}

				return new DefaultNumericSequenceWithAbsoluteDifferences(
						content.get(ATTRIBUTE_GROUP_NAME_INDEX),
						toDoubleAttributeList(stringGroupIndices, attributeList),
						elementNames);
			}
		},
		CATEGORY_TAG("category_tag") {
			@Override
			public AttributeGroup getAttributeGroup(List<String> content,
					List<Attribute> attributeList) {

				List<String> stringGroupIndices = Arrays.asList(content.get(
						ATTRIBUTE_GROUP_CONTENT_INDEX).split(
						ATTRIBUTE_GROUP_2ND_FIELD_DELIMITER));

				return new DefaultAttributeGroup(
						content.get(ATTRIBUTE_GROUP_NAME_INDEX),
						toAttributeList(stringGroupIndices, attributeList));
			}
		};

		public static AttributeGroupFactory getFactoryMatchingDBRepresentation(
				String dbRepresentation) {
			for (AttributeGroupFactory factory : AttributeGroupFactory.values()) {
				if (factory.getDatabaseRepresentation()
						.equals(dbRepresentation)) {
					return factory;
				}
			}
			throw new IllegalArgumentException(
					"no AttributeGroupType matching string representation");
		}

		public static List<Attribute> toAttributeList(
				List<String> stringGroupElements, List<Attribute> attributeList) {
			List<Attribute> result = new ArrayList<>();
			for (String stringGroupElement : stringGroupElements) {
				int attributeIndex = Integer.parseInt(stringGroupElement) - 1; // contract
																				// adjustment
				result.add(attributeList.get(attributeIndex));
			}
			return result;
		}

		public static List<Attribute<Double>> toDoubleAttributeList(
				List<String> stringGroupElements, List<Attribute> attributeList) {
			List<Attribute<Double>> result = new ArrayList<>();
			for (String stringGroupElement : stringGroupElements) {
				int attributeIndex = Integer.parseInt(stringGroupElement) - 1; // contract
																				// adjustment
				result.add(attributeList.get(attributeIndex));
			}
			return result;
		}

		public static AttributeGroup getGroup(List<String> dbEntry,
				List<Attribute> attributeList) {
			AttributeGroupFactory factory = getFactoryMatchingDBRepresentation(dbEntry
					.get(ATTRIBUTE_GROUP_TYPE_INDEX));
			return factory.getAttributeGroup(dbEntry, attributeList);
		}

		private String dbRepresentationString;

		AttributeGroupFactory(String dbRepresentationString) {
			this.dbRepresentationString = dbRepresentationString;
		}

		private String getDatabaseRepresentation() {
			return this.dbRepresentationString;
		}

		public abstract AttributeGroup getAttributeGroup(List<String> content,
				List<Attribute> attributeList);
	}
}
