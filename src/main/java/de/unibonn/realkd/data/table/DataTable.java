/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import de.unibonn.realkd.data.DataArtifact;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

/**
 * Aggregation of data attributes over some fixed set of objects (which are
 * identified by consecutive indices from 0 to m-1 where m is referred to as the
 * size of the table).
 * 
 * @author mboley
 * 
 */
public class DataTable implements DataArtifact {

	private final String name;
	private final String description;

	private AttributeGroupsStore attributeGroupsStore;

	private final String[] objectNames;
	private final SortedSet<Integer> objectIds;

	/**
	 * This also holds the actual data
	 */
	@SuppressWarnings("rawtypes")
	private final List<Attribute> attributes;

	public DataTable(String name, List<String> objectNames,
			@SuppressWarnings("rawtypes") List<Attribute> attributes,
			String description) {
		checkNotNull(name);
		checkNotNull(objectNames);
		checkNotNull(attributes);
		checkNotNull(description);
		this.name = name;
		this.description = description;
		this.attributes = new ArrayList<>();
		this.attributes.addAll(attributes);
		this.attributeGroupsStore = new AttributeGroupsStore();
		this.objectNames = objectNames.toArray(new String[objectNames.size()]);

		this.objectIds = new TreeSet<>();
		for (int i = 0; i < objectNames.size(); i++) {
			objectIds.add(i);
		}

	}

	public boolean isNumeric(int attributeIndex) {
		return attributes.get(attributeIndex) instanceof MetricAttribute;
	}

	public boolean isEmpty() {
		return this.attributes.size() == 0;
	}

	/**
	 * Checks if object has at least one missing values.
	 */
	public boolean atLeastOneAttributeValueMissingFor(int objectId) {
		return atLeastOneAttributeValueMissingFor(objectId, attributes);
	}

	/**
	 * Checks if object has at least one missing values for any of the specified
	 * test attributes.
	 */
	public boolean atLeastOneAttributeValueMissingFor(int objectId,
			@SuppressWarnings("rawtypes") List<Attribute> testAttributes) {
		for (@SuppressWarnings("rawtypes")
		Attribute attribute : testAttributes) {
			if (attribute.isValueMissing(objectId)) {
				return true;
			}
		}
		return false;
	}

	public int getNumberOfAttributes() {
		return attributes.size();
	}

	public int getSize() {
		return objectNames.length;
	}

	public String getName() {
		return name;
	}

	public SortedSet<Integer> getObjectIds() {
		return objectIds;
	}

	public String getDescription() {
		return description;
	}

	public String getObjectName(int id) {
		if (id >= this.objectNames.length) {
			throw new IllegalArgumentException("invalid object id");
		}
		return this.objectNames[id];
	}

	public List<String> getAttributeNames() {
		List<String> res = new ArrayList<>();
		for (Attribute<?> attribute : getAttributes()) {
			res.add(attribute.getName());
		}
		return res;
	}

	public List<String> getObjectNames() {
		return Arrays.asList(objectNames);
	}

	@SuppressWarnings("rawtypes")
	public List<Attribute> getAttributes() {
		return attributes;
	}

	public Attribute<?> getAttribute(int attributeIndex) {
		if (attributeIndex < 0 || attributeIndex >= attributes.size()) {
			throw new IllegalArgumentException("invalid attribute index :"
					+ attributeIndex);
		}
		return attributes.get(attributeIndex);
	}

	public AttributeGroupsStore getAttributeGroupsStore() {
		return attributeGroupsStore;
	}

	@Override
	public String toString() {
		return getName();
	}

}
