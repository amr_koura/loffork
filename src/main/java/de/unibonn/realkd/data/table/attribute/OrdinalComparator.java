/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table.attribute;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author bjacobs
 *
 * Provides a comparator given an ordered list of String elements.
 * The result of the comparison is the difference between the indices of the
 * compared object in the list. A negative result means that the first element
 * provided comes before the second element in the list.
 */
public class OrdinalComparator implements Comparator<String> {
	private final List<String> orderedValueList;
	private final String invalidValueErrorMessage = "Invalid value for this comparator: \"%s\"! Valid values are: %s\n";
	private final String nullValueErrorMessage = "Invalid value. Cannot compare against null value!\n";


	public OrdinalComparator(List<String> orderedValueList) {
		this.orderedValueList = new ArrayList<>();
		this.orderedValueList.addAll(orderedValueList);
	}

	/**
	 * @return < 0 if first comes before second; 0 if both elements are the same; > 0 if second comes before first.
	 */
	@Override
	public int compare(String o1, String o2) {
		// Missing values are regarded like even lower than the lowest valid element
		if (o1 == null || o2 == null) {
			throw new IllegalArgumentException(nullValueErrorMessage);
		}

		int firstIndex = orderedValueList.indexOf(o1);
		if (firstIndex == -1) {
			throw new IllegalArgumentException(
					String.format(invalidValueErrorMessage, o1, orderedValueList));
		}

		int secondIndex = orderedValueList.indexOf(o2);
		if (secondIndex == -1) {
			throw new IllegalArgumentException(
					String.format(invalidValueErrorMessage, o2, orderedValueList));
		}

		return firstIndex - secondIndex;
	}
}
