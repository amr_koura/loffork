/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table.attribute;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.table.attribute.DefaultAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultOrdinalAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

/**
 * @author bjacobs
 */

public class DefaultOrdinalAttribute extends DefaultAttribute<String> implements OrdinalAttribute<String> {

	private final Comparator<String> comparator;
	private String min, max, median;

	private List<Integer> sortedIndices;

	private String lowerQuartile;

	private String upperQuartile;

	/**
	 * Compares two data object indices based on the value that they have for
	 * some specific numeric attribute. Does not test for missing values (hence
	 * can be used only on indices for both of which values are present).
	 *
	 * @author mboley
	 *
	 */
	private class IndexComparator implements Comparator<Integer> {

		private DefaultOrdinalAttribute attribute;

		public IndexComparator(DefaultOrdinalAttribute attribute) {
			this.attribute = attribute;
		}

		public int compare(Integer i, Integer j) {
			return comparator.compare(attribute.getValue(i),
					attribute.getValue(j));
		}

	}


	/**
	 * @param name
	 * @param description
	 * @param values      list of T-values that can contain null entries to indicate
	 * @param comparator Comparator that is valid for this set of values
	 */
	public DefaultOrdinalAttribute(String name, String description, List<String> values, Comparator<String> comparator) {
		super(name, description, values);
		this.comparator = comparator;

		List<Integer> sortedNonMissingRowIndices = new ArrayList<>(values.size());
		List<String> nonMissingValues = new ArrayList<>(values.size());

		for (int i = 0; i < values.size(); i++) {
			if (!getMissingPositions().contains(i)) {
				sortedNonMissingRowIndices.add(i);
				nonMissingValues.add(values.get(i));
			}
		}
		Collections.sort(sortedNonMissingRowIndices, new IndexComparator(this));

		min = Collections.min(nonMissingValues, comparator);
		max = Collections.max(nonMissingValues, comparator);

		this.sortedIndices = sortedNonMissingRowIndices;


		{
			int lowerHalfPosition = sortedNonMissingRowIndices.size() / 2;
			int medianIdx = (sortedNonMissingRowIndices.size() % 2 == 0) ?
					lowerHalfPosition - 1 : lowerHalfPosition;
			median = values.get(sortedNonMissingRowIndices.get(medianIdx));
		}

		{
			int lowerQuarterPosition = sortedNonMissingRowIndices.size() / 4;

			int lowerQuarterIdx = (sortedNonMissingRowIndices.size() % 4 == 0) ?
					lowerQuarterPosition - 1 : lowerQuarterPosition;
			lowerQuartile = values.get(sortedNonMissingRowIndices.get(lowerQuarterIdx));
		}

		{
			int upperQuarterPosition = 3 * sortedNonMissingRowIndices.size() / 4;
			int upperQuarterIdx = (3 * sortedNonMissingRowIndices.size() % 4 == 0) ?
					upperQuarterPosition - 1 : upperQuarterPosition;
			upperQuartile = values.get(
					sortedNonMissingRowIndices.get(upperQuarterIdx));
		}
	}

	@Override
	public String getMedian() {
		return median;
	}

	@Override
	public String getMax() {
		return max;
	}

	@Override
	public String getMin() {
		return min;
	}

	@Override
	public String getPercentile(double frac) {
		int n = (int) Math.ceil(frac * sortedIndices.size() - 1);
		return getValue(sortedIndices.get(n));
	}

	@Override
	public List<Integer> getSortedNonMissingRowIndices() {
		return sortedIndices;
	}

	@Override
	public String getLowerQuartile() {
		return lowerQuartile;
	}

	@Override
	public String getUpperQuartile() {
		return upperQuartile;	}

	@Override
	public String getMedianOnRows(Set<Integer> rowSet) {
		String median;
		List<String> nonMissingRowValues = new ArrayList<>();

		for (int rowIndex : rowSet) {
			if (isValueMissing(rowIndex)) {
				continue;
			}
			nonMissingRowValues.add(getValue(rowIndex));
		}

		if (nonMissingRowValues.isEmpty()) {
			/* TODO previously here was Double.NaN, what to return with type String? */
			return null;
		}

		Collections.sort(nonMissingRowValues);
		if (nonMissingRowValues.size() % 2 == 0) {
			int idx = nonMissingRowValues.size() / 2 - 1;
			median = (nonMissingRowValues.get(idx));
		} else {
			int idx = nonMissingRowValues.size() / 2;
			median = nonMissingRowValues.get(idx);
		}
		return median;
	}

	@Override
	public Comparator<String> getComparator() {
		return comparator;
	}
}
