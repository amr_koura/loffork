/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.propositions;

import java.util.SortedSet;
import java.util.TreeSet;

import de.unibonn.realkd.data.propositions.Constraint;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.attribute.Attribute;

public class Proposition<T> {

	private final Attribute<T> attribute;

	private final Constraint<T> constraint;

	private final int indexInStore;

	private SortedSet<Integer> supportSet;

	public Proposition(Attribute<T> attribute, Constraint<T> constraint,
			int indexInStore) {
		this.attribute = attribute;
		this.constraint = constraint;
		this.indexInStore = indexInStore;
	}

	@Override
	public String toString() {
		return attribute.getName() + constraint.getSuffixNotationName();
	}

	/**
	 * Checks the proposition against a specific entry in the underlying
	 * DataTable.
	 * 
	 * @param i
	 *            the objectId in the database against which the proposition is
	 *            checked
	 * @return false if any value required for the check is missing; otherwise
	 *         true if and only if proposition holds.
	 */
	public boolean holdsFor(int i) {
		if (attribute.isValueMissing(i)) {
			return false;
		}
		return constraint.holds(attribute.getValue(i));
	}

	public int getIndexInStore() {
		return indexInStore;
	}

	public Attribute<T> getAttribute() {
		return attribute;
	}

	public Constraint<T> getConstraint() {
		return constraint;
	}

	public boolean determines(Proposition<T> anotherProposition) {
		if (this.getAttribute() == anotherProposition.getAttribute()) {
			return this.constraint.implies(anotherProposition.getConstraint());
		}
		return false;
	}

	/**
	 * Warning: uses lazy initialization
	 * 
	 * @return the support set of the proposition
	 */
	public SortedSet<Integer> getSupportSet() {
		if (supportSet == null) {
			supportSet = new TreeSet<>();
			for (int i = 0; i <= attribute.getMaxIndex(); i++) {
				if (holdsFor(i)) {
					supportSet.add(i);
				}
			}
		}
		return supportSet;
	}

	/**
	 * Convenience method.
	 * 
	 * @return getSupportSet().size()
	 */
	public int getSupportCount() {
		return getSupportSet().size();
	}

}
