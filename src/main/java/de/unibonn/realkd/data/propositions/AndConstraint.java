/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.propositions;

import de.unibonn.realkd.data.propositions.Constraint;
import de.unibonn.realkd.data.propositions.DerivedConstraint;

public class AndConstraint<T> implements DerivedConstraint<T> {

	private final Constraint<T> firstEntailedConstraint;

	private final Constraint<T> secondEntailedConstraint;

	public AndConstraint(Constraint<T> firstEntailedConstraint,
			Constraint<T> secondEntailedConstraint) {
		this.firstEntailedConstraint = firstEntailedConstraint;
		this.secondEntailedConstraint = secondEntailedConstraint;
	}

	@Override
	public boolean holds(T value) {
		return (firstEntailedConstraint.holds(value) && secondEntailedConstraint
				.holds(value));
	}

	@Override
	public String getDescription() {
		return firstEntailedConstraint.getDescription() + " AND "
				+ secondEntailedConstraint.getDescription();
	}

	@Override
	public String getSuffixNotationName() {
		return firstEntailedConstraint.getSuffixNotationName() + " AND "
				+ secondEntailedConstraint.getSuffixNotationName();
	}

	@Override
	public boolean implies(Constraint<T> anotherConstraint) {
		// conjunction implies another constraint if it is implied by either
		// entailed constraints
		return (firstEntailedConstraint.implies(anotherConstraint) || secondEntailedConstraint
				.implies(anotherConstraint));
	}

	@Override
	public boolean impliedBy(Constraint<T> anotherConstraint) {
		// another constraint implies conjunction if that constraint implies
		// both entailed constraints
		return (anotherConstraint.implies(firstEntailedConstraint) && anotherConstraint
				.implies(secondEntailedConstraint));
	}

}
