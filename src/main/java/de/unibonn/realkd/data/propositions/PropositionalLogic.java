/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.propositions;

import static de.unibonn.realkd.common.logger.LogChannel.DATA_INITIALIZATION;
import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;
import static de.unibonn.realkd.data.propositions.ConstraintFactory.CONSTRAINT_FACTORY;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

import de.unibonn.realkd.data.propositions.Constraint;
import de.unibonn.realkd.data.propositions.EqualsConstraint;
import de.unibonn.realkd.data.propositions.GreaterThanConstraint;
import de.unibonn.realkd.data.propositions.LargerThanConstraint;
import de.unibonn.realkd.data.propositions.LessThanConstraint;
import de.unibonn.realkd.data.propositions.NamedConstraint;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.common.logger.LogChannel;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.DataArtifact;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;
import de.unibonn.realkd.data.table.attributegroups.OrderedAttributeSequence;

public class PropositionalLogic implements DataArtifact {

	public enum PropositionFactory {

		CATEGORIC_EQUALiTY {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof CategoricalAttribute) {
					for (String category : ((CategoricalAttribute) attribute)
							.getCategories()) {
						store.addProposition(new Proposition<String>(attribute,
								new EqualsConstraint<String>(category),
								store.propositions.size()));
					}
				}
			}
		},

		ABOVE_MEDIAN {
			@Override
			public void constructPropositions(Attribute attribute, PropositionalLogic store) {
				// if (attribute instanceof OrdinalAttribute) {
				if (attribute instanceof OrdinalAttribute && !(attribute instanceof MetricAttribute)) {
					OrdinalAttribute ordinalAtt = (OrdinalAttribute) attribute;
					Comparable median = ordinalAtt.getMedian();
					Constraint<Comparable> constraint = new LargerThanConstraint<Comparable>(
							median,
							((OrdinalAttribute) attribute).getComparator());
					store.addProposition(new Proposition<Comparable>(attribute,
							constraint,	store.propositions.size()));
				}
			}
		},

/*
		VERY_LOW_PERCENTILE {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof OrdinalAttribute) {
					OrdinalAttribute ordinalAttribute = (OrdinalAttribute) attribute;

					Comparable min = ordinalAttribute.getMin();
					Comparable p20 = ordinalAttribute.getPercentile(0.2);

					if (min.equals(p20)) {
						Constraint<Comparable> equalsConstraint = new EqualsConstraint<>(min);

					} else {
						Constraint<Comparable> constraint = new NamedConstraint<>()
					}

					Constraint<Comparable> constraint = new AndConstraint<>()

					double upperValue = defaultMetricAttribute.getMean() - 1.5
							* defaultMetricAttribute.getStandardDeviation();
					Constraint<Double> lessThan = new LessThanConstraint(
							upperValue);
					String name = "very low";
					String description = name + " [inf," + (float) upperValue
							+ ")";
					store.addProposition(new Proposition<Double>(
							defaultMetricAttribute, new NamedConstraint<Double>(
							lessThan, "=" + name, description),
							store.propositions.size()));
				}
			}
		},
*/

		// /**
		// * value in interval from including minimum up to excluding
		// * 0.4-percentile
		// */
		// LOW_PERCENTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// double lowerValue = numericAttribute.getMin();
		// Constraint<Double> greaterThanOrEqual = new
		// GreaterThanOrEqualConstraint(
		// lowerValue);
		// double upperValue = numericAttribute.getPercentile(0.4);
		// Constraint<Double> lessThan = new LessThanConstraint(
		// upperValue);
		// Constraint<Double> andConstraint = new AndConstraint<Double>(
		// greaterThanOrEqual, lessThan);
		// String description = "low [" + (float) lowerValue + ","
		// + (float) upperValue + ")";
		// store.addProposition(new Proposition<Double>(
		// numericAttribute, new NamedConstraint<Double>(
		// andConstraint, "=low", description),
		// store.propositions.size()));
		// }
		// }
		// },
		//
		// /**
		// * value in interval from including mininmum up to excluding 0.2
		// * percentile
		// */
		// VERYLOW_PERCENTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// double lowerValue = numericAttribute.getMin();
		// Constraint<Double> greaterThanOrEqual = new
		// GreaterThanOrEqualConstraint(
		// lowerValue);
		// double upperValue = numericAttribute.getPercentile(0.2);
		// Constraint<Double> lessThan = new LessThanConstraint(
		// upperValue);
		// Constraint<Double> andConstraint = new AndConstraint<Double>(
		// greaterThanOrEqual, lessThan);
		// String description = "very low [" + (float) lowerValue
		// + "," + (float) upperValue + ")";
		// store.addProposition(new Proposition<Double>(
		// numericAttribute, new NamedConstraint<Double>(
		// andConstraint, "=very low", description),
		// store.propositions.size()));
		// }
		// }
		// },
		//
		// /**
		// * value in interval from including 0.2-percentile up to excluding
		// * 0.4-percentile
		// */
		// BELOW_40_DOWNTO_20_PERCENTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// double lowerValue = numericAttribute.getPercentile(0.2);
		// Constraint<Double> greaterThanOrEqual = new
		// GreaterThanOrEqualConstraint(
		// lowerValue);
		// double upperValue = numericAttribute.getPercentile(0.4);
		// Constraint<Double> lessThan = new LessThanConstraint(
		// upperValue);
		// Constraint<Double> andConstraint = new AndConstraint<Double>(
		// greaterThanOrEqual, lessThan);
		// String name = "decreased";
		// String description = name + " [" + (float) lowerValue + ","
		// + (float) upperValue + ")";
		// store.addProposition(new Proposition<Double>(
		// numericAttribute, new NamedConstraint<Double>(
		// andConstraint, "=" + name, description),
		// store.propositions.size()));
		// }
		// }
		// },

		VERY_LOW_BY_STDEV {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof MetricAttribute) {
					MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;
					double upperValue = defaultMetricAttribute.getMean() - 1.5
							* defaultMetricAttribute.getStandardDeviation();
					Constraint<Double> lessThan = new LessThanConstraint(
							upperValue);
					String name = "very low";
					String description = name + " [inf," + (float) upperValue
							+ ")";
					store.addProposition(new Proposition<Double>(
							defaultMetricAttribute, new NamedConstraint<Double>(
									lessThan, "=" + name, description),
							store.propositions.size()));
				}
			}
		},

		LOW_BY_STDEV {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof MetricAttribute) {
					MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;
					double upperValue = defaultMetricAttribute.getMean() - 0.5
							* defaultMetricAttribute.getStandardDeviation();
					Constraint<Double> lessThan = new LessThanConstraint(
							upperValue);
					String name = "low";
					String description = name + " [inf," + (float) upperValue
							+ ")";
					store.addProposition(new Proposition<Double>(
							defaultMetricAttribute, new NamedConstraint<Double>(
									lessThan, "=" + name, description),
							store.propositions.size()));
				}
			}
		},

		// NORMAL_PERCENTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// double lowerBorder = numericAttribute
		// .getPercentile(0.4);
		// double upperBorder = numericAttribute
		// .getPercentile(0.6);
		// store.addProposition(new Proposition(numericAttribute,
		// new NamedIntervalConstraint(lowerBorder, upperBorder, "normal"),
		// store.propositions.size()));
		// }
		// }
		// },

		NORMAL_BY_STDEV {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof MetricAttribute) {
					MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;
					double lowerBorder = defaultMetricAttribute.getMean() - 0.5
							* defaultMetricAttribute.getStandardDeviation();
					double upperBorder = defaultMetricAttribute.getMean() + 0.5
							* defaultMetricAttribute.getStandardDeviation();

					Constraint<Double> closedIntervalConstraint = CONSTRAINT_FACTORY
							.constructClosedIntervalConstraint(lowerBorder,
									upperBorder);
					store.addProposition(new Proposition<Double>(
							defaultMetricAttribute, new NamedConstraint<>(
									closedIntervalConstraint, "=normal",
									"normal [" + lowerBorder + ","
											+ upperBorder + "]"),
							store.propositions.size()));

					// store.addProposition(new Proposition(numericAttribute,
					// new NamedIntervalConstraint(lowerBorder,
					// upperBorder, "normal"), store.propositions
					// .size()));
				}
			}
		},

		// ABOVE_60_UPTO_80_PERCENTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// double lowerValue = numericAttribute.getPercentile(0.6);
		// Constraint<Double> greaterThan = new GreaterThanConstraint(
		// lowerValue);
		// double upperValue = numericAttribute.getPercentile(0.8);
		// Constraint<Double> lessThan = new LessThanOrEqualConstraint(
		// upperValue);
		// Constraint<Double> andConstraint = new AndConstraint<Double>(
		// greaterThan, lessThan);
		// String name = "increased";
		// String description = name + " (" + (float) lowerValue + ","
		// + (float) upperValue + "]";
		// store.addProposition(new Proposition<Double>(
		// numericAttribute, new NamedConstraint<Double>(
		// andConstraint, "=" + name, description),
		// store.propositions.size()));
		// }
		// }
		// },

		// ABOVE_80_UPTO_MAX_PERCENTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// double lowerValue = numericAttribute.getPercentile(0.8);
		// Constraint<Double> greaterThan = new GreaterThanConstraint(
		// lowerValue);
		// double upperValue = numericAttribute.getPercentile(1.0);
		// Constraint<Double> lessThanOrEqual = new LessThanOrEqualConstraint(
		// upperValue);
		// Constraint<Double> andConstraint = new AndConstraint<Double>(
		// greaterThan, lessThanOrEqual);
		// // new Proposition(numericAttribute,
		// // new NamedIntervalConstraint(numericAttribute
		// // .getPercentile(0.4), numericAttribute.getPercentile(0.6),
		// // "normal")
		// String name = "very high";
		// String description = name + " (" + (float) lowerValue + ","
		// + (float) upperValue + "]";
		// store.addProposition(new Proposition<Double>(
		// numericAttribute, new NamedConstraint(
		// andConstraint, "=" + name, description),
		// store.propositions.size()));
		// }
		// }
		// },

		HIGH_BY_STDEV {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof MetricAttribute) {
					MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;
					double lowerValue = defaultMetricAttribute.getMean() + 0.5
							* defaultMetricAttribute.getStandardDeviation();
					Constraint<Double> greaterThan = new GreaterThanConstraint(
							lowerValue);
					String name = "high";
					String description = name + " (" + (float) lowerValue
							+ ",inf]";
					store.addProposition(new Proposition<Double>(
							defaultMetricAttribute, new NamedConstraint<Double>(
									greaterThan, "=" + name, description),
							store.propositions.size()));
				}
			}
		},

		VERY_HIGH_BY_STDEV {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof MetricAttribute) {
					MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;
					double lowerValue = defaultMetricAttribute.getMean() + 1.5
							* defaultMetricAttribute.getStandardDeviation();
					Constraint<Double> greaterThan = new GreaterThanConstraint(
							lowerValue);
					String name = "very high";
					String description = name + " (" + (float) lowerValue
							+ ",inf]";
					store.addProposition(new Proposition<Double>(
							defaultMetricAttribute, new NamedConstraint<Double>(
									greaterThan, "=" + name, description),
							store.propositions.size()));
				}
			}
		},

		// ABOVE_60_UPTO_MAX_PERCENTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// double lowerValue = numericAttribute.getPercentile(0.6);
		// Constraint<Double> greaterThan = new GreaterThanConstraint(
		// lowerValue);
		// double upperValue = numericAttribute.getPercentile(1.0);
		// Constraint<Double> lessThanOrEqual = new LessThanOrEqualConstraint(
		// upperValue);
		// Constraint<Double> andConstraint = new AndConstraint<Double>(
		// greaterThan, lessThanOrEqual);
		// String name = "high";
		// String description = name + " (" + (float) lowerValue + ","
		// + (float) upperValue + "]";
		// store.addProposition(new Proposition<Double>(
		// numericAttribute, new NamedConstraint<Double>(
		// andConstraint, "=" + name, description),
		// store.propositions.size()));
		// }
		// }
		// },

		// ABOVE_MEDIAN {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// store.addProposition(new Proposition(numericAttribute,
		// new NamedIntervalConstraint(numericAttribute
		// .getMedian(), numericAttribute.getMax(),
		// "upper half"), store.propositions.size()));
		// }
		// }
		// },
		//
		// BELOW_MEDIAN {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// store.addProposition(new Proposition(numericAttribute,
		// new NamedIntervalConstraint(numericAttribute
		// .getMin(), numericAttribute.getMedian(),
		// "lower half"), store.propositions.size()));
		// }
		// }
		// },
		//
		// FIRST_QUARTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// store.addProposition(new Proposition(numericAttribute,
		// new NamedIntervalConstraint(numericAttribute
		// .getMin(), numericAttribute
		// .getLowerQuartile(), "very low"),
		// store.propositions.size()));
		// }
		// }
		// },
		//
		// SECOND_QUARTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// store.addProposition(new Proposition(numericAttribute,
		// new NamedIntervalConstraint(numericAttribute
		// .getLowerQuartile(), numericAttribute
		// .getMedian(), "low"), store.propositions
		// .size()));
		// }
		// }
		// },
		//
		// THIRD_QUARTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// store.addProposition(new Proposition(numericAttribute,
		// new NamedIntervalConstraint(numericAttribute
		// .getMedian(), numericAttribute
		// .getUpperQuartile(), "high"),
		// store.propositions.size()));
		// }
		// }
		// },
		//
		// FOURTH_QUARTILE {
		// @Override
		// public void constructPropositions(Attribute attribute,
		// PropositionStore store) {
		// if (attribute.isNumeric()) {
		// NumericAttribute numericAttribute = (NumericAttribute) attribute;
		// store.addProposition(new Proposition(numericAttribute,
		// new NamedIntervalConstraint(numericAttribute
		// .getUpperQuartile(), numericAttribute
		// .getMax(), "very high"), store.propositions
		// .size()));
		// }
		// }
		// },

		POSITIVE_AND_NEGATIVE {
			@Override
			public void constructPropositions(Attribute attribute,
					PropositionalLogic store) {
				if (attribute instanceof MetricAttribute) {
					MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;
					if (defaultMetricAttribute.getMin() < 0) {
						store.addProposition(new Proposition<Double>(
								defaultMetricAttribute, new NamedConstraint<Double>(
										new LessThanConstraint(0), "=negative",
										"negative [-inf,0)"),
								store.propositions.size()));
						store.addProposition(new Proposition<Double>(
								defaultMetricAttribute, new NamedConstraint<Double>(
										new GreaterThanConstraint(0),
										"=positive", "positive (0,inf]"),
								store.propositions.size()));
					}
				}
			}
		};

		public abstract void constructPropositions(Attribute attribute,
				PropositionalLogic store);

	}

	public enum PropositionFromGroupFactory {

		CONSECUTIVE_CHANGE_ATTRIBUTES {
			@Override
			public void constructPropositions(AttributeGroup attributeGroup,
					PropositionalLogic store) {

				if (attributeGroup instanceof OrderedAttributeSequence) {
					OrderedAttributeSequence sequenceGroup = (OrderedAttributeSequence) attributeGroup;
					for (int i = 0; i < sequenceGroup.getElements().size() - 1; i++) {
						Attribute attribute = sequenceGroup.getChangeAttribute(
								i, i + 1);
						for (PropositionFactory attributeFactory : PropositionFactory
								.values()) {
							attributeFactory.constructPropositions(
									(Attribute) attribute, store);
						}
					}
				}

			}
		};

		public abstract void constructPropositions(
				AttributeGroup attributeGroup, PropositionalLogic store);

	}

	private DataTable dataTable;

	private List<Proposition> propositions;

	private void addProposition(Proposition proposition) {
		DATA_INITIALIZATION.log("Add proposition " + proposition,
				LogMessageType.INTRA_COMPONENT_MESSAGE);
		for (Proposition alreadyPresentProposition : propositions) {
			if (proposition.determines(alreadyPresentProposition)) {
				DATA_INITIALIZATION.log("WARNING: " + proposition
						+ " determines already present proposition "
						+ alreadyPresentProposition,
						LogMessageType.INTRA_COMPONENT_MESSAGE);
			}
			if (alreadyPresentProposition.determines(proposition)) {
				DATA_INITIALIZATION.log("WARNING: " + proposition
						+ " is determined by already present proposition "
						+ alreadyPresentProposition,
						LogMessageType.INTRA_COMPONENT_MESSAGE);
			}

		}
		this.propositions.add(proposition);
	}

	public PropositionalLogic(DataTable dataTable) {
		this.dataTable = dataTable;
		this.propositions = new ArrayList<>();
		this.init();
	}
	
	public SortedSet<Integer> getObjectIds() {
		return dataTable.getObjectIds();
	}

	public List<Proposition> getPropositions() {
		return propositions;
	}

	public List<Proposition> getPropositionsAbout(Attribute attribute) {
		List<Proposition> result = new ArrayList<>();
		for (Proposition prop : this.propositions) {
			if (prop.getAttribute() == attribute) {
				result.add(prop);
			}
		}
		return result;
	}

	private void init() {
		DATA_INITIALIZATION.log("Initialize proposition store",
				LogMessageType.INTER_COMPONENT_MESSAGE);
		for (Attribute attribute : dataTable.getAttributes()) {
			for (PropositionFactory propFactory : PropositionFactory.values()) {
				propFactory.constructPropositions(attribute, this);
			}
		}

		for (AttributeGroup attributeGroup : dataTable
				.getAttributeGroupsStore().getAttributeGroups()) {
			for (PropositionFromGroupFactory propFactory : PropositionFromGroupFactory
					.values()) {
				propFactory.constructPropositions(attributeGroup, this);
			}
		}

		DEFAULT.log(LogChannel.DATA_INITIALIZATION.toString(),
				"Done initializing proposition store (" + propositions.size()
						+ " propositions added)",
				LogMessageType.INTER_COMPONENT_MESSAGE);
	}

	@Override
	public int getSize() {
		return dataTable.getSize();
	}

	public DataTable getDatatable() {
		return dataTable;
	}

	public String getObjectName(int objectIndex) {
		return dataTable.getObjectName(objectIndex);
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public String getName() {
		return "Statements about "+dataTable.getName();
	}

	@Override
	public String getDescription() {
		return "";
	}
}
