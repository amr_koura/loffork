/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.propositions;

import de.unibonn.realkd.data.propositions.AndConstraint;
import de.unibonn.realkd.data.propositions.Constraint;
import de.unibonn.realkd.data.propositions.ConstraintFactory;
import de.unibonn.realkd.data.propositions.EqualsConstraint;
import de.unibonn.realkd.data.propositions.GreaterThanConstraint;
import de.unibonn.realkd.data.propositions.LessThanConstraint;
import de.unibonn.realkd.data.propositions.OrConstraint;

public class ConstraintFactory {

	public static ConstraintFactory CONSTRAINT_FACTORY = new ConstraintFactory();

	private ConstraintFactory() {
		;
	}

	public Constraint<Double> constructLessOrEqualsConstraint(double threshold) {
		Constraint<Double> equalsConstraint = new EqualsConstraint<Double>(
				threshold);
		Constraint<Double> lessThanConstraint = new LessThanConstraint(
				threshold);
		return new OrConstraint<>(lessThanConstraint, equalsConstraint);
	}

	public Constraint<Double> constructGreaterOrEqualsConstraint(
			double threshold) {
		Constraint<Double> equalsConstraint = new EqualsConstraint<Double>(
				threshold);
		Constraint<Double> greaterThanConstraint = new GreaterThanConstraint(
				threshold);
		return new OrConstraint<>(greaterThanConstraint, equalsConstraint);
	}

	public Constraint<Double> constructClosedIntervalConstraint(
			double lowerThreshold, double upperThreshold) {
		Constraint<Double> lowerBorder = constructGreaterOrEqualsConstraint(lowerThreshold);
		Constraint<Double> upperBorder = constructLessOrEqualsConstraint(upperThreshold);
		return new AndConstraint<>(lowerBorder, upperBorder);
	}

}
