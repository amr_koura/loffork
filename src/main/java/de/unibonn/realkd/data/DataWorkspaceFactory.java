/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.unibonn.realkd.data;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.data.DataArtifact;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.DataWorkspaceFactory;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

public class DataWorkspaceFactory {

	private class DefaultDataWorkspace implements DataWorkspace {

		private final List<DataArtifact> dataArtifacts = new ArrayList<>();

		@Override
		public void add(DataArtifact dataArtifact) {
			dataArtifacts.add(dataArtifact);
		}

		@Override
		public List<Object> getAllObjectsOfType(Class<?> clazz) {
			List<Object> result = new ArrayList<Object>();
			for (Object object : dataArtifacts) {
				if (clazz.isInstance(object)) {
					result.add(object);
				}
			}
			return result;
		}

		@Override
		public List<DataTable> getAllDatatables() {
			List<DataTable> result = new ArrayList<>();
			for (Object dataTable : getAllObjectsOfType(DataTable.class)) {
				result.add((DataTable) dataTable);
			}
			return result;
		}

		@Override
		public List<PropositionalLogic> getAllPropositionalLogics() {
			List<PropositionalLogic> result = new ArrayList<>();
			for (Object dataTable : getAllObjectsOfType(PropositionalLogic.class)) {
				result.add((PropositionalLogic) dataTable);
			}
			return result;
		}

	}

	public static DataWorkspaceFactory INSTANCE = new DataWorkspaceFactory();

	private DataWorkspaceFactory() {
		;
	}

	public DataWorkspace createDataWorkspace() {
		return new DefaultDataWorkspace();
	}

}
