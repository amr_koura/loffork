package de.unibonn.realkd.patterns;

import de.unibonn.realkd.data.propositions.Proposition;

/**
 * @author bjacobs
 */

public class InterestingnessFunction {
    public static double FREQUENCY(int localSize, int globalSize) {
	return localSize / (double) globalSize;
    }

    public static double LIFT(LogicallyDescribedLocalPattern pattern, double product, double denominator) {
	return (pattern.getFrequency() - product) / denominator;
    }

/*
    public static double NEGATIVE_LIFT() {
	return 0.0;
    }


    public static double ABSOLUTE_LIFT() {
	return 0.0;
    }
*/


    public static double PRODUCT_OF_INDIVIDUAL_FREQUENCIES(LogicallyDescribedLocalPattern pattern) {
	double product = 1.;
	double globalSize = pattern.getPropositionalLogic().getSize();

	for (Proposition literal : pattern.getDescription().getElements()) {
	    product *= literal.getSupportSet().size() / globalSize;
	}

	return product;
    }


    public static double DEVIATION_OF_FREQUENCY() {
	return 0.0;
    }


    public static double MANHATTEN_MEAN_DEVIATION() {
	return 0.0;
    }


    public static double TOTAL_VARIATION_DISTANCE() {
	return 0.0;
    }


    public static double ANGLE_COSINE_DISTANCE() {
	return 0.0;
    }


    public static double OUTLIER_SCORE() {
	return 0.0;
    }
}

