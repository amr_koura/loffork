/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.unibonn.realkd.data.DataArtifact;
import de.unibonn.realkd.data.table.attribute.Attribute;

public abstract class AbstractPattern implements Pattern {

	private final List<InterestingnessMeasure> measuresInOrder;

	private final Map<InterestingnessMeasure, Double> measureToValues;

	private final DataArtifact dataArtifact;

	public AbstractPattern(DataArtifact dataArtifact) {
		this.dataArtifact = dataArtifact;
		this.measuresInOrder = new ArrayList<>();
		this.measureToValues = new HashMap<>();
	}

	/**
	 * To be called by subclasses during pattern construction in order to bind
	 * measurements. <br>
	 * 
	 * WARNING: should only be called during constructor execution in order to
	 * maintain true immutability of patterns <br>
	 * 
	 * getMeasures() returns measures in order of binding
	 * 
	 */
	protected void addMeasurement(InterestingnessMeasure measure, Double value) {
		this.measuresInOrder.add(measure);
		this.measureToValues.put(measure, value);
	}

	@Override
	public List<InterestingnessMeasure> getMeasures() {
		return measuresInOrder;
	}

	@Override
	public double getValue(InterestingnessMeasure measure) {
		return measureToValues.get(measure);
	}

	@Override
	public boolean hasMeasure(InterestingnessMeasure measure) {
		return measureToValues.containsKey(measure);
	}
	
	public final DataArtifact getDataArtifact() {
		return this.dataArtifact;
	}

	@Override
	public final String toString() {
		StringBuilder resultBuilder = new StringBuilder();
		resultBuilder.append("{\n");
		appendClassInformation(resultBuilder);
		resultBuilder.append(getAdditionsForStringRepresentation());
		resultBuilder.append("attributes: [\n");
		@SuppressWarnings("rawtypes")
		Iterator<Attribute> attributeIterator = getAttributes().iterator();
		while (attributeIterator.hasNext()) {
			Attribute<?> attribute = attributeIterator.next();
			resultBuilder.append("\t"+attribute.getName());
			resultBuilder.append(attributeIterator.hasNext() ? ",\n" : "\n");
		}
		resultBuilder.append("],\n");
		resultBuilder.append("measures: [\n");
		Iterator<InterestingnessMeasure> measureIterator = getMeasures()
				.iterator();
		while (measureIterator.hasNext()) {
			InterestingnessMeasure measure = measureIterator.next();
			resultBuilder.append("\t"+measure.getName() + ": " + getValue(measure));
			resultBuilder.append(measureIterator.hasNext() ? ",\n" : "\n");
		}
		resultBuilder.append("]\n");
		resultBuilder.append("}\n");
		return resultBuilder.toString();
	}

	private void appendClassInformation(StringBuilder resultBuilder) {
		resultBuilder
				.append("class: " + this.getClass().getSimpleName() + ",\n");
	}

	/**
	 * Hook that can be overriden by sub-classes in order to add information to
	 * the string representation of pattern as computed by {@link #toString} .
	 */
	protected String getAdditionsForStringRepresentation() {
		return "";
	}

}
