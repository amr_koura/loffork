/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * Aggregates a set of propositions that are stored in canonical order given by
 * their index in a propositional logic.
 * 
 * @author mboley
 * 
 */
public class Description {

	private static class CanonicalOrder implements Comparator<Proposition> {

		@Override
		public int compare(Proposition o1, Proposition o2) {
			return Integer.compare(o1.getIndexInStore(), o2.getIndexInStore());
		}

	}

	public Comparator<Proposition> PROPOSITION_ORDER = new CanonicalOrder();

	private final List<Proposition> elements;

	private PropositionalLogic propositionalLogic;

	public Description(PropositionalLogic propositionalLogic,
			Collection<Proposition> elements) {
		this.propositionalLogic = propositionalLogic;
		List<Proposition> orderedElements = new ArrayList<>(elements);
		Collections.sort(orderedElements, PROPOSITION_ORDER);
		this.elements = Collections.unmodifiableList(orderedElements);

		supportSet = new TreeSet<>();
		calculateSupport();
	}

	private Description(Description oldDescription, Proposition augmentation) {
		this.propositionalLogic = oldDescription.getPropositionalLogic();

		elements = new ArrayList<>(oldDescription.getElements());
		elements.add(augmentation);
		Collections.sort(elements, PROPOSITION_ORDER);

		supportSet = new TreeSet<>();
		supportSet.addAll(oldDescription.getSupportSet());
		supportSet.retainAll(augmentation.getSupportSet());
	}

	public PropositionalLogic getPropositionalLogic() {
		return propositionalLogic;
	}

	/**
	 * 
	 * @return number of contained propositions
	 */
	public int size() {
		return this.elements.size();
	}

	public boolean isEmpty() {
		return this.elements.isEmpty();
	}

	public Proposition<?> getElement(int i) {
		return this.elements.get(i);
	}

	public List<String> getElementsAsStringList() {
		List<String> descriptionList = new ArrayList<>();
		for (Proposition<?> proposition : getElements()) {
			descriptionList.add(proposition.toString());
		}

		return descriptionList;
	}

	public Description getSpecialization(Proposition<?> augmentation) {
		ArrayList<Proposition> newElements = new ArrayList<>(this.getElements());
		newElements.add(augmentation);

		return new Description(this, augmentation);
	}

	public Description getGeneralization(Proposition<?> reductionElement) {
		if (!getElements().contains(reductionElement)) {
			throw new IllegalArgumentException(
					"reduction element not part of description");
		}
		List<Proposition> newDescription = new ArrayList<>(getElements());
		newDescription.remove(reductionElement);
		return new Description(getPropositionalLogic(), newDescription);

	}

	public boolean containsAttribute(Attribute attribute) {
		for (Proposition proposition : getElements()) {
			if (proposition.getAttribute() == attribute) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @return the attributes that this description relates to
	 */
	public List<Attribute> getAttributes() {
		List<Attribute> result = new ArrayList<Attribute>();
		for (Proposition proposition : getElements()) {
			result.add(proposition.getAttribute());
		}
		return result;
	}

	public List<Proposition> getElements() {
		return this.elements;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Description))
			return false;

		Description other = (Description) o;

		if (other.getElements().size() != this.getElements().size()) {
			return false;
		}

		for (int i = 0; i < this.getElements().size(); i++) {
			if (this.getElements().get(i) != other.getElements().get(i)) {
				return false;
			}
		}

		return true;
	}

	private final SortedSet<Integer> supportSet;

	/**
	 * computes the support set of the pattern by forming the intersection of
	 * the support sets of all propositions. Note that in the special case of an
	 * empty description, the support set has to be the complete set of objects
	 */
	private void calculateSupport() {
		if (getElements().isEmpty()) {
			this.supportSet.addAll(propositionalLogic.getObjectIds());
		} else {
			supportSet.addAll(getElements().get(0).getSupportSet());
		}
		for (int i = 1; i < getElements().size(); i++) {
			supportSet.retainAll(getElements().get(i).getSupportSet());
		}
	}

	@Override
	public String toString() {
		return elements.toString();
	}

	public SortedSet<Integer> getSupportSet() {
		return supportSet;
	}
}
