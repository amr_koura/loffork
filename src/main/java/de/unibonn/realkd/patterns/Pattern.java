/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import java.util.List;
import java.util.SortedSet;

import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.data.DataArtifact;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;

public interface Pattern {

	public abstract boolean equals(Object o);

	public abstract int hashCode();

	public abstract String toString();
	
	public abstract DataArtifact getDataArtifact();

	public abstract SortedSet<Integer> getSupportSet();

	public abstract List<Attribute> getAttributes();

	public abstract List<InterestingnessMeasure> getMeasures();

	public abstract double getValue(InterestingnessMeasure measure);

	public abstract boolean hasMeasure(InterestingnessMeasure measure);

	/**
	 * Abstract 0-element that can be used for providing absolute positive and
	 * negative examples within the preference learning framework. This is done
	 * by providing: positivePattern is preferred over ZERO_PATTERN (or the
	 * other way around for negative examples).
	 * 
	 * Inner products with this pattern must be 0 in all implementations of
	 * InnerProductSpace. Does not support any methods other than toString and
	 * equals.
	 * 
	 * This is not to be confused with the empty pattern!
	 */
	public static final Pattern ZERO_PATTERN = new Pattern() {

		@Override
		public SortedSet<Integer> getSupportSet() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Pattern clone() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String toString() {
			return "ZERO";
		}

		@Override
		public boolean equals(Object o) {
			return o == this;
		}

		@Override
		public List<Attribute> getAttributes() {
			throw new UnsupportedOperationException();
		}

		@Override
		public List<InterestingnessMeasure> getMeasures() {
			throw new UnsupportedOperationException();
		}

		@Override
		public double getValue(InterestingnessMeasure measure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean hasMeasure(InterestingnessMeasure measure) {
			return false;
		}

		@Override
		public PropositionalLogic getDataArtifact() {
			throw new UnsupportedOperationException();
		}

	};

}