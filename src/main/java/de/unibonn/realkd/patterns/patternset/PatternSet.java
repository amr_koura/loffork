/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.patternset;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.AbstractPattern;
import de.unibonn.realkd.patterns.Pattern;

/**
 * PatternSet is a pattern that aggregates a set of patterns. Its support
 * set is the union of the support sets of the contained patterns.
 *
 * @author bkang
 */
public class PatternSet extends AbstractPattern {

    private final Set<Pattern> patterns;

    private final SortedSet<Integer> supportSet;

    public PatternSet(PropositionalLogic propLogic, Set<Pattern> patterns) {
        super(propLogic);
        this.patterns = patterns;
        this.supportSet = new TreeSet<>();
        calculateSupport();
    }

    public Set<Pattern> getPatterns() {
        return patterns;
    }

    @Override
    public SortedSet<Integer> getSupportSet() {
        return supportSet;
    }

    @Override
    public List<Attribute> getAttributes() {
        return new ArrayList<>();
    }

    @Override
    protected String getAdditionsForStringRepresentation() {
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append(super.getAdditionsForStringRepresentation());
        resultBuilder.append("elements: [\n");
        for (Pattern pattern : patterns) {
            resultBuilder.append(pattern.toString());
        }
        resultBuilder.append("]\n");
        return resultBuilder.toString();
    }

    private void calculateSupport() {
        if (patterns.isEmpty()) {
            this.supportSet.addAll(getDataArtifact().getObjectIds());
        } else {
            for (Pattern pattern : patterns) {
                this.supportSet.addAll(pattern.getSupportSet());
            }
        }

    }
}
