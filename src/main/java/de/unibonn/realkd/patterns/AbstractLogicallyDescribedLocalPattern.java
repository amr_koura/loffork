/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import java.util.HashSet;
import java.util.SortedSet;

import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * @author ptokmakov
 */
public abstract class AbstractLogicallyDescribedLocalPattern extends
		AbstractPattern implements LogicallyDescribedLocalPattern {

	private final Description description;

	public AbstractLogicallyDescribedLocalPattern(
			PropositionalLogic propositionalLogic, Description description) {
		super(propositionalLogic);
		this.description = description;
		super.addMeasurement(InterestingnessMeasure.FREQUENCY,
				InterestingnessMeasure.FREQUENCY.getValue(this));
		// .getSupportSet().size() / (double) propositionalLogic.getSize());
	}

	public final PropositionalLogic getPropositionalLogic() {
		return (PropositionalLogic) getDataArtifact();
	}

	@Override
	public double getFrequency() {
		return getValue(InterestingnessMeasure.FREQUENCY);
	}

	@Override
	public Description getDescription() {
		return description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass()
				|| o == Pattern.ZERO_PATTERN)
			return false;

		LogicallyDescribedLocalPattern that = (LogicallyDescribedLocalPattern) o;

		return this.description.equals(that.getDescription());
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = description != null ? new HashSet<>(description.getElements())
				.hashCode() : 0;
		// temp = featureDescriptor.getFrequency() != +0.0d ?
		// Double.doubleToLongBits(featureDescriptor.getFrequency()) : 0L;
		temp = getFrequency() != +0.0d ? Double
				.doubleToLongBits(getFrequency()) : 0L;
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		// temp = featureDescriptor.getDeviation() != +0.0d ?
		// Double.doubleToLongBits(featureDescriptor.getDeviation()) : 0L;
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		// result = 31 * result + targetIndex;
		return result;
	}

	// @Override
	// public String toString() {
	// return description.toString();
	// }

	@Override
	protected String getAdditionsForStringRepresentation() {
		return "description: " + description.toString() + ",\n";
	}

	@Override
	public SortedSet<Integer> getSupportSet() {
		return description.getSupportSet();
	}

}
