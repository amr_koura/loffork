/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.outlier;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.AbstractPattern;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Pattern indicating that a specific set of rows in the dataTable shows an
 * anormal behavior with respect to a specific set of attributes.
 * 
 * @author mboley
 * 
 */
public class Outlier extends AbstractPattern implements Pattern {

	// private final DataTable dataTable;
	private final SortedSet<Integer> rows;
	private final Set<Attribute> attributes;
	private final DataTable dataTable;

	public Outlier(DataTable dataTable, SortedSet<Integer> rows,
			Set<Attribute> attributes, double score, double frequency) {
		super(dataTable);
		this.dataTable = dataTable;
		this.rows = rows;
		this.attributes = attributes;
		this.addMeasurement(InterestingnessMeasure.OUTLIER_SCORE, score);
		this.addMeasurement(InterestingnessMeasure.FREQUENCY, frequency);
	}

	// @Override
	// public double getFrequency() {
	// return this.rows.size() / (double) this.getDataTable().getSize();
	// }

	// @Override
	// public DataTable getDataTable() {
	// return dataTable;
	// }

	@Override
	public SortedSet<Integer> getSupportSet() {
		return this.rows;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Attribute> getAttributes() {
		return new ArrayList<Attribute>(attributes);
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Outlier) {
			return (this.attributes.containsAll(((Outlier) other).attributes) && this.rows
					.containsAll(((Outlier) other).rows));
		}
		return false;
	}

	public DataTable getDatatable() {
		return this.dataTable;
	}

}
