/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import de.unibonn.realkd.data.propositions.Proposition;

public enum InterestingnessMeasure {

	FREQUENCY("frequency") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			return (pattern instanceof LogicallyDescribedLocalPattern);
		}

		@Override
		public double getValue(Pattern pattern) {
			int localSize = ((LogicallyDescribedLocalPattern) pattern)
					.getDescription().getSupportSet().size();
			int globalSize = pattern.getDataArtifact().getSize();
			return localSize / (double) globalSize;
		}
	},

	LIFT("lift") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			return (pattern instanceof LogicallyDescribedLocalPattern);
		}

		@Override
		public double getValue(Pattern pattern) {
			LogicallyDescribedLocalPattern castPattern = (LogicallyDescribedLocalPattern) pattern;

			double product;
			if (pattern.hasMeasure(PRODUCT_OF_INDIVIDUAL_FREQUENCIES)) {
				product = pattern.getValue(PRODUCT_OF_INDIVIDUAL_FREQUENCIES);
			} else {
				product = PRODUCT_OF_INDIVIDUAL_FREQUENCIES
						.getValue(castPattern);
			}

			double denominator = Math.pow(2, castPattern.getDescription()
					.size() - 2.);

			double frequency = castPattern.getFrequency();

			return (frequency - product) / denominator;
		}
	},

	NEGATIVE_LIFT("negative lift") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return 0;
		}
	},

	ABSOLUTE_LIFT("absolute lift") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return 0;
		}
	},

	PRODUCT_OF_INDIVIDUAL_FREQUENCIES("expected frequency") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			return (pattern instanceof LogicallyDescribedLocalPattern);
		}

		@Override
		public double getValue(Pattern pattern) {
			LogicallyDescribedLocalPattern castPattern = (LogicallyDescribedLocalPattern) pattern;

			double product = 1.0;
			int propLogicSize = castPattern.getPropositionalLogic().getSize();

			for (Proposition literal : castPattern.getDescription()
					.getElements()) {
				product *= literal.getSupportSet().size()
						/ (double) propLogicSize;
			}

			return product;
		}
	},

	DEVIATION_OF_FREQUENCY("deviation of frequency") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			return (pattern instanceof LogicallyDescribedLocalPattern);
		}

		@Override
		public double getValue(Pattern pattern) {
			LogicallyDescribedLocalPattern castPattern = (LogicallyDescribedLocalPattern) pattern;

			double product;
			if (pattern.hasMeasure(PRODUCT_OF_INDIVIDUAL_FREQUENCIES)) {
				product = pattern.getValue(PRODUCT_OF_INDIVIDUAL_FREQUENCIES);
			} else {
				product = PRODUCT_OF_INDIVIDUAL_FREQUENCIES
						.getValue(castPattern);
			}

			double frequency = castPattern.getFrequency();

			return frequency - product;
		}
	},

	MANHATTAN_MEAN_DEVIATION("Manhattan mean deviation") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return 0;
		}
	},

	POSITIVE_MEAN_DIFFERENCE("positive mean difference") {

		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();

		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
		}

	},

	TOTAL_VARIATION_DISTANCE("total variation distance") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return 0;
		}
	},

	ANGLE_COSINE_DISTANCE("angle cosine distance") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return 0;
		}
	},

	OUTLIER_SCORE("outlier score") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
			// return 0;
		}
	},
	GLOBAL_MEAN("global mean") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();

			// // TODO Auto-generated method stub
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();

			// // TODO Auto-generated method stub
			// return 0;
		}
	},
	LOCAL_MEAN("local mean") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();

			// // TODO Auto-generated method stub
			// return false;
		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();
		}
	},
	NEGATIVE_MEAN_DIFFERENCE("negative mean difference") {
		@Override
		public boolean isApplicable(Pattern pattern) {
			throw new UnsupportedOperationException();

		}

		@Override
		public double getValue(Pattern pattern) {
			throw new UnsupportedOperationException();

		}
	};

	private final String _name;

	private InterestingnessMeasure(String name) {
		this._name = name;
	}

	public String getName() {
		return _name;
	}

	public abstract boolean isApplicable(Pattern pattern);

	public abstract double getValue(Pattern pattern);
}
