/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * @author bjacobs
 */

public class TargetAttributesBuilder {

	private final List<Integer> indices;

	public TargetAttributesBuilder(DataTable table,
			Collection<Attribute> targetAttributes) {

		List<Integer> indices = new ArrayList<>();
		for (Attribute targetAttribute : targetAttributes) {

			int index = table.getAttributes().indexOf(targetAttribute);
			if (index == -1) {
				throw new IllegalArgumentException("Target attribute "
						+ targetAttribute + " is not contained in datatable "
						+ table.getName());
			}
			indices.add(index);
		}

		this.indices = indices;
	}

	public Collection<Attribute> build(DataTable table) {
		List<Attribute> tableAtts = table.getAttributes();

		List<Attribute> targetAttributes = new ArrayList<>();
		for (Integer index : indices) {
			targetAttributes.add(tableAtts.get(index));
		}

		return targetAttributes;
	}
}
