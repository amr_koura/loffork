/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import java.util.LinkedHashMap;
import java.util.List;

import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.PatternFromLogicalDescriptionBuilder;

/**
 * 
 * 
 * @author bjacobs
 * 
 */
public class DefaultExceptionalModelPatternBuilder implements
		PatternFromLogicalDescriptionBuilder<ExceptionalModelPattern> {
	private final PropositionalLogic propositionalLogic;
	@SuppressWarnings("rawtypes")
	private final List<Attribute> targets;
	private final ModelFactory modelFactory;
	private final ModelDistanceFunction modelDistanceFunction;

	public DefaultExceptionalModelPatternBuilder(
			PropositionalLogic propositionalLogic,
			@SuppressWarnings("rawtypes") List<Attribute> targets,
			ModelFactory modelFactory,
			ModelDistanceFunction modelDistanceFunction) {

		this.propositionalLogic = propositionalLogic;
		this.targets = targets;
		this.modelFactory = modelFactory;
		this.modelDistanceFunction = modelDistanceFunction;
	}

	@Override
	public ExceptionalModelPattern build(Description description) {
		AbstractModel globalModel = this.modelFactory.getModel(
				propositionalLogic.getDatatable(), targets);
		AbstractModel localModel = this.modelFactory.getModel(
				propositionalLogic.getDatatable(), targets,
				description.getSupportSet());

		LinkedHashMap<InterestingnessMeasure, Double> measures = new LinkedHashMap<>();

		if (globalModel instanceof MeanDeviationModel
				&& localModel instanceof MeanDeviationModel
				&& ((MeanDeviationModel) globalModel).getAttributes().size() == 1) {
			measures.put(InterestingnessMeasure.GLOBAL_MEAN,
					((MeanDeviationModel) globalModel).getMean().get(0));
			measures.put(InterestingnessMeasure.LOCAL_MEAN,
					((MeanDeviationModel) localModel).getMean().get(0));
		}

		measures.put(
				modelDistanceFunction.getCorrespondingInterestingnessMeasure(),
				modelDistanceFunction.distance(globalModel, localModel));

		return new ExceptionalModelPattern(propositionalLogic, description,
				targets, modelDistanceFunction, localModel, globalModel,
				measures);
	}
}
