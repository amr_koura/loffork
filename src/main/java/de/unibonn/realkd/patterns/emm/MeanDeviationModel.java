/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import static com.google.common.collect.Lists.newArrayListWithCapacity;

import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.InterestingnessMeasure;

public class MeanDeviationModel extends AbstractModel {

	private static class MeanDeviationDistance implements ModelDistanceFunction {

		@Override
		public double distance(AbstractModel globalModel,
				AbstractModel localModel) {
			if (!(globalModel instanceof MeanDeviationModel && localModel instanceof MeanDeviationModel)) {
				throw new IllegalArgumentException(
						"models must be mean deviation models");
			}

			double distance = 0;
			List<Double> globalMeans = ((MeanDeviationModel) globalModel)
					.getMean();
			List<Double> localMeans = ((MeanDeviationModel) localModel)
					.getMean();
			for (int i = 0; i < globalMeans.size(); i++) {
				distance += Math.abs(globalMeans.get(i) - localMeans.get(i));
			}
			return distance;
		}

		@Override
		public InterestingnessMeasure getCorrespondingInterestingnessMeasure() {
			return InterestingnessMeasure.MANHATTAN_MEAN_DEVIATION;
		}

		@Override
		public boolean isApplicable(Class<? extends AbstractModel> modelClass) {
			return (MeanDeviationModel.class.isAssignableFrom(modelClass));
		}

		public String toString() {
			return "Manhattan mean distance";
		}

	}

	private static class PositiveDeviation implements ModelDistanceFunction {

		@Override
		public double distance(AbstractModel globalModel,
				AbstractModel localModel) {
			if (!(globalModel instanceof MeanDeviationModel
					&& localModel instanceof MeanDeviationModel
					&& globalModel.getAttributes().size() == 1 && globalModel
					.getAttributes().equals(localModel.getAttributes()))) {
				throw new IllegalArgumentException(
						"models must be mean deviation models with one attribute");
			}

			return Math.max(
					0,
					((MeanDeviationModel) localModel).getMean().get(0)
							- ((MeanDeviationModel) globalModel).getMean().get(
									0));
		}

		@Override
		public InterestingnessMeasure getCorrespondingInterestingnessMeasure() {
			return InterestingnessMeasure.POSITIVE_MEAN_DIFFERENCE;
		}

		@Override
		public boolean isApplicable(Class<? extends AbstractModel> modelClass) {
			return (MeanDeviationModel.class.isAssignableFrom(modelClass));
		}

		public String toString() {
			return "Positive mean difference";
		}

	}

	private static class NegativeDeviation implements ModelDistanceFunction {

		@Override
		public double distance(AbstractModel globalModel,
				AbstractModel localModel) {
			if (!(globalModel instanceof MeanDeviationModel
					&& localModel instanceof MeanDeviationModel
					&& globalModel.getAttributes().size() == 1 && globalModel
					.getAttributes().equals(localModel.getAttributes()))) {
				throw new IllegalArgumentException(
						"models must be mean deviation models with one attribute");
			}

			return Math.max(0, ((MeanDeviationModel) globalModel).getMean()
					.get(0))
					- ((MeanDeviationModel) localModel).getMean().get(0);
		}

		@Override
		public InterestingnessMeasure getCorrespondingInterestingnessMeasure() {
			return InterestingnessMeasure.NEGATIVE_MEAN_DIFFERENCE;
		}

		@Override
		public boolean isApplicable(Class<? extends AbstractModel> modelClass) {
			return (MeanDeviationModel.class.isAssignableFrom(modelClass));
		}

		public String toString() {
			return "Negative mean difference";
		}

	}

	public static final ModelDistanceFunction NEGATIVE_MEAN_DEVIATION = new NegativeDeviation();

	public static final ModelDistanceFunction MANHATTEN_MEAN_DEVIATION = new MeanDeviationDistance();

	public static final ModelDistanceFunction POSITIVE_MEAN_DEVIATION = new PositiveDeviation();

	private List<Double> means;

	public MeanDeviationModel(DataTable dataTable, List<Attribute> attributes) {
		super(dataTable, attributes);
		means = computeMeans();
	}

	public MeanDeviationModel(DataTable dataTable, List<Attribute> attributes,
			Set<Integer> rows) {
		super(dataTable, attributes, rows);
		means = computeMeansOnRows();
	}

	private List<Double> computeMeans() {
		List<Double> means = newArrayListWithCapacity(attributes.size());
		for (Attribute attribute : attributes) {
			if (attribute instanceof MetricAttribute) {
				means.add(((MetricAttribute) attribute).getMean());
			} else {
				throw new IllegalArgumentException(
						"can not construct mean model for non-numeric attributes");
			}
		}
		return means;
	}

	private List<Double> computeMeansOnRows() {
		List<Double> means = newArrayListWithCapacity(attributes.size());
		for (Attribute attribute : attributes) {
			if (attribute instanceof MetricAttribute) {
				means.add(((MetricAttribute) attribute)
						.getMeanOnRows(getRows()));
			} else {
				throw new IllegalArgumentException(
						"can not construct mean model for non-numeric attributes");
			}
		}
		return means;
	}

	public List<Double> getMean() {
		return means;
	}
}
