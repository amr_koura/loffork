/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.AbstractLogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.InterestingnessMeasure;

public class ExceptionalModelPattern extends
		AbstractLogicallyDescribedLocalPattern {

	private final AbstractModel globalModel;
	private final AbstractModel localModel;
	private final List<Attribute> targets;
	private final ModelDistanceFunction modelDistanceFunction;

	ExceptionalModelPattern(PropositionalLogic propositionalLogic,
			Description description, List<Attribute> targets,
			ModelDistanceFunction modelDistanceFunction,
			AbstractModel localModel, AbstractModel globalModel,
			LinkedHashMap<InterestingnessMeasure, Double> measures) {

		super(propositionalLogic, description);
		this.targets = targets;
		this.modelDistanceFunction = modelDistanceFunction;
		this.localModel = localModel;
		this.globalModel = globalModel;

		for (Map.Entry<InterestingnessMeasure, Double> entry : measures
				.entrySet()) {
			addMeasurement(entry.getKey(), entry.getValue());
		}
		// List<Attribute> unmodTargets = unmodifiableList(targets);
		// initModels(unmodTargets);
	}

	public AbstractModel getGlobalModel() {
		return globalModel;
	}

	public AbstractModel getLocalModel() {
		return localModel;
	}

	public List<Attribute> getTargetAttributes() {
		return globalModel.getAttributes();
	}

	/**
	 * <p>
	 * Convenience function that returns distance between global and local model
	 * with respect to distance function.
	 * </p>
	 * <p>
	 * NOTE: function should be used judiciously, because different distance
	 * functions can have different scales. Hence, comparing patterns based on
	 * value of this method can be deceiving.
	 * 
	 */
	public double getModelDeviation() {
		return modelDistanceFunction.distance(globalModel, localModel);
		// return getValue(modelDistanceFunction
		// .getCorrespondingInterestingnessMeasure());
	}

	@Override
	public List<Attribute> getAttributes() {
		return getTargetAttributes();
	}

}
