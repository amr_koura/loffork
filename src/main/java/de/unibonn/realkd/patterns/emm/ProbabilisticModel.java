/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import java.util.List;
import java.util.Set;

import org.apache.commons.math3.special.Erf;

import de.unibonn.realkd.patterns.emm.AbstractModel;
import de.unibonn.realkd.patterns.emm.ContingencyTable;
import de.unibonn.realkd.patterns.emm.ContingencyTableCellKey;
import de.unibonn.realkd.patterns.emm.ContingencyTableModel;
import de.unibonn.realkd.patterns.emm.GaussianModel;
import de.unibonn.realkd.patterns.emm.ModelDistanceFunction;
import de.unibonn.realkd.patterns.emm.ProbabilisticModel;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.InterestingnessMeasure;

public abstract class ProbabilisticModel extends AbstractModel {

	private static class TotalVariationDistance implements
			ModelDistanceFunction {

		@Override
		public double distance(AbstractModel globalModel,
				AbstractModel localModel) {
			if (globalModel instanceof ContingencyTableModel
					&& localModel instanceof ContingencyTableModel) {
				return contingencyTableDistance(
						(ContingencyTableModel) globalModel,
						(ContingencyTableModel) localModel);
			} else if (globalModel instanceof GaussianModel
					&& localModel instanceof GaussianModel) {
				return gaussianDistance((GaussianModel) globalModel,
						(GaussianModel) localModel);
			}
			return 0;
		}

		private double gaussianDistance(GaussianModel globalModel,
				GaussianModel localModel) {
			// TODO: check formula
			return Math.abs(Erf.erf((localModel.getMean() - globalModel
					.getMean())
					/ (2 * Math.sqrt(2 * globalModel.getVariance()))));
		}

		private double contingencyTableDistance(
				ContingencyTableModel globalModel,
				ContingencyTableModel localModel) {
			ContingencyTable ct1 = globalModel.getProbabilities();
			ContingencyTable ct2 = localModel.getProbabilities();

			double absoluteDifference = 0.0;

			for (ContingencyTableCellKey key : ct1.getKeys()) {
				absoluteDifference += Math.abs(ct1.getNormalizedValue(key)
						- ct2.getNormalizedValue(key));
			}

			return absoluteDifference / 2.0;
		}

		@Override
		public InterestingnessMeasure getCorrespondingInterestingnessMeasure() {
			return InterestingnessMeasure.TOTAL_VARIATION_DISTANCE;
		}

		@Override
		public boolean isApplicable(Class<? extends AbstractModel> modelClass) {
			return (ProbabilisticModel.class.isAssignableFrom(modelClass));
		}

		@Override
		public String toString() {
			return "Total variation distance";
		}
	}

	public static ModelDistanceFunction TOTALVARIATION = new TotalVariationDistance();

	public ProbabilisticModel(DataTable dataTable,
			@SuppressWarnings("rawtypes") List<Attribute> attributes) {
		super(dataTable, attributes);
	}

	public ProbabilisticModel(DataTable dataTable,
			@SuppressWarnings("rawtypes") List<Attribute> attributes,
			Set<Integer> rows) {
		super(dataTable, attributes, rows);
	}
}
