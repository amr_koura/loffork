/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.Set;

import de.unibonn.realkd.patterns.emm.ProbabilisticModel;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

public class GaussianModel extends ProbabilisticModel {

	private double mean;
	private double variance;
	private double min;
	private double max;

	public GaussianModel(DataTable dataTable, Attribute attribute) {
		super(dataTable, newArrayList(attribute));
		mean = ((MetricAttribute) attribute).getMean();
		variance = ((MetricAttribute) attribute).getVariance();
		min = ((MetricAttribute) attribute).getMin();
		max = ((MetricAttribute) attribute).getMax();
	}

	public GaussianModel(DataTable dataTable, Attribute attribute,
			Set<Integer> rows) {
		super(dataTable, newArrayList(attribute), rows);
		if (!(attribute instanceof MetricAttribute)) {
			throw new IllegalArgumentException(
					"can only be instantiated with numeric attribute");
		}
		mean = getMeanOnRows((MetricAttribute) attribute);
		getMinMaxVarianceOnRows((MetricAttribute) attribute);
	}

	private double getMeanOnRows(MetricAttribute attribute) {
		return attribute.getMeanOnRows(getRows());
	}

	private void getMinMaxVarianceOnRows(MetricAttribute attribute) {
		double result = 0.0;
		if (getRows().size() == 0) {
			variance = 0;
			min = 0;
			max = 0;
			return;
		}
		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;
		for (int rowIx : getRows()) {
			if (attribute.isValueMissing(rowIx)) {
				continue;
			}
			double value = attribute.getValue(rowIx);
			min = min(min, value);
			max = max(max, value);
			result += (mean - value) * (mean - value);
		}
		variance = result / getRows().size();
	}

	public double getMean() {
		return mean;
	}

	public double getVariance() {
		return variance;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}
}
