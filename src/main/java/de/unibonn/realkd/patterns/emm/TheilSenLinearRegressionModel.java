/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.patterns.emm.AbstractModel;
import de.unibonn.realkd.patterns.emm.ModelDistanceFunction;
import de.unibonn.realkd.patterns.emm.TheilSenLinearRegressionModelDistanceFunction;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

/**
 * 
 * @author eevendijevs
 *
 */
public class TheilSenLinearRegressionModel extends AbstractModel {
	private Double slope = null;
	private Double intercept = null;
	private List<Double> covariateValues;
	private List<Double> regressandValues;

	public Double getSlope() {
		return this.slope;
	}

	public Double getIntercept() {
		return this.intercept;
	}

	public static ModelDistanceFunction COSINEDISTANCE = new TheilSenLinearRegressionModelDistanceFunction();

	public TheilSenLinearRegressionModel(DataTable dataTable,
			List<Attribute> attributes) {
		super(dataTable, attributes);

		// estimateParametersOn(attributes.get(0).getDataTable().getObjectIds());
		estimateParametersOn(getDataTable().getObjectIds());
	}

	public TheilSenLinearRegressionModel(DataTable dataTable,
			List<Attribute> attributes, Set<Integer> rows) {
		super(dataTable, attributes, rows);

		// DataTable dataTable = attributes.get(0).getDataTable();
		// List<List<String>> fullRows = newArrayList();
		// for (int rowIndex : getRows()) {
		// fullRows.add(dataTable.getRow(rowIndex));
		// }

		estimateParametersOn(getRows());
	}

	public Double predict(Double x) {
		if (this.slope == null || this.intercept == null) {
			return null;
		}
		return this.slope * x + intercept;
	}

	// // we approximate values of attributes[1] as a function of attributes[0]
	// private void estimateParametersOn(List<List<String>> fullRows) {
	// ensureNumericalityOfAttributes();
	// ensureOnlyTwoAttributes();
	//
	// covariateValues = new ArrayList<>();
	// regressandValues = new ArrayList<>();
	// int covariateAttributeIndex = getAttributes().get(0).getIndexInTable();
	// int regressandAttributeIndex = getAttributes().get(1).getIndexInTable();
	// for (List<String> fullRow : fullRows) {
	// covariateValues.add(Double.parseDouble(fullRow
	// .get(covariateAttributeIndex)));
	// regressandValues.add(Double.parseDouble(fullRow
	// .get(regressandAttributeIndex)));
	// }
	//
	// estimateSlope();
	// estimateIntercept();
	// }

	private void estimateParametersOn(Set<Integer> rows) {
		ensureNumericalityOfAttributes();
		ensureOnlyTwoAttributes();

		covariateValues = new ArrayList<>();
		regressandValues = new ArrayList<>();
		for (Integer row : rows) {
			if (attributes.get(0).isValueMissing(row)
					|| attributes.get(1).isValueMissing(row)) {
				continue;
			}
			covariateValues.add(((MetricAttribute) attributes.get(0))
					.getValue(row));
			regressandValues.add(((MetricAttribute) attributes.get(1))
					.getValue(row));
		}

		estimateSlope();
		estimateIntercept();
	}

	private void estimateSlope() {
		List<Double> slopes = new ArrayList<>();
		for (int i = 0; i < covariateValues.size() - 1; i++) {
			for (int j = i + 1; j < covariateValues.size(); j++) {
				double covDiff = covariateValues.get(j)
						- covariateValues.get(i);
				if (covDiff != 0d) {
					slopes.add((regressandValues.get(j) - regressandValues
							.get(i)) / covDiff);
				}
			}
		}
		if (slopes.size() > 0) {
			this.slope = medianOf(slopes);
		}
	}

	// private void estimateIntercept(List<List<String>> fullRows) {
	// if (this.slope == null) {
	// return;
	// }
	// List<Double> intercepts = new ArrayList<>();
	// for (int i = 0; i < covariateValues.size(); i++) {
	// intercepts.add(regressandValues.get(i) - this.slope
	// * covariateValues.get(i));
	// }
	// if (intercepts.size() > 0) {
	// this.intercept = medianOf(intercepts);
	// }
	// }

	private void estimateIntercept() {
		if (this.slope == null) {
			return;
		}
		List<Double> intercepts = new ArrayList<>();
		for (int i = 0; i < covariateValues.size(); i++) {
			intercepts.add(regressandValues.get(i) - this.slope
					* covariateValues.get(i));
		}
		if (intercepts.size() > 0) {
			this.intercept = medianOf(intercepts);
		}
	}

	private void ensureNumericalityOfAttributes() {
		if (!(getAttributes().get(0) instanceof MetricAttribute)
				|| !(getAttributes().get(1) instanceof MetricAttribute)) {
			throw new IllegalArgumentException(
					"Both of targets attributes must be numeric!");
		}
	}

	private void ensureOnlyTwoAttributes() {
		if (2 != getAttributes().size()) {
			throw new IllegalArgumentException(
					"There must be two target attributes only.");
		}
	}

	private Double medianOf(List<Double> values) {
		Double median;
		Collections.sort(values);
		if (values.size() % 2 == 0) {
			int ind = values.size() / 2 - 1;
			median = (values.get(ind) + values.get(ind + 1)) / 2.0;
		} else {
			median = values.get(values.size() / 2);
		}
		return median;
	}
}
