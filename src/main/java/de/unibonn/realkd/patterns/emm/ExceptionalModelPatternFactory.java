/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import java.util.List;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Description;

public enum ExceptionalModelPatternFactory {

	TVD_BETWEEN_CONTINGENCY_TABLE_PF {
		@Override
		public ExceptionalModelPattern newExceptionModelPattern(
				PropositionalLogic propositionalLogic, List<Proposition> description,
				List<Attribute> targets) {
			// return new ExceptionalModelPattern(dataTable, new Description(
			// dataTable, description), targets,
			// ContingencyTableModelFactory.INSTANCE,
			// ContingencyTableModel.TOTALVARIATION);
		    	Description desc = new Description(propositionalLogic, description);
		    	return new DefaultExceptionalModelPatternBuilder(propositionalLogic,
				targets, ContingencyTableModelFactory.INSTANCE,
				ContingencyTableModel.TOTALVARIATION).build(desc);

/*
			return new ExceptionalModelPattern(propositionalLogic,
					new Description(propositionalLogic,
							description), targets,
					ContingencyTableModelFactory.INSTANCE,
					ContingencyTableModel.TOTALVARIATION);
*/
		}

		@Override
		public boolean isApplicable(List<Attribute> targets) {
			return ContingencyTableModelFactory.INSTANCE.isApplicable(targets);
		}

		@Override
		public String toString() {
			return "Total Variation of Contingency Tables";
		}
	},

	TVD_BETWEEN_GAUSSIAN_PF {
		@Override
		public ExceptionalModelPattern newExceptionModelPattern(
				PropositionalLogic propositionalLogic, List<Proposition> description,
				List<Attribute> targets) throws IllegalArgumentException {
			if (targets.size() != 1) {
				throw new IllegalArgumentException(
						"Target attribute for Gaussian subgroup not singleton");
			}
			if (!(targets.get(0) instanceof MetricAttribute)) {
				throw new IllegalArgumentException(
						"Target attribute for Gaussian subgroup not numeric");
			}

			// return new ExceptionalModelPattern(dataTable, new Description(
			// dataTable, description), targets,
			// GaussianModelFactory.INSTANCE, GaussianModel.TOTALVARIATION);
			Description desc = new Description(propositionalLogic, description);
			return new DefaultExceptionalModelPatternBuilder(propositionalLogic,
				targets, GaussianModelFactory.INSTANCE,
				GaussianModel.TOTALVARIATION).build(desc);

/*
		    return new ExceptionalModelPattern(propositionalLogic,
					new Description(propositionalLogic,
							description), targets,
					GaussianModelFactory.INSTANCE, GaussianModel.TOTALVARIATION);
*/
		}

		@Override
		public boolean isApplicable(List<Attribute> targets) {
			return GaussianModelFactory.INSTANCE.isApplicable(targets);
		}

		@Override
		public String toString() {
			return "Total Variation of Fitted Gaussians";
		}
	},

	ANGLE_BETWEEN_LINEAR_REGRESSION_PF {
		@Override
		public ExceptionalModelPattern newExceptionModelPattern(
				PropositionalLogic propositionalLogic, List<Proposition> description,
				List<Attribute> targets) {
			// return new ExceptionalModelPattern(dataTable, new Description(
			// dataTable, description), targets,
			// TheilSenLinearRegressionModelFactory.INSTANCE,
			// TheilSenLinearRegressionModel.COSINEDISTANCE);
			Description desc = new Description(propositionalLogic, description);
			return new DefaultExceptionalModelPatternBuilder(propositionalLogic,
				targets, TheilSenLinearRegressionModelFactory.INSTANCE,
				TheilSenLinearRegressionModel.COSINEDISTANCE).build(desc);

/*
			return new ExceptionalModelPattern(propositionalLogic,
					new Description(propositionalLogic,
							description), targets,
					TheilSenLinearRegressionModelFactory.INSTANCE,
					TheilSenLinearRegressionModel.COSINEDISTANCE, localModel, globalModel, measures);
*/
		}

		@Override
		public boolean isApplicable(List<Attribute> targets) {
			return TheilSenLinearRegressionModelFactory.INSTANCE
					.isApplicable(targets);
		}

		@Override
		public String toString() {
			return "Cosine distance between regression models";
		}
	},

	EUCLEDIAN_MEAN_DEV_PF {
		@Override
		public ExceptionalModelPattern newExceptionModelPattern(
				PropositionalLogic propositionalLogic, List<Proposition> description,
				List<Attribute> targets) {
			// return new ExceptionalModelPattern(dataTable, new Description(
			// dataTable, description), targets,
			// MeanModelFactory.INSTANCE, MeanDeviationModel.MEANDEVIATION);
			Description desc = new Description(propositionalLogic, description);
			return new DefaultExceptionalModelPatternBuilder(propositionalLogic,
				targets, MeanModelFactory.INSTANCE,
				MeanDeviationModel.MANHATTEN_MEAN_DEVIATION).build(desc);

/*
		    return new ExceptionalModelPattern(propositionalLogic,
					new Description(propositionalLogic,
							description), targets, MeanModelFactory.INSTANCE,
					MeanDeviationModel.MEANDEVIATION);
*/
		}

		@Override
		public String toString() {
			return "Eucledian distance between means";
		}

		@Override
		public boolean isApplicable(List<Attribute> targets) {
			return MeanModelFactory.INSTANCE.isApplicable(targets);
		}
	}

	;

	public abstract ExceptionalModelPattern newExceptionModelPattern(
			PropositionalLogic propositionalLogic, List<Proposition> description,
			List<Attribute> targets);

	public abstract boolean isApplicable(List<Attribute> targets);

}
