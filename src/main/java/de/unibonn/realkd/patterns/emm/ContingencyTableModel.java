/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.emm;

import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.patterns.emm.CategoricalKeyComputer;
import de.unibonn.realkd.patterns.emm.ContingencyTable;
import de.unibonn.realkd.patterns.emm.ContingencyTableCellKey;
import de.unibonn.realkd.patterns.emm.NumericalKeyComputer;
import de.unibonn.realkd.patterns.emm.ProbabilisticModel;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

public class ContingencyTableModel extends ProbabilisticModel {

	private ContingencyTable contingencyTable;

	public ContingencyTableModel(DataTable dataTable, List<Attribute> attributes) {
		super(dataTable, attributes);
		DEFAULT.log("contingency_table_construction",
				"constructing global contingency table for attributes: "
						+ attributes, LogMessageType.DEBUG_MESSAGE);
		contingencyTable = computeTable2();
	}

	public ContingencyTableModel(DataTable dataTable,
			List<Attribute> attributes, Set<Integer> rows) {
		super(dataTable, attributes, rows);
		DEFAULT.log("contingency_table_construction",
				"constructing local contingency table on " + rows.size()
						+ " rows for attributes: " + attributes,
				LogMessageType.DEBUG_MESSAGE);
		contingencyTable = computeTableForRows2();
	}

	private ContingencyTable computeTable2() {
		// return
		// computeTableForRows2(attributes.get(0).getDataTable().getObjectIds());
		return computeTableForRows2(getDataTable().getObjectIds());
	}

	private ContingencyTable computeTableForRows2() {
		return computeTableForRows2(getRows());
	}

	// private ContingencyTable computeTableForRows2(List<List<String>> rows)
	// {
	// List<ContingencyTable.KeyComputer> keyComputers = getKeyComputers();
	// List<Integer> attributeIndicesInTable = getAttributeIndicesInTable();
	// ContingencyTable table = new ContingencyTable(keyComputers);
	//
	// for (List<String> row : rows) {
	// List<String> key = computeKey(row, keyComputers,
	// attributeIndicesInTable);
	// ContingencyTableCellKey cellKey = new ContingencyTableCellKey(key);
	// table.incrementValue(cellKey);
	// }
	//
	// return table;
	// }

	private boolean rowValueMissingForAtLeastOneAttribute(Integer rowId) {
		for (Attribute attribute : attributes) {
			if (attribute.isValueMissing(rowId)) {
				return true;
			}
		}
		return false;
	}

	private ContingencyTable computeTableForRows2(Set<Integer> rows) {
		List<ContingencyTable.KeyComputer> keyComputers = getKeyComputers();
		ContingencyTable table = new ContingencyTable(keyComputers);

		for (Integer row : rows) {
			if (rowValueMissingForAtLeastOneAttribute(row)) {
				continue;
			}
			List<String> key = computeKey(row, keyComputers);
			ContingencyTableCellKey cellKey = new ContingencyTableCellKey(key);
			table.incrementValue(cellKey);
		}

		return table;
	}

	// private List<String> computeKey(List<String> row,
	// List<ContingencyTable.KeyComputer> keyComputers,
	// List<Integer> attributeIndicesInTable) {
	// List<String> key = newArrayListWithCapacity(attributes.size());
	//
	// Iterator<ContingencyTable.KeyComputer> itKC = keyComputers.iterator();
	// Iterator<Integer> itAIIT = attributeIndicesInTable.iterator();
	//
	// while (itKC.hasNext()) {
	// key.add(itKC.next().computeKey(row.get(itAIIT.next())));
	// }
	//
	// return key;
	// }

	/**
	 * 
	 * @param row
	 *            with present values for all attributes
	 * @param keyComputers
	 * @return a list of keys (one for each target attribute) whose elements
	 *         identify jointly the cell in the contingency table that
	 *         corresponds the target attribute values in row
	 */
	private List<String> computeKey(Integer row,
			List<ContingencyTable.KeyComputer> keyComputers) {
		List<String> key = newArrayListWithCapacity(attributes.size());
		Iterator<ContingencyTable.KeyComputer> itKC = keyComputers.iterator();

		for (Attribute attribute : attributes) {
			key.add(itKC.next().computeKey(attribute.getValue(row).toString()));
		}

		return key;
	}

	// private List<Integer> getAttributeIndicesInTable() {
	// List<Integer> indices = newArrayListWithCapacity(attributes.size());
	// for (Attribute attribute : attributes) {
	// indices.add(attribute.getIndexInTable());
	// }
	// return indices;
	// }

	private List<ContingencyTable.KeyComputer> getKeyComputers() {
		List<ContingencyTable.KeyComputer> keyComputers = newArrayListWithCapacity(attributes
				.size());
		for (Attribute attribute : attributes) {
			ContingencyTable.KeyComputer keyComputer = getKeyComputer(attribute);
			if (keyComputer != null) {
				keyComputers.add(keyComputer);
			}
		}
		return keyComputers;
	}

	private ContingencyTable.KeyComputer getKeyComputer(Attribute attribute) {
		if (attribute instanceof CategoricalAttribute) {
			return new CategoricalKeyComputer((CategoricalAttribute) attribute);
		}

		if (attribute instanceof MetricAttribute) {
			return new NumericalKeyComputer((MetricAttribute) attribute);
		}

		// TODO: Add key computation for ordinal attribute
		throw new UnsupportedOperationException(
				"key computation not supported for attributes that are neither categoric nor numeric");

	}

	public ContingencyTable getProbabilities() {
		return contingencyTable;
	}
}