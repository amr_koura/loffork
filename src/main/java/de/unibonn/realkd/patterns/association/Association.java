/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.association;

import java.util.List;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.AbstractLogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.InterestingnessMeasure;

/**
 * Pattern class that captures above (or below) expectation frequency of the
 * conjunction of a set of propositions.
 * 
 * @author mboley
 * 
 */
public class Association extends AbstractLogicallyDescribedLocalPattern {

	Association(Description description, List<InterestingnessMeasure> measures,
			List<Double> measureValues) {
		super(description.getPropositionalLogic(), description);
		for (int i=0; i<measures.size();i++) {
			addMeasurement(measures.get(i), measureValues.get(i));
		}
	}

	public double getLift() {
		if (hasMeasure(InterestingnessMeasure.LIFT)) {
			return getValue(InterestingnessMeasure.LIFT);
		} else {
			return -1 * getValue(InterestingnessMeasure.NEGATIVE_LIFT);
		}
	}

	public double getExpectedFrequency() {
		return getValue(InterestingnessMeasure.PRODUCT_OF_INDIVIDUAL_FREQUENCIES);
	}

	@Override
	public List<Attribute> getAttributes() {
		return getDescription().getAttributes();
	}

}
