/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.association;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.PatternFromLogicalDescriptionBuilder;

/**
 * Builder for associations that binds the measures (in addition to frequency):
 * product of individual frequencies, deviation (difference) of frequency, lift.
 * 
 * @author mboley, bjacobs
 * 
 */
public class DefaultAssociationBuilder implements
		PatternFromLogicalDescriptionBuilder<Association> {

	private double computeProductOfIndividualFrequencies(Description description) {
		double product = 1.0;
		int propLogicSize = description.getPropositionalLogic().getSize();

		for (Proposition<?> literal : description.getElements()) {
			product *= literal.getSupportSet().size() / (double) propLogicSize;
		}

		return product;
	}

	@Override
	public Association build(Description description) {
		List<InterestingnessMeasure> measures = new ArrayList<>();
		List<Double> values = new ArrayList<>();

		double productOfFrequencies = computeProductOfIndividualFrequencies(description);

		double denominator = Math.pow(2, description.size() - 2.);

		double frequency = (double) description.getSupportSet().size()
				/ description.getPropositionalLogic().getSize();

		double deviationOfFrequency = frequency - productOfFrequencies;

		double lift = deviationOfFrequency / denominator;

		measures.add(InterestingnessMeasure.PRODUCT_OF_INDIVIDUAL_FREQUENCIES);
		values.add(productOfFrequencies);

		if (lift < 0) {
			measures.add(InterestingnessMeasure.NEGATIVE_LIFT);
			values.add(-1 * lift);
		} else {
			measures.add(InterestingnessMeasure.LIFT);
			values.add(lift);
		}

		measures.add(InterestingnessMeasure.DEVIATION_OF_FREQUENCY);
		values.add(deviationOfFrequency);

		return new Association(description, measures, values);
	}
}
